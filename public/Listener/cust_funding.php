<?php namespace Listener;

require('PaypalIPN.php');

use PaypalIPN;

$ipn = new PaypalIPN();

// Use the sandbox endpoint during testing.
//$ipn->useSandbox();
$verified = $ipn->verifyIPN();
if ($verified) {
    /*
     * Process IPN
     * A list of variables is available here:
     * https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/
     */


    //transaction details
    $business = isset($_POST["business"]) ? $_POST["business"] : ""; //me as the merchant
    $custom = isset($_POST["custom"]) ? $_POST["custom"] : "";
    $receiver_email = isset($_POST["receiver_email"]) ? $_POST["receiver_email"] : "";
    $parent_txn_id = isset($_POST["parent_txn_id"]) ? $_POST["parent_txn_id"] : "";
    $txn_id = isset($_POST["txn_id"]) ? $_POST["txn_id"] : "";
    $txn_type = isset($_POST["txn_type"]) ? $_POST["txn_type"] : "";

    //payer details
    $first_name = isset($_POST["first_name"]) ? $_POST["first_name"] : "";
    $last_name = isset($_POST["last_name"]) ? $_POST["last_name"] : "";
    $payer_email = isset($_POST["payer_email"]) ? $_POST["payer_email"] : "";
    $payer_id = isset($_POST["payer_id"]) ? $_POST["payer_id"] : "";

    //payment info
    $mc_gross = isset($_POST["mc_gross"]) ? $_POST["mc_gross"] : "0";
    $mc_fee = isset($_POST["mc_fee"]) ? $_POST["mc_fee"] : "0";
    $currency = isset($_POST["mc_currency"]) ? $_POST["mc_currency"] : "";
    $payment_status = $_POST["payment_status"];
    $pending_reason = isset($_POST["pending_reason"]) ? $_POST["pending_reason"] : ""; //This variable is set only if payment_status is Pending.
    $payer_status = isset($_POST["payer_status"]) ? $_POST["payer_status"] : "";
    $reason_code = isset($_POST["reason_code"]) ? $_POST["reason_code"] : ""; //This variable is set if payment_status is Reversed, Refunded, Canceled_Reversal, or Denied.
    $payment_date = isset($_POST["payment_date"]) ?$_POST["payment_date"] : "";
    $payment_type = isset($_POST["payment_type"]) ? $_POST["payment_type"] :"";
    $address_status = isset($_POST["address_status"]) ? $_POST["address_status"] : "";
    $item_name = isset($_POST["item_name"]) ? $_POST["item_name"] : "";
    $address_name = isset( $_POST["address_name"]) ?  $_POST["address_name"] : "";
    $address_street = isset($_POST["address_street"]) ? $_POST["address_street"] : "";
    $address_zip = isset($_POST["address_zip"]) ? $_POST["address_zip"] : "";
    $address_country_code = isset($_POST["address_country_code"]) ? $_POST["address_country_code"] : "";
    $ipn_track_id = isset($_POST["ipn_track_id"]) ? $_POST["ipn_track_id"] : "";
    $protection_eligibility = isset($_POST["protection_eligibility"]) ? $_POST["protection_eligibility"] : "";

    $ENDPOINT =  "https://www.essayenergy.com/paypal/ipn/receive";


    $req="&txn_type=$txn_type&custom=$custom&business=$business&receiver_email=$receiver_email&parent_txn_id=$parent_txn_id&txn_id=$txn_id&first_name=$first_name
                &last_name=$last_name&payer_email=$payer_email&payer_id=$payer_id&mc_gross=$mc_gross&mc_fee=$mc_fee&currency=$currency&payment_status=$payment_status
                &pending_reason=$pending_reason&payer_status=$payer_status&reason_code=$reason_code&payment_date=$payment_date&payment_type=$payment_type
                &address_status=$address_status&item_name=$item_name&address_name=$address_name&address_street=$address_street&address_zip=$address_zip
                &address_country_code=$address_country_code&ipn_track_id=$ipn_track_id&protection_eligibility=$protection_eligibility";


    $ch = curl_init($ENDPOINT);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    $res = curl_exec($ch);

    if ( ! ($res)) {
        $errno = curl_errno($ch);
        $errstr = curl_error($ch);
        curl_close($ch);
        throw new \Exception("Internal cURL error: [$errno] $errstr");
    }

    if ($res != "success") {
        throw new \Exception("internal failure".$res);
    }

}

// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
header("HTTP/1.1 200 OK");
