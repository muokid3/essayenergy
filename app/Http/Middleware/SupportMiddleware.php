<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class SupportMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->user_group != 4) {
            return new Response(view('errors.unauthorized'));
        }
        return $next($request);
    }
}
