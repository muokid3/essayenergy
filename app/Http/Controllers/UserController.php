<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->user_group == 1){
            //customer
            return redirect('/orders/my_orders');

        }else if (Auth::user()->user_group == 2 ){
            //writer
            abort(404);

        }else if (Auth::user()->user_group == 3 ){
            //admin
            return view('user.index');


        }else if (Auth::user()->user_group == 4 ){
            //support
            $orders = Order::where('status_id',11)->orderBy('id', 'desc')->paginate(50);
            return view('support.orders_list')->with(['orders'=>$orders]);

        }else{
            abort(404);

        }
    }

}
