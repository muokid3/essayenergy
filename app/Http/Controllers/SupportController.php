<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Notifications\BidPlaced;
use App\Notifications\OrderAccepted;
use App\Notifications\OrderCompleted;
use App\Order;
use App\OrderAssignment;
use App\Rating;
use App\SampleEssay;
use App\User;
use App\WriterProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class SupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->request = new Request();
        $this->order = new Order();

    }

    public function order($order_id)
    {
        $order = Order::find($order_id);

        if (is_null($order)){
            Session::flash('error', 'Order not available');
            return redirect('/dashboard');
        }else{

            if (is_null($order->assigned)){
                return view("support.order_details")->withOrder($order);

            }else{
                if ($order->assigned->support_id = \auth()->user()->id){
                    return view("support.order_details")->withOrder($order);

                }else{
                    Session::flash('error', 'Order not available');
                    return redirect('/dashboard');
                }
            }

        }
    }

    public function accept_order(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'welcome_message' => 'required',
        ]);

        $this->order = Order::find($this->request->order_id);

        if (is_null($this->order) || ($this->order->status_id != 11 )){
            Session::flash('error', 'This order is no longer available');
            return redirect('/dashboard');
        }else{

            DB::transaction(function () {

                $orderAssignemnt = new OrderAssignment();
                $orderAssignemnt->order_id = $this->request->order_id;
                $orderAssignemnt->support_id = Auth::user()->id;
                $orderAssignemnt->status_id = 6;
                $orderAssignemnt->welcome_message = $this->request->welcome_message;



                if ($orderAssignemnt->saveOrFail()){

                    $this->order->status_id = 6;
                    $this->order->update();

                    $customer = Customer::find($this->order->customer_id);

                    //notify customer here
                    $customer->user->notify(new OrderAccepted($this->order, Auth::user(), $orderAssignemnt));

                    Session::flash('success', 'Order has been accepted');
                }else{
                    Session::flash('error', 'An error occurred when accepting the order, please try again!');
                }

            });
        }

        return redirect('order/'.$this->request->order_id);
    }

    public function update_submission(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'order_submission' => 'required',
        ]);

        if ($this->request->btn_action == "save_submit"){


            $this->validate($request, [
                'file' => 'required',
            ]);


            DB::transaction(function () {

                $file = $this->request->file('file');
                //get filename with extension
                $filenamewithextension = $file->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $file->getClientOriginalExtension();

                //filename to store
                $filenametostore = 'submission/'.$filename.'_'.time().'.'.$extension;

                //Upload File to s3
                Storage::disk('s3')->put($filenametostore, fopen($file, 'r+'), 'public');



                $orderAssignemnt = OrderAssignment::find($this->request->assigned_id);
                $order = Order::find($orderAssignemnt->order_id);
                $orderAssignemnt->submission = $this->request->order_submission;
                $orderAssignemnt->support_finished = 1;
                $orderAssignemnt->submitted_file = $filenametostore;


                if ($orderAssignemnt->status_id == 12) {
                    //in revision. update to completed
                    $orderAssignemnt->status_id=5; //completed
                }else{
                    //ongoing. update to submitted
                    $orderAssignemnt->status_id=7; //submitted
                }

                if ($order->status_id == 12) {
                    //in revision. update to completed
                    $order->status_id=5; //completed
                }else{
                    //ongoing. update to submitted
                    $order->status_id=7; //submitted
                }

                if ($orderAssignemnt->update() && $order->update()){
                    //notify customer
                    $order->customer->user->notify(new OrderCompleted($order,Auth::user()));
                    Session::flash('success', 'Your order has been saved and submitted to the customer successfully');
                }else{
                    Session::flash('error', 'An error occurred when updating the submission, please try again!');
                }

            });

        }else{
            DB::transaction(function () {

                $orderAssignemnt = OrderAssignment::find($this->request->assigned_id);
                $orderAssignemnt->submission = $this->request->order_submission;


                if ($orderAssignemnt->update()){
                    Session::flash('success', 'Updated successfully');
                }else{
                    Session::flash('error', 'An error occurred when updating the submission, please try again!');
                }

            });
        }
        return redirect()->back();
    }

    public function in_progress()
    {
        $orderAssignments = OrderAssignment::where('support_id', Auth::user()->id)
            ->where('status_id', 6)
            ->orderBy('id', 'desc')
            ->paginate(15);
        return view('support.in_progress')->withOrdersAssignments($orderAssignments);
    }

    public function completed()
    {
        $orderAssignments = OrderAssignment::where('support_id', Auth::user()->id)
            ->where('status_id', 5)
            ->orderBy('id', 'desc')
            ->paginate(15);
        return view('support.completed')->withOrdersAssignments($orderAssignments);
    }

    public function my_orders()
    {
        $orderAssignments = OrderAssignment::where('support_id', Auth::user()->id)
            ->orderBy('id', 'desc')
            ->paginate(15);
        return view('support.my_orders')->withOrdersAssignments($orderAssignments);
    }

    public function rate_customer(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'rating' => 'required',
            'review' => 'required',
        ],[
            'rating.required' => 'Please provide a rating',
            'review.required' => 'Please provide a review'
        ]);

        DB::transaction(function () {
            $rating = new Rating();
            $rating->rated_user = $this->request->rated_user;
            $rating->rated_by = Auth::user()->id;
            $rating->order_id = $this->request->order_id;
            $rating->rating = $this->request->rating;
            $rating->review = $this->request->review;


            $orderAssignemnt = OrderAssignment::find($this->request->assigned_id);
            $orderAssignemnt->support_done_review = 1;

            if ($rating->saveOrFail() && $orderAssignemnt->update()){
                Session::flash('success', 'Thank you for your rating');
            }else{
                Session::flash('error', 'An error occurred when rating, please try again!');
            }

        });

        return redirect()->back();
    }

















//    public function remove_bid($bid_id)
//    {
//        $bid = Bid::find($bid_id);
//
//        if (is_null($bid)){
//            Session::flash('error', 'Bid not found');
//            return redirect('/dashboard');
//        }else{
//            $bid->delete();
//            return redirect()->back();
//        }
//    }

//    public function update_bid(Request $request)
//    {
//        $this->request = $request;
//        $this->validate($request, [
//            'bid_amount' => 'required',
//        ]);
//
//        $order = Order::find($this->request->order_id);
//
//        if ($this->request->bid_amount < $order->minimum_bid()){
//            Session::flash('error', "You can not bid below $".$order->minimum_bid()." for this order");
//        }else{
//
//            DB::transaction(function () {
//
//                $bid = Bid::find($this->request->bid_id);
//                $bid->bid_amount = $this->request->bid_amount;
//
//
//                if ($bid->update()){
//                    Session::flash('success', 'Your bid has been updated successfully');
//                }else{
//                    Session::flash('error', 'An error occurred when updating the bid, please try again!');
//                }
//
//            });
//
//        }
//
//        return redirect()->back();
//    }
//
//
//    public function bids()
//    {
//        $bids = Bid::join('orders','bids.order_id','orders.id')
//            ->select('bids.*','orders.id as order_id', 'orders.topic','orders.deadline','orders.pages')
//            ->where('bids.bidder_id', Auth::user()->id)
//            ->where('bids.status', 0)
//            ->where('orders.status', 1)
//            ->orderBy('bids.updated_at', 'desc')
//            ->paginate(15);
//        return view('writer.bids')->withBids($bids);
//    }



    public function profile()
    {
        $writerProfile = WriterProfile::where('user_id',Auth::user()->id)->first();
        if (is_null($writerProfile)){
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('dashboard');
        }else{
            return view('writer.profile')->withProfile($writerProfile);

        }
    }


}
