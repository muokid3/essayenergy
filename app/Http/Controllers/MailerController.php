<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;


class MailerController extends Controller
{
    public function welcome_user($user, $password)
    {
        Mail::send('mailer.welcome_user', ['user' => $user, 'password' => $password], function ($message) use ($user) {
            $subject = "EssayEnergy Registration";
            $message->to($user->email, $user->name)->subject($subject);
        });
    }
}
