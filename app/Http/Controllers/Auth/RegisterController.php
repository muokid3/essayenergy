<?php

namespace App\Http\Controllers\Auth;

use App\AccountHistory;
use App\Customer;
use App\Http\Controllers\MailerController;
use App\Notifications\FreeCredit;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    protected $random_pass;


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/orders/new';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['user_group'] == 2){
            //issa writer
            return Validator::make($data, [
                'name' => 'required|string|max:255|unique:users',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ],[
                'email.unique' => 'Please enter your password to continue'
            ]);
        }else{
            return Validator::make($data, [
                'email' => 'required|string|email|max:255|unique:users',
            ],[
                'email.unique' => 'Please enter your password to continue'
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $customer  = new Customer();
        $customer->saveOrFail();

        if ($data['user_group'] == 2){
            //issa writer
            $user = User::create([
                'name' => $data['name'],
                'customer_id' => $customer->id,
                'email' => $data['email'],
                'user_group' => $data['user_group'],
                'password' => Hash::make($data['password']),
            ]);


            return $user;
        }else{
            $name = Carbon::now()->timestamp;
            $this->random_pass = $this->randomPassword();
            $user = User::create([
                'name' => $name,
                'customer_id' => $customer->id,
                'email' => $data['email'],
                'user_group' => $data['user_group'],
                'first_order_type' => $data['first_order_type'],
                'first_order_pages' => $data['first_order_pages'],
                'password' => Hash::make($this->random_pass),
            ]);

            //send customer email here
            (new MailerController())->welcome_user($user,$this->random_pass);


            //credit free amount
            $this->free_credit(20, $user);


            Session::flash("success", "Please complete the form to create and publish your order!");

            return $user;
        }

    }

    function free_credit ($amount, $user){
        $customer = $user->customer;

        $transaction = new AccountHistory();
        $transaction->customer_id = $user->customer_id;
        $transaction->amount = $amount;
        $transaction->previous_balance = $customer->balance;
        $transaction->transaction_type = 'CR';
        $transaction->source = 'EssayEnergy Offer';
        $transaction->external_trx_id = "N/A";
        $transaction->narration = "EssayEnergy offer on sign up";
        $transaction->saveOrFail();

        $customer->balance +=  $amount;
        $customer->update();

        $user->notify(new FreeCredit($amount));

    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

}
