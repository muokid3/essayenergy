<?php

namespace App\Http\Controllers;

use App\About;
use App\AccountHistory;
use App\Blog;
use App\Customer;
use App\DummyReview;
use App\Notifications\EssayPurchased;
use App\Notifications\FreeCredit;
use App\Notifications\PaymentReceived;
use App\Order;
use App\PayPalIPNMessage;
use App\PrivacyPolicy;
use App\Rating;
use App\SampleEssay;
use App\SampleEssayPayment;
use App\TermsAndCondition;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use Sample\PayPalClient;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->request = new Request();
        $this->returnStatus = "failed";

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $topBlogs = Blog::orderBy('id','desc')->limit(4)->get();


        return view('welcome')->with(['topBlogs'=>$topBlogs]);
    }

    public function how_it_works()
    {
        return view('how_it_works');
    }

    public function latest_orders()
    {
        $orders = Order::where('status_id',11)->orderBy('id', 'desc')->limit(6)->get();
        return view('latest_orders')->with(['orders'=>$orders]);
    }

    public function blog()
    {
        $blogPosts = Blog::orderBy('id','desc')->paginate(10);

        return view('blog')->with(['blogPosts'=>$blogPosts]);
    }

    public function get_blog($slug)
    {
        $blogpost = Blog::where('slug', $slug)->firstOrFail();
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();

        return view('blog_post')->with(['blogpost'=>$blogpost,'latest'=>$latest]);
    }

//    public function latest_reviews()
//    {
//        $reviews = DB::table('ratings')
//            ->join('users', 'ratings.rated_by', '=', 'users.id')
//            ->join('orders', 'ratings.order_id', '=', 'orders.id')
//            ->select('users.name', 'ratings.rating','ratings.order_id','ratings.created_at', 'ratings.review', 'orders.type_id',
//                'orders.pages', 'orders.topic', 'orders.discipline_id')
//            ->where('users.user_group','=',1)
//            ->limit(20)
//            ->get();
//
//        return view('latest_reviews')->with(['reviews'=>$reviews]);
//    }

    public function about()
    {
        $about = About::all();
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();


        return view('about')->with(['about'=>$about,'latest'=>$latest]);
    }

    public function latest_reviews()
    {
        $reviews = DummyReview::orderBy('created_at','desc')->get();
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();


        return view('latest_reviews')->with(['reviews'=>$reviews,'latest'=>$latest]);
    }


    public function terms(){

        $terms = TermsAndCondition::all();
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();


        return view('terms')->with(['terms'=>$terms,'latest'=>$latest]);
    }

    public function privacy(){

        $privacy = PrivacyPolicy::all();
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();


        return view('privacy')->with(['privacy'=>$privacy,'latest'=>$latest]);
    }

    public function sample_papers()
    {
        $sample_papers = SampleEssay::orderBy('created_at','desc')->paginate(20);
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();


        return view('sample_papers')->with(['sample_papers'=>$sample_papers,'latest'=>$latest]);
    }

    public function sample_paper($slug)
    {
        $sample_paper = SampleEssay::where('slug',$slug)->first();
        $latest = Blog::orderBy('id', 'desc')->limit(4)->get();

        if (is_null($sample_paper))
            abort(404);

        return view('sample_paper')->with(['sample_paper'=>$sample_paper,'latest'=>$latest]);
    }

    public function top_writers()
    {
        $topWriters = User::join('ratings', 'users.id', 'ratings.rated_user')
            ->join('writer_profiles', 'users.id', 'writer_profiles.user_id')
            ->where('users.user_group', 2)
            ->where('writer_profiles.approved', 1)
            ->selectRaw(DB::raw('users.id as id,  AVG(ratings.rating) as rating'))
            ->groupBy('id')
            ->orderBy('rating', 'desc')
            ->limit(12)
            ->get();
        return view('top_writers');
    }

    public function receive_ipn(Request $request)
    {
        $this->request = $request;
        $paypalIpnMessage = PayPalIPNMessage::where('txn_id', $this->request->txn_id)
                                            ->where('payment_status',$this->request->payment_status)
                                            ->orderBy('id','desc')
                                            ->first();

        //file_put_contents("request.log", $this->request);


        if ($this->request->receiver_email == "essayenergy18@gmail.com" && is_null($paypalIpnMessage)){
            DB::transaction(function () {
                $paypalIpnMessage = new PayPalIPNMessage();
                $paypalIpnMessage->business = $this->request->business;
                $paypalIpnMessage->receiver_email = $this->request->receiver_email;
                $paypalIpnMessage->txn_id = $this->request->txn_id;
                $paypalIpnMessage->parent_txn_id = $this->request->parent_txn_id;
                $paypalIpnMessage->payment_type = $this->request->payment_type;
                $paypalIpnMessage->txn_type = $this->request->txn_type;
                $paypalIpnMessage->item_name = $this->request->item_name;
                $paypalIpnMessage->mc_gross = $this->request->mc_gross;
                $paypalIpnMessage->mc_fee = $this->request->mc_fee;
                $paypalIpnMessage->mc_currency = $this->request->currency;
                $paypalIpnMessage->payment_date = $this->request->payment_date;
                $paypalIpnMessage->payment_status = $this->request->payment_status;
                $paypalIpnMessage->pending_reason = $this->request->pending_reason;
                $paypalIpnMessage->reason_code = $this->request->reason_code;
                $paypalIpnMessage->protection_eligibility = $this->request->protection_eligibility;
                $paypalIpnMessage->address_status = $this->request->address_status;
                $paypalIpnMessage->address_name = $this->request->address_name;
                $paypalIpnMessage->address_street = $this->request->address_street;
                $paypalIpnMessage->address_zip = $this->request->address_zip;
                $paypalIpnMessage->address_country_code = $this->request->address_country_code;
                $paypalIpnMessage->payer_id = $this->request->payer_id;
                $paypalIpnMessage->first_name = $this->request->first_name;
                $paypalIpnMessage->last_name = $this->request->last_name;
                $paypalIpnMessage->payer_email = $this->request->payer_email;
                $paypalIpnMessage->payer_status = $this->request->payer_status;
                $paypalIpnMessage->custom = $this->request->custom;
                $paypalIpnMessage->ipn_track_id = $this->request->ipn_track_id;


                $user = User::where('email', $this->request->custom)->first();

                Log::info("User top up for:".$user);

                //file_put_contents("test.log", $user);




                if ($paypalIpnMessage->saveOrFail()){

                        $customer = $user->customer;

                        $transaction = new AccountHistory();
                        $transaction->customer_id = $user->customer_id;
                        $transaction->amount = $this->request->mc_gross;
                        $transaction->previous_balance = $customer->balance;
                        $transaction->transaction_type = 'CR';
                        $transaction->source = 'Paypal';
                        $transaction->external_trx_id = $this->request->txn_id;
                        $transaction->narration = "PayPal top up. - ".$this->request->payment_status;
                        $transaction->saveOrFail();

                        $customer->balance +=  $this->request->mc_gross;
                        $customer->update();

                    Log::info("Top up received: ".$user);

                    //notify customer
                        $user->notify(new PaymentReceived($paypalIpnMessage->payment_status, $transaction));

                    $this->returnStatus = "success";
                }

            });
        }

        return $this->returnStatus;


    }

    public static function getSamplePaperOrder($orderId)
    {

        // 3. Call PayPal to get the transaction details
        $client = PayPalClient::client();
        $response = $client->execute(new OrdersGetRequest($orderId));
        /**
         *Enable the following line to print complete response as JSON.
         */
        //print json_encode($response->result);
        Log::info("##################### START #####################");
        Log::info(json_encode($response->result, JSON_PRETTY_PRINT));
        Log::info("##################### END #####################");

        $orderID = $response->result->id;
        $status = $response->result->status;
        $amount = $response->result->purchase_units[0]->amount->value;
        $currency = $response->result->purchase_units[0]->amount->currency_code;
        $customID = $response->result->purchase_units[0]->custom_id;
        $payerGivenName = $response->result->payer->name->given_name;
        $payerSurname = $response->result->payer->name->surname;
        $payerEmail = $response->result->payer->email_address;


        $customArray = explode("#",$customID);

        $essayID = $customArray[1];
        $userID = $customArray[3];



        $sampleEssayPayment = new SampleEssayPayment();
        $sampleEssayPayment->user_id = $userID;
        $sampleEssayPayment->essay_id = $essayID;
        $sampleEssayPayment->order_id = $orderID;
        $sampleEssayPayment->status = $status;
        $sampleEssayPayment->amount = $amount;
        $sampleEssayPayment->currency = $currency;
        $sampleEssayPayment->payer_given_name = $payerGivenName;
        $sampleEssayPayment->payer_surname = $payerSurname;
        $sampleEssayPayment->payer_email = $payerEmail;
        $sampleEssayPayment->saveOrFail();

        if ($status == "COMPLETED"){
            $sampleEssay = SampleEssay::find($essayID);

            if (!is_null($sampleEssay))
                auth()->user()->notify(new EssayPurchased($sampleEssay));
        }

        return;

    }

    public function download_sample_paper($_id){
        $samplePaper = SampleEssay::find($_id);

        if (is_null($samplePaper))
            abort(404);

        $payment = SampleEssayPayment::where('user_id',auth()->user()->id)->where('essay_id',$samplePaper->id)->first();

        if (is_null($payment))
            abort(401);

        $path =  Storage::disk('s3')->url($samplePaper->file);

            $data = Storage::disk('s3')->get($path);
            $getMimeType = Storage::disk('s3')->getMimetype($path);
            $newFileName = 'filename.pdf';
            $headers = [
                'Content-type' => $getMimeType,
                'Content-Disposition'=>sprintf('attachment; filename="%s"', $newFileName)
            ];
            return Response::make($data, 200, $headers);



        //return Storage::response($file);
        //return redirect($file);

    }

    public function register_user(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users,email'
        ]);

        $customer  = new Customer();
        $customer->saveOrFail();

        $name = Carbon::now()->timestamp;
        $random_pass = $this->randomPassword();

        $user = User::create([
            'name' => $name,
            'customer_id' => $customer->id,
            'email' => $request->email,
            'user_group' => 1,
            'password' => Hash::make($random_pass),
        ]);

        //credit free amount
        $this->free_credit(20, $user);

        Auth::login($user);

        //send customer email here
        (new MailerController())->welcome_user($user,$random_pass);


        return redirect('/');
    }

    public function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    function free_credit ($amount, $user){
        $customer = $user->customer;

        $transaction = new AccountHistory();
        $transaction->customer_id = $user->customer_id;
        $transaction->amount = $amount;
        $transaction->previous_balance = $customer->balance;
        $transaction->transaction_type = 'CR';
        $transaction->source = 'EssayEnergy Offer';
        $transaction->external_trx_id = "N/A";
        $transaction->narration = "EssayEnergy offer on sign up";
        $transaction->saveOrFail();

        $customer->balance +=  $amount;
        $customer->update();

        $user->notify(new FreeCredit($amount));

    }




}
