<?php

namespace App\Http\Controllers;

use App\AccountHistory;
use App\PayPalIPNMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function customer_payments()
    {
        $transactions = AccountHistory::where('customer_id', Auth::user()->customer_id)->orderBy('id', 'desc')->paginate(15);

        return view('payments.customer_payments')->with(['transactions' => $transactions]);
    }

    public function pay_btc(Request $request)
    {

        return view('payments.pay_btc')->with(['amount' => $request->amountbtc]);
    }
}
