<?php

namespace App\Http\Controllers;

use App\AccountHistory;
use App\AdditionalMaterial;
use App\Customer;
use App\DeadlinePricingRule;
use App\Escrow;
use App\Notifications\BidAccepted;
use App\Notifications\NewRevision;
use App\Notifications\OrderApproved;
use App\Order;
use App\OrderAssignment;
use App\OrderCancellation;
use App\Rating;
use App\Revision;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->request = new Request();
        $this->order = new Order();
        $this->writer = new User();

    }

    public function new_order()
    {
        if(Auth::user()->user_group == 2){
            return redirect('/dashboard');
        }else{
            return view('orders.new');
        }
    }

    public function create_order(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'type' => 'required',
            'pages' => 'required',
            'deadline' => 'required',
            'topic' => 'required',
            'discipline' => 'required',
            'service_type' => 'required',
            'citation' => 'required',
            'instructions' => 'required',
            'additional_info_file' => 'file|max:5000',

        ]);

        $order = new Order();


        DB::transaction(function () use($order){

            if ($this->request->hasFile('additional_info_file')) {



                $file = $this->request->file('additional_info_file');
                //get filename with extension
                $filenamewithextension = $file->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $file->getClientOriginalExtension();

                //filename to store
                $filenametostore = 'order_files/'.$filename.'_'.time().'.'.$extension;

                //Upload File to s3
                Storage::disk('s3')->put($filenametostore, fopen($file, 'r+'), 'public');



                //$file = $this->request->file('additional_info_file');
                //$destinationPath = 'uploads';

                //if ($file->move($destinationPath,Auth::user()->name.'-'.$file->getClientOriginalName())){

                    $date = Carbon::createFromFormat('m/d/Y h:i a', $this->request->deadline);
                    $citation = $this->request->citation == "Other" ? "Other - ".$this->request->citation_other : $this->request->citation;

                    $deadlineHours = $date->diffInHours(Carbon::now());

                    $deadline_pricing_rule = DeadlinePricingRule::where('min_hrs','<=',$deadlineHours)
                                                                    ->where('max_hrs','>=',$deadlineHours)
                                                                    ->first();

                    if (is_null($deadline_pricing_rule))
                        abort(500);


                    $order->customer_id = Auth::user()->customer_id;
                    $order->status_id = 3;
                    $order->deadline_pricing_rule_id = $deadline_pricing_rule->id;
                    $order->type_id = $this->request->type;
                    $order->pages = $this->request->pages;
                    $order->deadline = $date;
                    $order->topic = $this->request->topic;
                    $order->cost = $deadline_pricing_rule->price_per_page*$this->request->pages;
                    $order->discipline_id = $this->request->discipline;
                    $order->type_of_service = $this->request->service_type;
                    $order->citation = $citation;
                    $order->instructions = $this->request->instructions;
                    $order->file_path = $filenametostore;

                    $user = Auth::user();
                    $user->first_order_complete = 1;

                    if ($order->saveOrFail() && $user->update()){

                        Session::flash('success', 'Order has been placed successfully!');
                    }else{
                        Session::flash('error', 'An error occurred when placing the order.Please try again');
                    }


//                }else{
//                    Session::flash('error', 'An error occurred when uploading the file, please try again.');
//                }

            }else{

                $date = Carbon::createFromFormat('m/d/Y h:i a', $this->request->deadline);
                $citation = $this->request->citation == "Other" ? "Other - ".$this->request->citation_other : $this->request->citation;

                $deadlineHours = $date->diffInHours(Carbon::now());

                $deadline_pricing_rule = DeadlinePricingRule::where('min_hrs','<=',$deadlineHours)
                    ->where('max_hrs','>=',$deadlineHours)
                    ->first();

                if (is_null($deadline_pricing_rule))
                    abort(500);


                $order->customer_id = Auth::user()->customer_id;
                $order->status_id = 3;
                $order->deadline_pricing_rule_id = $deadline_pricing_rule->id;
                $order->type_id = $this->request->type;
                $order->pages = $this->request->pages;
                $order->deadline = $date;
                $order->topic = $this->request->topic;
                $order->cost = $deadline_pricing_rule->price_per_page*$this->request->pages;
                $order->discipline_id = $this->request->discipline;
                $order->type_of_service = $this->request->service_type;
                $order->citation = $citation;
                $order->instructions = $this->request->instructions;

                $user = Auth::user();
                $user->first_order_complete = 1;

                if ($order->saveOrFail() && $user->update()){
                    Session::flash('success', 'Order has been created successfully!');
                }else{
                    Session::flash('error', 'An error occurred when creating the order.Please try again');
                }

            }
        });

        return redirect('/orders/details/'.$order->id);

    }

    public function order($order_id)
    {
        $order = Order::find($order_id);

        if (is_null($order) || $order->customer_id != Auth::user()->customer_id){
            Session::flash('error', 'Order not found');
            abort(404);
        }

        switch ($order->status_id) {
            case 11: //published order. ready to be taken
                $page = "orders.published_order_details";
                break;
            case 3: //new order. take to reserve funds
                $page = "orders.new_order_details";
                break;
            case 5: //completed order
                $page = 'orders.completed_order_details';
                break;
            case 6: //in progress order
                $page = 'orders.ongoing_order_details';
                break;
            case 7: //submitted order
                $page = 'orders.submitted_order_details';
                break;
            case 8: //cancelled order
                $page = 'orders.cancelled_order_details';
                break;
            case 12: //order order with revision
                $page = 'orders.revision_order_details';
                break;
            default:
                $page = 'orders.new_order_details';

        }

        return view($page)->withOrder($order);

    }

    public function reserve_funds($order_id)
    {
        $order= Order::find($order_id);


        if (is_null($order) || $order->customer_id != Auth::user()->customer_id){
            Session::flash('error', 'Order not found');
            return redirect('/dashboard');
        }else{
            //check balance from wallet api here
            $walletBal = auth()->user()->customer->balance;

            if ($walletBal >= $order->cost){
                //has enough balance
                //add escrow entry and return success

                DB::transaction(function () use ($order) {

                    $order->status_id = 11; //published

                    $escrow = new Escrow();
                    $escrow->customer_id = Auth::user()->customer_id;
                    $escrow->order_id = $order->id;
                    $escrow->total_charge = $order->cost;

                    $custAccHistory = new AccountHistory();
                    $custAccHistory->customer_id = Auth::user()->customer_id;
                    $custAccHistory->amount = $order->cost;
                    $custAccHistory->previous_balance = Auth::user()->customer->balance;
                    $custAccHistory->transaction_type = "DR";
                    $custAccHistory->source = "Internal wallet";
                    $custAccHistory->external_trx_id = "N/A";
                    $custAccHistory->narration = "Payment reservation for order #".$order->id;

                    $customer = auth()->user()->customer;
                    $customer->balance = auth()->user()->customer->balance - $order->cost;


                    if ($order->update() && $escrow->saveOrFail() && $custAccHistory->saveOrFail()){
                        $customer->update();
                        //notify support people here
//                        $this->writer->notify(new BidAccepted($this->bid,$this->order,$this->writer));

                        Session::flash('success', 'Funds have been reserved successfully!');
                    }else{
                        Session::flash('error', 'An error occurred when reserving funds. Please try again');
                    }
                });
                return json_encode(["error" => false, "message" => "Funds reserved"]);

            }else{
                //return false with insufficient funds
                return json_encode(["error" => true, "message" => "Insufficient balance. Please Add Funds to your account to continue"]);

            }


        }
    }

    public function cancel_order(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required',

        ]);


        DB::transaction(function () {

            $request = (object)$_POST;

            $order = Order::find($request->order_id);
            $escrow = Escrow::where('customer_id', auth()->user()->customer_id)->where('order_id',$request->order_id)->first();



            if (is_null($order) || $order->customer_id != Auth::user()->customer_id) {
                Session::flash('error', 'Order not found');
                return redirect('/dashboard');
            }else{
                $orderCancellation = new OrderCancellation();
                $orderCancellation->order_id = $request->order_id;
                $orderCancellation->customer_id = Auth::user()->customer_id;
                $orderCancellation->reason = $request->reason;


                $order->status_id=8;

                if ($orderCancellation->saveOrFail() && $order->update()){
                    //return money back to wallet here
                    $escrow->delete();
                    Session::flash('success', 'Your order has been cancelled successfully.');
                }else{

                    Session::flash('message', 'An error occurred when cancelling your order. Please contact admin');
                }

                return redirect()->back();

            }

        });

        return redirect()->back();
    }

    public function request_revision(Request $request)
    {
        $this->validate($request, [
            'instructions' => 'required',

        ]);


        DB::transaction(function () {

            $request = (object)$_POST;

            $order = Order::find($request->order_id);


            if (is_null($order) || $order->customer_id != Auth::user()->customer_id) {
                Session::flash('error', 'Order not found');
                return redirect('/dashboard');
            }else{
                $revision = new Revision();
                $revision->order_id = $request->order_id;
                $revision->status_id = 1;
                $revision->instructions = $request->instructions;

                $orderAssignment = $order->assigned;
                $orderAssignment->status_id=12;


                $order->status_id=12;

                if ($revision->saveOrFail() && $order->update() && $orderAssignment->update()){
                    //notify suppport here
                    $orderAssignment->support->notify(new NewRevision($order));
                    Session::flash('success', 'Your revision request has been posted');
                }else{

                    Session::flash('message', 'An error occurred when posting your revision request. Please contact admin');
                }

                return redirect()->back();

            }

        });

        return redirect()->back();
    }

    public function my_orders()
    {
        //published orders, waiting for support
        $orders = Order::where('customer_id', auth()->user()->customer_id)
            ->orderBy('id', 'desc')
            ->paginate(15);
        return view('orders.my_orders')->withOrders($orders);
    }

    public function in_progress()
    {
        $orders = Order::where('customer_id', auth()->user()->customer_id)
            ->where('status_id', 6)
            ->orderBy('updated_at', 'desc')
            ->paginate(15);
        return view('orders.in_progress')->withOrders($orders);
    }

    public function completed()
    {
        $orders = Order::where('customer_id', auth()->user()->customer_id)
            ->where('status_id', 5)
            ->orderBy('updated_at', 'desc')
            ->paginate(15);
        return view('orders.completed')->withOrders($orders);
    }

    public function cancelled()
    {
        $orders = Order::where('customer_id', auth()->user()->customer_id)
            ->where('status_id', 8)
            ->orderBy('updated_at', 'desc')
            ->paginate(15);
        return view('orders.cancelled')->withOrders($orders);
    }

    public function mark_complete($order_id)
    {
        $order = Order::find($order_id);

        if (is_null($order) || $order->customer_id != Auth::user()->customer_id){
            Session::flash('error', 'Order not found');
            return redirect('/dashboard');
        }else{
            //move money from wallets


            DB::transaction(function () use ($order){

                $escrow = Escrow::where('customer_id', auth()->user()->customer_id)->where('order_id',$order->id)->first();



                $supportAccHistory = new AccountHistory();
                $supportAccHistory->customer_id = $order->assigned->support->customer_id;
                $supportAccHistory->amount = $escrow->total_charge;
                $supportAccHistory->previous_balance = $order->assigned->support->customer->balance;
                $supportAccHistory->transaction_type = "CR";
                $supportAccHistory->source = "Internal wallet";
                $supportAccHistory->external_trx_id = "N/A";
                $supportAccHistory->narration = "Payment credit for order #".$order->id;
                $supportAccHistory->saveOrFail();


                $supportCust = $order->assigned->support->customer;
                $supportCust->balance = $supportCust->balance+$escrow->total_charge;
                $supportCust->update();


                $systemAccHistory = new AccountHistory();
                $systemAccHistory->customer_id = 1;
                $systemAccHistory->amount = $escrow->total_charge;
                $systemAccHistory->previous_balance = Customer::find(1)->balance;
                $systemAccHistory->transaction_type = "CR";
                $systemAccHistory->source = "Internal wallet";
                $systemAccHistory->external_trx_id = "N/A";
                $systemAccHistory->narration = "Payment credit for order #".$order->id;
                $systemAccHistory->saveOrFail();


                $systemCust = Customer::find(1);
                $systemCust->balance = $systemCust->balance+$escrow->total_charge;
                $systemCust->update();



                $order->status_id = 5;
                $order->update();


                $orderAssignemnt = $order->assigned;
                $orderAssignemnt->customer_approved = 1;
                $orderAssignemnt->status_id = 5;
                $orderAssignemnt->update();


                $revision = Revision::where('order_id',$order->id)->orderBy('id','desc')->first();
                if (!is_null($revision)){
                    $revision->status_id=2;
                    $revision->update();
                }

                if ($escrow->delete()){
                    //notify support here
                    $order->assigned->support->notify(new OrderApproved($order));
                    Session::flash('success', 'Accepted successfully! You can now download your files');
                }else{
                    Session::flash('error', 'An fatal error occurred. Please try again');
                }

            });

            return redirect()->back();
        }
    }

    public function upload_additional(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'filesToUpload.*' => 'required|mimes:jpg,jpeg,png,bmp,pdf,doc,docx,xls,xlsx|max:2000'
        ],[
            'filesToUpload.*.required' => 'Please select at least one file',
            'filesToUpload.*.mimes' => 'Only jpg, jpeg, png, bmp, pdf, xls, xlsx, doc and docx files are allowed',
            'filesToUpload.*.max' => 'Sorry! Maximum allowed size for a file is 5MB',
        ]);


        DB::transaction(function () {

            if ($this->request->hasFile('filesToUpload')) {
                $files = $this->request->file('filesToUpload');
//                $destinationPath = 'uploads';

                foreach ($files as $file) {
//                    $file = $this->request->file('additional_info_file');
                    //get filename with extension
                    $filenamewithextension = $file->getClientOriginalName();

                    //get filename without extension
                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                    //get file extension
                    $extension = $file->getClientOriginalExtension();

                    //filename to store
                    $filenametostore = 'order_files/'.$filename.'_'.time().'.'.$extension;

                    //Upload File to s3
                    Storage::disk('s3')->put($filenametostore, fopen($file, 'r+'), 'public');



                    //$file->move($destinationPath,Auth::user()->name.'-'.$file->getClientOriginalName());

                    $additionalMaterial= new AdditionalMaterial();
                    $additionalMaterial->customer_id = Auth::user()->customer_id;
                    $additionalMaterial->order_id = $this->request->order_id;
                    $additionalMaterial->file_path = $filenametostore;// 'uploads/'.Auth::user()->name.'-'.$file->getClientOriginalName();

                    $additionalMaterial->saveOrFail();

                }

                Session::flash('success', 'Additional content has been uploaded successfully');

            }else{

                Session::flash('error', 'Please select one or more files');


            }
        });

        return redirect()->back();

    }

    public function extend_deadline(Request $request)
    {
        $this->validate($request, [
            'new_deadline' => 'required',

        ]);

        DB::transaction(function () {

            $request = (object)$_POST;

            $order = Order::find($request->order_id);


            if (is_null($order) || $order->customer_id != Auth::user()->customer_id) {
                Session::flash('error', 'Order not found');
                return redirect('/dashboard');
            }else{

                $date = Carbon::createFromFormat('m/d/Y h:i a', $request->new_deadline);

                Session::flash('success', 'Your order deadline has been changed successfully.');

                $order->deadline = $date;
                $order->update();

                return redirect()->back();

            }

        });

        return redirect()->back();
    }

//    public function download_order($order_id){
//
//        $order = Order::find($order_id);
//
//        if (is_null($order) || $order->customer_id != Auth::user()->customer_id){
//            Session::flash('error', 'Order not found');
//            return redirect('/dashboard');
//        }else{
//            $pdf = App::make('dompdf.wrapper');
//            $pdf->loadHTML($order->assigned->submission );
//            //return $pdf->stream();
//            return $pdf->download('Order #'.$order_id.'.pdf');
//        }
//
//
//    }

    public function rate_support(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'rating' => 'required',
            'review' => 'required',
        ],[
            'rating.required' => 'Please provide a rating',
            'review.required' => 'Please provide a review'
        ]);

        DB::transaction(function () {
            $rating = new Rating();
            $rating->rated_user = $this->request->rated_user;
            $rating->rated_by = Auth::user()->id;
            $rating->order_id = $this->request->order_id;
            $rating->rating = $this->request->rating;
            $rating->review = $this->request->review;


            $orderAssignment = OrderAssignment::find($this->request->order_assignment_id);
            $orderAssignment->customer_done_review = 1;

            if ($rating->saveOrFail() && $orderAssignment->update()){
                Session::flash('success', 'Thank you for your rating');
            }else{
                Session::flash('error', 'An error occurred when rating, please try again!');
            }

        });

        return redirect()->back();
    }




























    public function republish($order_id)
    {
        $order = Order::find($order_id);

        if (is_null($order) || $order->customer_id != Auth::user()->customer_id){
            Session::flash('error', 'Order not found');
            return redirect('/dashboard');
        }else{
            $order->status = 1;
            $order->update();
            return redirect()->back();
        }
    }



//    public function bid_details($bid_id)
//    {
//        $bid = Bid::find($bid_id);
//        $order =$bid->order;
//
//        if (is_null($bid) || is_null($order) || $order->customer_id != Auth::user()->id){
//            Session::flash('error', 'Bid not found');
//            return redirect('/dashboard');
//        }else{
//            return view('orders.bid_details')->withOrder($order)->withBid($bid);
//        }
//    }








}
