<?php

namespace App\Http\Controllers;

use App\Notifications\BidPlaced;
use App\Notifications\OrderCompleted;
use App\Order;
use App\Rating;
use App\SampleEssay;
use App\User;
use App\WriterProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;


class WriterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->request = new Request();
        $this->order = new Order();

    }

    public function create_profile(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'description' => 'required',
            'citation' => 'required',
            'discipline' => 'required',
            'native_language' => 'required',
            'uni_name' => 'required',
            'course_name' => 'required',
            'degree' => 'required',
            'grad_year' => 'required',
            'company_name' => 'required',
            'field_of_work' => 'required',
            'position' => 'required',
            'duration' => 'required',

        ]);


        DB::transaction(function () {

            $seralizedCitation = serialize($this->request->citation);
            $seralizedDiscipline = serialize($this->request->discipline);

            $writerProfile = new WriterProfile();
            $writerProfile->user_id = Auth::user()->id;
            $writerProfile->first_name = $this->request->first_name;
            $writerProfile->last_name = $this->request->last_name;
            $writerProfile->gender = $this->request->gender;
            $writerProfile->country = $this->request->country;
            $writerProfile->description = $this->request->description;
            $writerProfile->citation_ids = $seralizedCitation;
            $writerProfile->discipline_ids = $seralizedDiscipline;
            $writerProfile->native_language =$this->request->native_language;
            $writerProfile->uni_name = $this->request->uni_name;
            $writerProfile->course_name = $this->request->course_name;
            $writerProfile->degree = $this->request->degree;
            $writerProfile->grad_year = $this->request->grad_year;
            $writerProfile->company_name = $this->request->company_name;
            $writerProfile->field_of_work = $this->request->field_of_work;
            $writerProfile->position = $this->request->position;
            $writerProfile->duration = $this->request->duration;

            if ($writerProfile->saveOrFail()){
                Session::flash('success', 'Profile has been saved successfully!');
            }else{
                Session::flash('error', 'An error occurred when saving the profile, please try again!');
            }


        });

        return redirect('/dashboard');

    }


    public function update_profile(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'description' => 'required',
            'citation' => 'required',
            'discipline' => 'required',
            'native_language' => 'required',
            'uni_name' => 'required',
            'course_name' => 'required',
            'degree' => 'required',
            'grad_year' => 'required',
            'company_name' => 'required',
            'field_of_work' => 'required',
            'position' => 'required',
            'duration' => 'required',

        ]);


        DB::transaction(function () {

            $seralizedCitation = serialize($this->request->citation);
            $seralizedDiscipline = serialize($this->request->discipline);

            $writerProfile = WriterProfile::where('user_id', Auth::user()->id)->first();
            $writerProfile->first_name = $this->request->first_name;
            $writerProfile->last_name = $this->request->last_name;
            $writerProfile->gender = $this->request->gender;
            $writerProfile->country = $this->request->country;
            $writerProfile->description = $this->request->description;
            $writerProfile->citation_ids = $seralizedCitation;
            $writerProfile->discipline_ids = $seralizedDiscipline;
            $writerProfile->native_language =$this->request->native_language;
            $writerProfile->uni_name = $this->request->uni_name;
            $writerProfile->course_name = $this->request->course_name;
            $writerProfile->degree = $this->request->degree;
            $writerProfile->grad_year = $this->request->grad_year;
            $writerProfile->company_name = $this->request->company_name;
            $writerProfile->field_of_work = $this->request->field_of_work;
            $writerProfile->position = $this->request->position;
            $writerProfile->duration = $this->request->duration;

            if ($writerProfile->save()){
                Session::flash('success', 'Profile has been updated successfully!');
            }else{
                Session::flash('error', 'An error occurred when updating the profile, please try again!');
            }


        });

        return redirect('/writer/profile');

    }


    public function create_sample_essay(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'sample_essay' => 'required',
        ]);

        DB::transaction(function () {


            $sampleEssay = new SampleEssay();
            $sampleEssay->user_id = Auth::user()->id;
            $sampleEssay->essay = $this->request->sample_essay;


            if ($sampleEssay->saveOrFail()){
                Session::flash('success', 'Sample essay has been saved successfully! Please wait as we review and approve your profile');
            }else{
                Session::flash('error', 'An error occurred when saving the sample essay, please try again!');
            }


        });

        return redirect('/dashboard');

    }

    public function update_sample_essay(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'sample_essay' => 'required',
        ]);

        DB::transaction(function () {

            $sampleEssay = SampleEssay::where('user_id',Auth::user()->id)->first();
            $sampleEssay->user_id = Auth::user()->id;
            $sampleEssay->essay = $this->request->sample_essay;


            if ($sampleEssay->update()){
                Session::flash('success', 'Sample essay has been updated');
            }else{
                Session::flash('error', 'An error occurred when updating the sample essay, please try again!');
            }


        });

        return redirect('/dashboard');

    }

    public function bid($order_id)
    {
        $order = Order::find($order_id);

        if (is_null($order) || ($order->status != 1 && is_null(Bid::where('order_id',$order_id)->where('bidder_id',Auth::user()->id)->first()))){
            Session::flash('error', 'Order not available');
            return redirect('/dashboard');
        }else{

            return view('writer.order_details')->withOrder($order);
        }
    }

    public function place_bid(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'bid_amount' => 'required',
            'welcome_message' => 'required',
        ]);

        $this->order = Order::find($this->request->order_id);

        if ($this->request->bid_amount != $this->order ->budget){
//        if ($this->request->bid_amount < $order->minimum_bid()){
            Session::flash('error', "You can only bid $".$this->order->budget." for this order");
//            Session::flash('error', "You can not bid below $".$order->minimum_bid()." for this order");
        }else{

            DB::transaction(function () {

                $bid = new Bid();
                $bid->bidder_id = Auth::user()->id;
                $bid->order_id = $this->request->order_id;
                $bid->bid_amount = $this->request->bid_amount;
                $bid->welcome_message = $this->request->welcome_message;


                if ($bid->saveOrFail()){

                    $customer = User::find($this->order->customer_id);

                    $customer->notify(new BidPlaced($bid, Auth::user()));

                    Session::flash('success', 'Your bid has been placed successfully');
                }else{
                    Session::flash('error', 'An error occurred when placing the bid, please try again!');
                }

            });

        }

        return redirect()->back();
    }

    public function remove_bid($bid_id)
    {
        $bid = Bid::find($bid_id);

        if (is_null($bid)){
            Session::flash('error', 'Bid not found');
            return redirect('/dashboard');
        }else{
            $bid->delete();
            return redirect()->back();
        }
    }

    public function update_bid(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'bid_amount' => 'required',
        ]);

        $order = Order::find($this->request->order_id);

        if ($this->request->bid_amount < $order->minimum_bid()){
            Session::flash('error', "You can not bid below $".$order->minimum_bid()." for this order");
        }else{

            DB::transaction(function () {

                $bid = Bid::find($this->request->bid_id);
                $bid->bid_amount = $this->request->bid_amount;


                if ($bid->update()){
                    Session::flash('success', 'Your bid has been updated successfully');
                }else{
                    Session::flash('error', 'An error occurred when updating the bid, please try again!');
                }

            });

        }

        return redirect()->back();
    }



    public function update_submission(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'order_submission' => 'required',
        ]);

        if ($this->request->btn_action == "save_submit"){

            DB::transaction(function () {

                $bid = Bid::find($this->request->bid_id);
                $order = Order::find($bid->order_id);
                $bid->submission = $this->request->order_submission;
                $bid->writer_finished = 1;

                if ($bid->update()){
                    //notify customer
                    $order->customer->notify(new OrderCompleted($order));
                    Session::flash('success', 'Your order has been saved and submitted to the customer successfully');
                }else{
                    Session::flash('error', 'An error occurred when updating the submission, please try again!');
                }

            });

        }else{
            DB::transaction(function () {

                $bid = Bid::find($this->request->bid_id);
                $bid->submission = $this->request->order_submission;


                if ($bid->update()){
                    Session::flash('success', 'Updated successfully');
                }else{
                    Session::flash('error', 'An error occurred when updating the submission, please try again!');
                }

            });
        }
        return redirect()->back();
    }


    public function in_progress()
    {
        $orders = Order::join('bids','orders.bid_id','bids.id')
            ->select('orders.*')
            ->where('bids.bidder_id', Auth::user()->id)
            ->where('bids.status', 1)
            ->where('orders.status', 2)
            ->orderBy('bids.updated_at', 'desc')
            ->paginate(15);
        return view('writer.in_progress')->withOrders($orders);
    }

    public function bids()
    {
        $bids = Bid::join('orders','bids.order_id','orders.id')
            ->select('bids.*','orders.id as order_id', 'orders.topic','orders.deadline','orders.pages')
            ->where('bids.bidder_id', Auth::user()->id)
            ->where('bids.status', 0)
            ->where('orders.status', 1)
            ->orderBy('bids.updated_at', 'desc')
            ->paginate(15);
        return view('writer.bids')->withBids($bids);
    }

    public function completed()
    {
        $orders = Order::join('bids','orders.bid_id','bids.id')
            ->select('orders.*')
            ->where('bids.bidder_id', Auth::user()->id)
            ->where('bids.status', 1)
            ->where('orders.status', 3)
            ->orderBy('bids.updated_at', 'desc')
            ->paginate(15);
        return view('writer.completed')->withOrders($orders);
    }


    public function rate_customer(Request $request)
    {
        $this->request = $request;
        $this->validate($request, [
            'rating' => 'required',
            'review' => 'required',
        ],[
            'rating.required' => 'Please provide a rating',
            'review.required' => 'Please provide a review'
        ]);

            DB::transaction(function () {
                $rating = new Rating();
                $rating->rated_user = $this->request->rated_user;
                $rating->rated_by = Auth::user()->id;
                $rating->order_id = $this->request->order_id;
                $rating->rating = $this->request->rating;
                $rating->review = $this->request->review;


                $bid = Bid::find($this->request->bid_id);
                $bid->writer_done_review = 1;

                if ($rating->saveOrFail() && $bid->update()){
                    Session::flash('success', 'Thank you for your rating');
                }else{
                    Session::flash('error', 'An error occurred when rating, please try again!');
                }

            });

        return redirect()->back();
    }

    public function profile()
    {
        $writerProfile = WriterProfile::where('user_id',Auth::user()->id)->first();
        if (is_null($writerProfile)){
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('dashboard');
        }else{
            return view('writer.profile')->withProfile($writerProfile);

        }
    }


}
