<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->request = new Request();

    }

    public function index()
    {
        $reviews = Rating::where('rated_user',Auth::user()->id)->paginate(15);
        return view('reviews.index')->withReviews($reviews);
    }
}
