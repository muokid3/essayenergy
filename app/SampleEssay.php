<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SampleEssay extends Model
{
    public function discipline()
    {
        return $this->belongsTo('App\Discipline', 'discipline_id');
    }
}
