<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CryptoPayment extends Model
{
    protected $table='crypto_payments';
    protected $primaryKey='paymentID';
}
