<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderApproved extends Notification implements ShouldQueue
{
    use Queueable;
    protected $order;


    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Order Accepted!')
            ->greeting('Hello!')
            ->line('Your order #'.$this->order->id.' was accepted and approved by the customer!')
            ->line('Please write a review and give a rating to the customer')
            ->action('Write Review ', url('/order/'.$this->order->id))
            ->line('Thank you for using Essay Energy!');
    }

    public function toDatabase($notifiable)
    {
        return [
            'order' => $this->order,
            'support' => $notifiable,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'order' => $this->order,
            'support' => $notifiable,
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
