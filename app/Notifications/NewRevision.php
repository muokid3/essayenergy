<?php

namespace App\Notifications;

use App\Order;
use App\OrderAssignment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewRevision extends Notification implements ShouldQueue
{
    use Queueable;
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param OrderAssignment $orderAssignment
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('New revision!')
            ->greeting('Hello!')
            ->line('Your submitted order #'.$this->order->id.' has a revision!')
            ->line('Please action on this and re-submit the order again')
            ->action('View Revision ', url('/order/'.$this->order->id))
            ->line('Thank you for using Essay Energy!');
    }

    public function toDatabase($notifiable)
    {
        return [
            'order' => $this->order,
            'support' => $notifiable,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'order' => $this->order,
            'support' => $notifiable,
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
