<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FreeCredit extends Notification implements ShouldQueue
{
    use Queueable;

    protected $amount;

    /**
     * Create a new notification instance.
     *
     * @param $amount
     */
    public function __construct($amount)
    {
        $this->amount = $amount;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You have received $'.$this->amount.' on your account!')
            ->greeting('Congratulations!')
            ->line('Your account has been credited $'.$this->amount.' for signing up on EssayEnergy ')
            ->line('You can use this amount to place an order for your essay or research paper. Click the button below to get started')
            ->action('Place an Order', "essayenergy.com/orders/new")
            ->line('Thank you for using Essay Energy!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
