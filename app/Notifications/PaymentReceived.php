<?php

namespace App\Notifications;

use App\AccountHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaymentReceived extends Notification implements ShouldQueue
{
    use Queueable;
    protected $transaction;
    protected $status;


    /**
     * Create a new notification instance.
     *
     * @param $status
     * @param AccountHistory $transaction
     */
    public function __construct($status, AccountHistory $transaction)
    {
        //
        $this->transaction = $transaction;
        $this->status = $status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Payment '.$this->status)
            ->greeting('Hello customer!')
            ->line('Your payment of $'.$this->transaction->amount.' has been '.$this->status)
            ->line('Click the button below to see your transaction history')
            ->action('Transaction History', "essayenergy.com/payments")
            ->line('Thank you for using Essay Energy!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
