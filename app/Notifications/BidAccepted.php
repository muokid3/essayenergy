<?php

namespace App\Notifications;

use App\Bid;
use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BidAccepted extends Notification implements ShouldQueue
{
    use Queueable;

    protected $bid;
    protected $order;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Bid $bid, Order $order, User $user)
    {
        $this->bid = $bid;
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Bid Accepted!')
            ->greeting('Congratulations '.$this->user->name.'!')
            ->line('Your bid for order #'.$this->order->id.' ('.$this->order->topic.') has been accepted!')
            ->action('Start working', url('/bid/'.$this->order->id))
            ->line('Thank you for using Essay Energy!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
