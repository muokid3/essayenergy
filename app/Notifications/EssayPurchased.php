<?php

namespace App\Notifications;

use App\SampleEssay;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EssayPurchased extends Notification
{
    use Queueable;
    protected $sampleEssay;

    /**
     * Create a new notification instance.
     *
     * @param SampleEssay $sampleEssay
     */
    public function __construct(SampleEssay $sampleEssay)
    {
        $this->sampleEssay = $sampleEssay;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Essay Purchased!')
            ->greeting('Hello!')
            ->line('You have successfully purchased the essay "'.$this->sampleEssay->question.'".')
            ->line('Please note that this essay is meant to be used for research purposes only.')
            ->action('View and download ', url('/sample_papers/'.$this->sampleEssay->slug))
            ->line('Thank you for using Essay Energy!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
