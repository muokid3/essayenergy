<?php

namespace App\Notifications;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;


class OrderCompleted extends Notification implements ShouldQueue
{
    use Queueable;
    protected $order;
    protected $user;


    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param User $user
     */
    public function __construct(Order $order, User $user)
    {
        $this->order = $order;
        $this->user = $user;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your order is complete!')
            ->greeting('Hello!')
            ->line('Your order #'.$this->order->id.' ('.$this->order->topic.') has been completed by the writer!')
            ->line('Please acknowledge receipt to download your order and release funds to the writer')
            ->action('View Order', url('/orders/details/'.$this->order->id))
            ->line('Thank you for using Essay Energy!');
    }

    public function toDatabase($notifiable)
    {
        return [
            'order' => $this->order,
            'support' => $this->user,
            'customer' => $notifiable
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'order' => $this->order,
            'support' => $this->user,
            'customer' => $notifiable
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
