<?php

namespace App\Notifications;

use App\Order;
use App\OrderAssignment;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;


class OrderAccepted extends Notification
{
    use Queueable;

    protected $order;
    protected $user;
    protected $orderAssignment;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param User $user
     * @param OrderAssignment $orderAssignment
     */
    public function __construct(Order $order, User $user,OrderAssignment $orderAssignment)
    {
        $this->orderAssignment = $orderAssignment;
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast','mail'];

    }

    public function toDatabase($notifiable)
    {
        return [
            'order' => $this->order,
            'orderAssignment' => $this->orderAssignment,
            'support' => $this->user,
            'customer' => $notifiable
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'order' => $this->order,
            'orderAssignment' => $this->orderAssignment,
            'support' => $this->user,
            'customer' => $notifiable
        ]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Order '.$this->order->id.' is being processed')
            ->greeting('Your order #'.$this->order->id.' has been assigned to a writer and is currently being processed')
            ->action('View Order', url('/orders/details/'.$this->order->id))
            ->line('You will be notified once the order is ready')
            ->line('Thank you for using Essay Energy!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
