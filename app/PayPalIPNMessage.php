<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayPalIPNMessage extends Model
{
    //
    protected $casts = [
        'mc_gross' => 'double',
    ];
}
