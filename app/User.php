<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'customer_id', 'email','user_group','first_order_type','first_order_pages', 'first_order_deadline', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function reserved()
    {
        return DB::table('escrows')
            ->where('customer_id', '=',$this->customer_id)
            ->sum('total_charge');
    }

    public function avg_rating(){
        return DB::table('ratings')
            ->where('rated_user', '=',$this->id)
            ->avg('rating');
    }
}
