<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WriterProfile extends Model
{
    public function citation_ids()
    {
        return unserialize($this->citation_ids);
    }

    public function discipline_ids()
    {
        return unserialize($this->discipline_ids);
    }
}
