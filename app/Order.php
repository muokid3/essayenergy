<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 */
class Order extends Model
{
    public function type()
    {
        return $this->belongsTo('App\Type', 'type_id');
    }

    public function discipline()
    {
        return $this->belongsTo('App\Discipline', 'discipline_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function assigned()
    {
        return $this->hasOne('App\OrderAssignment', 'order_id');
    }

    public function order_status()
    {
        return $this->belongsTo('App\StatusCode', 'status_id');
    }

    public function revision()
    {
        return Revision::where('order_id', $this->id)->where('status_id',1)->orderBy('id','desc')->first();
    }

    public function minimum_bid()
    {

        $diff = Carbon::parse(date("d-m-Y H:i:s", strtotime($this->deadline)))->diffInHours(Carbon::now());

        if ($diff <= 12){
            return 11;
        } elseif (12 < $diff && $diff <= 24){
            return 10;
        } elseif (24 < $diff && $diff <= 36){
            return 9;
        } elseif (36 < $diff && $diff <= 48){
            return 8;
        } elseif (48 < $diff && $diff <= 60){
            return 7;
        } elseif (60 < $diff && $diff <= 72){
            return 6;
        } elseif (72 < $diff && $diff <= 84){
            return 5;
        } else{
            return 4;
        }
    }

    public function recommended_bid()
    {

        $diff = Carbon::parse(date("d-m-Y H:i:s", strtotime($this->deadline)))->diffInHours(Carbon::now());

        if ($diff <= 12){
            return 14;
        } elseif (12 < $diff && $diff <= 24){
            return 13;
        } elseif (24 < $diff && $diff <= 36){
            return 11;
        } elseif (36 < $diff && $diff <= 48){
            return 10;
        } elseif (48 < $diff && $diff <= 60){
            return 8;
        } elseif (60 < $diff && $diff <= 72){
            return 7;
        } elseif (72 < $diff && $diff <= 84){
            return 6;
        } else{
            return 5;
        }
    }
}
