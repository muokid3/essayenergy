<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAssignment extends Model
{
    public function support()
    {
        return $this->belongsTo('App\User', 'support_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
