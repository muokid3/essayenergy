<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <title>Essay Energy</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description" content="At essayenergy, the customer is king. we ensure that once you have submitted your order, its our duty to follow the instructions to the letter, submit a plagiarism free paper and deliver on time with strict adherence to academic writing standards.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    <link href="{{url('/form-select2/select2.css')}}" type="text/css" rel="stylesheet"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />


    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


</head>
<body>

<div class="super_container">


    @include('layouts.header')

    <!-- Home -->

    <div class="home">
        <div class="home_slider_container">

            <!-- Home Slider -->
            <div class="owl-carousel owl-theme home_slider">

                <!-- Home Slider Item -->
                <div class="owl-item">
                    <div class="home_slider_background" style="background-image:url(images/home_slider_1.jpg)"></div>
                    <div class="home_slider_content">
                        <div class="container">
                            <div class="row">
                                <div class="col text-center">
                                    <div class="home_slider_title">Find a Writer for Your Essay.</div>
                                    <div class="home_slider_subtitle">High quality and timely papers</div>

                                    @guest
                                        <div class="home_slider_form_container hidden-xs">
                                        <form  action="{{url('/register')}}" method="post"  style="height: 46px;background: #FFFFFF;border-radius: 3px;"
                                              class="hidden-xs d-flex flex-lg-row flex-column align-items-center justify-content-between">
                                            {{ csrf_field() }}
                                            <div class="d-flex flex-row align-items-center justify-content-start">
                                                <input type="hidden" name="user_group"  value="1">
                                                <input type="email" name="email" class="home_search_input" placeholder="E-Mail" required="required">
                                                @if ($errors->has('email'))
                                                    @php
                                                        echo "<script type='text/javascript'>
                                                            $(document).ready(function(){
                                                                $('#loginModal').modal('show');
                                                            });
                                                        </script>";
                                                    @endphp
                                                @endif

                                                <select id="specialDude" required style="width: 33%" class="dropdown_item_select home_search_input" name="first_order_type">
                                                    <option value="">Type of paper</option>
                                                    @foreach(\App\Type::all() as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach

                                                </select>

                                                <input type="number" name="first_order_pages" min="1" class="home_search_input" placeholder="Number of pages" required="required">

                                            </div>
                                            <button type="submit" class="btn btn-success">Continue</button>
                                        </form>
                                    </div>
                                    @else
                                        <a href="{{url('orders/new')}}" class="btn btn-success" style="margin-top: 20px">Place your order now</a>

                                    @endauth

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>


    </div>

    <!-- Welcome To Essay Energy -->

    <div class="features">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class=" text-center">
                        <h2 class="section_title">Welcome To Essay Energy</h2>
                        <div class="section_subtitle">
                            <p>
                                At essayenergy, the customer is king. we ensure that once you have submitted your order, its our duty to follow the instructions to the letter, submit a plagiarism free paper and deliver on time with strict adherence to academic writing standards.                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row features_row">

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="images/icon_1.png" alt=""></div>
                        <h3 class="feature_title">Place your Order</h3>
                        <div class="feature_text"><p>Fill out the order form with the order instructions using the subtitles given</p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="images/icon_2.png" alt=""></div>
                        <h3 class="feature_title">Reserve Money</h3>
                        <div class="feature_text"><p>Reserve the funds for your order with escrow</p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="images/icon_3.png" alt=""></div>
                        <h3 class="feature_title">Communicate with writers</h3>
                        <div class="feature_text"><p>Engage your writer via chat and choose a writer of your preference</p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="images/icon_4.png" alt=""></div>
                        <h3 class="feature_title">Track your order to completion</h3>
                        <div class="feature_text"><p>Feel free to request for a draft of your work and review the paper progress.
                                Pay for the paper and download it, request for a free revision if necessary!</p></div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Guaranteed Quality Work -->
    <div class="counter">
        <div class="counter_background" style="background-image:url(images/counter_background.jpg)"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="counter_content">
                        <h2 class="counter_title">Guaranteed Quality Work</h2>
                        <div class="counter_text">
                            <p>
                                Enjoy top quality essay help by veteran specialists. Our services guarantee you will be satisfied after our team of experts will help you make your essay error-free, with accurate grammar, spellings, context, guidelines and everything in between. If you are looking to submit the perfect essay then get the best essay help to achieve your goal.

                            </p>
                        </div>

                        <!-- Milestones -->

                        <div class="milestones d-flex flex-md-row flex-column align-items-center justify-content-between">

                            <!-- Milestone -->
                            <div class="milestone">
                                <div class="milestone_counter" data-end-value="{{config('constants.orders')}}">0</div>
                                {{--<div class="milestone_counter" data-end-value="{{\App\Order::where('status_id', 5)->count()}}">0</div>--}}
                                <div class="milestone_text">Completed Orders</div>
                            </div>

                            <!-- Milestone -->
                            {{--<div class="milestone">--}}
                                {{--<div class="milestone_counter" data-end-value="{{\App\WriterProfile::where('approved',1)->count()}}">0</div>--}}
                                {{--<div class="milestone_text">Qualified Writers</div>--}}
                            {{--</div>--}}

                            {{--<!-- Milestone -->--}}
                            {{--<div class="milestone">--}}
                                {{--<div class="milestone_counter" data-end-value="4">0</div>--}}
                                {{--<div class="milestone_text">Online Writers</div>--}}
                            {{--</div>--}}

                        </div>
                    </div>
                </div>
            </div>

            <div class="counter_form">
                <div class="row fill_height">
                    <div class="col fill_height">
                        @guest

                            <form class="counter_form_content d-flex flex-column align-items-center justify-content-center" action="{{url('/register')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="user_group"  value="1">
                                <div class="counter_form_title">place order now</div>

                                <input type="email" name="email" class="counter_input" placeholder="E-Mail" required="required">
                                @if ($errors->has('email'))
                                    {{--@php--}}
                                        {{--echo "<script type='text/javascript'>--}}
                                            {{--$(document).ready(function(){--}}
                                                {{--$('#loginModal').modal('show');--}}
                                            {{--});--}}
                                        {{--</script>";--}}
                                    {{--@endphp--}}
                                    @section('scripts')
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#loginModal').modal('show');
                                            });
                                        </script>
                                    @endsection
                                @endif
                                {{--{!! Form::select('first_order_type', \App\Type::pluck('name', 'id'), null,--}}
                                 {{--['class' => 'dropdown_item_select counter_input', 'id'=>'specialDude','required']) !!}--}}

                                <select id="specialDude" required  class="dropdown_item_select counter_input" name="first_order_type">
                                    <option value="">Type of paper</option>
                                    @foreach(\App\Type::all() as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach

                                </select>


                                <input type="number" name="first_order_pages" min="1" class="counter_input" placeholder="Number of pages" required="required">

                                <button type="submit" class="btn btn-success btn-block">Get Started Now</button>
                            </form>

                        @else
                            <div style="text-align: center;" class="counter_form_content d-flex flex-column align-items-center justify-content-center">
                                <div class="counter_form_title">place order now</div>

                                <a href="{{url('orders/new')}}" class="btn btn-success align-center" style="margin-top: 20px">Place your order now</a>

                            </div>

                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- why choose us -->

    <div class="features" style="background-image:url(images/courses_background.jpg)">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class=" text-center">
                        <h2 class="section_title">Why Choose Us</h2>
                        <div class="section_subtitle">
                            <p>
                                At essayenergy, the customer is king. we ensure that once you have submitted your order, its our duty to follow the instructions
                                to the letter, submit a plagiarism free paper and deliver on time with strict adherence to academic writing standards.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row features_row">

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="img/support.png" height="55" alt=""></div>
                        <h3 class="feature_title">Direct Chat</h3>
                        <div class="feature_text"><p>Direct chart with your writer/ support.</p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="img/shield.png" height="55" alt=""></div>
                        <h3 class="feature_title">Top Quality</h3>
                        <div class="feature_text"><p>Top quality papers completed by experts in the specific fields.</p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="img/affordable.png" height="55" alt=""></div>
                        <h3 class="feature_title">Affordable</h3>
                        <div class="feature_text"><p>We offer the lowest market prices</p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-3 feature_col">
                    <div class="feature text-center trans_400" style="height: 100%">
                        <div class="feature_icon"><img src="img/confidential.png" height="55" alt=""></div>
                        <h3 class="feature_title">Confidentiality</h3>
                        <div class="feature_text"><p>Customer confidentiality and security</p></div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- our Guarantee -->
    <div class="counter">
        <div class="counter_background" style="background-image:url(images/counter_background.jpg)"></div>
        <div class="container">

            <div class="row">
                <div class="col">
                    <div class=" text-center">
                        <h2 class="section_title" style="padding-top: 93px; color: #FFFFFF">Our Guarantee</h2>
                        <div class="section_subtitle">
                            <p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div class="counter_content" style="padding-top: 0px">
                        <h3 style="color: #ffffff;">Top Quality Work</h3>
                        <div class="counter_text">
                            <p>
                                At essay energy, we have a team of professionals working day and night to ensure you get the help you need with your essay. We have employed on part-time basis a team of high school and college tutors, post graduate students as well as university professors who working with us round the clock.

                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="counter_content" style="padding-top: 0px">
                        <h3 style="color: #ffffff;">Timely Task Delivery</h3>
                        <div class="counter_text">
                            <p>
                                We understand the need to deliver that assignment, report, blackboard discussion in 30minutes. At essay energy, we ensure that you turn in your paper on time without compromising the quality of the paper. Our team of professionals will ensure that your paper is sent to you on time to allow you submit it on time.

                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="counter_content" style="padding-top: 0px">
                        <h3 style="color: #ffffff;">Money Back Guarantee</h3>
                        <div class="counter_text">
                            <p>
                                In every single transaction, both parties must receive value. As such, we have you covered. If the essay does not meet your expectations in terms of the quality of work done, the agreed upon deadline or failure to follow instructions, you get your money back! Isn’t that awesome?

                            </p>
                        </div>
                    </div>
                </div>
                


            </div>
        </div>
    </div>

    <!-- Disciplines -->

    <div class="features" style="background-image:url(images/courses_background.jpg)">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class=" text-center">
                        <h2 class="section_title">Disciplines</h2>
                        <div class="section_subtitle">
                            <p>
                                 We have a well talented team that has specialised in the following disciplines
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row features_row">

                <!-- Features Item -->
                <div class="col-lg-4 feature_col">
                    <div class="feature trans_400" style="height: 100%;">
                        <h3 class="feature_title">Business management</h3>
                        <div class="feature_text">
                            <p>
                                We understand the need for your business management paper to be completed by a professional who understands the need to use proper business terms and language. We also cannot underscore the need for research and presentation of findings. Our business management writing team will ensure that your paper adheres to every requirement in your instructions and that all the deliverables are accurate.

                            </p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-4 feature_col">
                    <div class="feature trans_400" style="height: 100%;">
                        <h3 class="feature_title">Finance</h3>
                        <div class="feature_text">
                            <p>
                                Finance assignments, reports and data gathering tasks are often tedious and involves massive research and presentations. At essayenergy, we have a team of dedicated finance writers who share the passion. For your finance task, we will ensure that your paper is completed and reviewed by our editors before it is submitted.
                            </p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-4 feature_col">
                    <div class="feature trans_400" style="height: 100%;">
                        <h3 class="feature_title">English literature</h3>
                        <div class="feature_text">
                            <p>
                                If literature is not your thing or you are just not in the mood, we will do the reading for you! We will respond to those prompts and write your analysis essay.  We have a huge team of English literature writers ready to take up that tasks and enjoy writing that piece. Leave it to us and lets do us!
                            </p></div>
                    </div>
                </div>

            </div>

            <div class="row features_row">

                <!-- Features Item -->
                <!-- Features Item -->
                <div class="col-lg-4 feature_col">
                    <div class="feature trans_400" style="height: 100%;">
                        <h3 class="feature_title">Accounting</h3>
                        <div class="feature_text">
                            <p>
                                Our team considers the need to have your accounting task completed by a certified professional. We have taken it upon ourselves to engage a team of certified public accountants and secretaries who work for us on a part time basis. Therefore, at any particular time, we have experts on standby waiting for that task!
                            </p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-4 feature_col">
                    <div class="feature trans_400" style="height: 100%;">
                        <h3 class="feature_title">Nursing and Medicine</h3>
                        <div class="feature_text">
                            <p>
                                At any particular point, we have nurses and doctors on study leave. We have come up with a structure that allows qualified nurses and doctors sign up with us and earn as they study. For that reason, your particular requirement will be completed by a partner who really understands the task.
                            </p></div>
                    </div>
                </div>

                <!-- Features Item -->
                <div class="col-lg-4 feature_col">
                    <div class="feature trans_400" style="height: 100%;">
                        <h3 class="feature_title">History</h3>
                        <div class="feature_text">
                            <p>
                                History has always been fun reading and writing for most of our writers. We have a team that loves history across the board. Once you’ve engaged us, you will have your history paper written by an expert who enjoys writing the particular subject.
                            </p></div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Latest News -->

    <div class="news">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <h2 class="section_title">Blog</h2>
                        <div class="section_subtitle"><p>Check out our blog to keep yourself posted</p></div>
                    </div>
                </div>
            </div>
            <div class="row news_row">
                <div class="col-lg-7 news_col">

                    <!-- News Post Large -->
                    <div class="news_post_large_container">
                        <div class="news_post_large">
                            <div class="news_post_image">
                                <img class="media-object" src="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($topBlogs->first()->image_link)}}" alt="">
                            </div>
                            <div class="news_post_large_title"><a href="{{url('blog/'.$topBlogs->first()->slug)}}">{{$topBlogs->first()->title}}</a></div>
                            <div class="news_post_meta">
                                <ul>
                                    <li><a href="#">{{$topBlogs->first()->author->name}}</a></li>
                                    <li><a href="#">{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($topBlogs->first()->created_at)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}</a></li>
                                </ul>
                            </div>
                            <div class="news_post_text">
                                <p>
                                   {!! substr($topBlogs->first()->post,0,250) !!} ...
                                </p>
                            </div>
                            <div class="news_post_link"><a href="{{url('blog/'.$topBlogs->first()->slug)}}">read more</a></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 news_col">
                    <div class="news_posts_small">

                    @if (sizeof($topBlogs) > 1)
                        @foreach($topBlogs->slice(1) as $blog)
                            <!-- News Posts Small -->
                                <div class="news_post_small">
                                    <div class="news_post_small_title"><a href="{{url('blog/'.$blog->slug)}}">{{$blog->title}}</a></div>
                                    <div class="news_post_meta">
                                        <ul>
                                            <li><a href="#">{{$blog->author->name}}</a></li>
                                            <li><a href="#">{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($blog->created_at)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                        @endforeach
                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('layouts.footer')

    <!-- Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sign in to continue to Essay Energy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 mx-auto">
                                <div class="account-wall">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">

                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Password" autofocus required>
                                        @if ($errors->has('password'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                        @if ($errors->has('email'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('email') }}</strong></span>
                                        @endif

                                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                                            Sign in</button>

                                        <a href="{{url('/password/reset')}}"  class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>




</div>

<link rel="stylesheet" href="{{url('/build/css/bootstrap-datetimepicker.min.css')}}">

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>




<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="{{ url('/form-select2/select2.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ url('/js/moment.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<script src="{{url('build/js/bootstrap-datetimepicker.min.js')}}"></script>

@yield('scripts')



{{--<script src="{{ url('/js/datepicker.min.js') }}"></script> <!-- All Scripts -->--}}
{{--<script src="{{ url('/js/datetimepicker.js') }}"></script> <!-- All Scripts -->--}}



<script>
    $(document).ready(function () {
        $('select:not(#specialDude)').removeClass('form-control');
        $('select:not(#specialDude)').select2({width: '100%'});

//        $(function() {
//            $( ".datetimepicker" ).datetimepicker({
//
//            });
//        });

        $('.datetimepicker').datetimepicker({
            minDate: moment()
        });


//        $(function () {
//            $("#deadlineDate").datepicker({
//                //viewMode: 'years',
//                format: 'DD-MM-YYYY'
//                //maxDate: moment().subtract(18, 'years')
//            });
//        });
    });
</script>

</body>

</html>