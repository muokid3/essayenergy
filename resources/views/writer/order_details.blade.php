@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Order Details
                                <small>Place your bid on this order to get started</small>

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->

                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <button class="btn btn-success btn-lg btn-block" type="button">ORDER ID <span class="badge">#{{$order->id}}</span></button>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <span class="btn btn-primary btn-lg btn-block " type="button">Status <span class="badge">{{$order->order_status()}}</span></span>
                                </div>

                            </ul>


                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                    <div class="card">
                                        <div class="body">

                                           <div class="row">
                                               <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12"
                                               style="border-right: 1px solid #eee;">
                                                   <img src="{{url('/inner/images/user.png')}}" class="img-rounded" alt="Customer" width="75%" >
                                                   <br>
                                                   <h5>
                                                       #{{$order->customer->name}}
                                                   </h5>

                                                   @switch(round($order->customer->avg_rating(),0))
                                                   @case(1)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(2)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(3)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(4)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(5)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   @break
                                                   @endswitch


                                                   <h5>Completed Orders: {{\App\Order::where('customer_id', $order->customer->id)->where('status',3)->count()}}</h5>

                                               </div>

                                               <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12"
                                                    style="border-right: 1px solid #eee;">

                                                   <table class='table no-border' border="0" cellpadding="0" cellspacing="0">
                                                       <tbody>
                                                            <tr>
                                                                <td>Type of paper: </td>
                                                                <td>{{$order->type->name}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Topic: </td>
                                                                <td>{{$order->topic}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Pages: </td>
                                                                <td>{{$order->pages}} ({{$order->pages*275}} words)</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Discipline: </td>
                                                                <td>{{$order->discipline->discipline}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Type of service: </td>
                                                                <td>{{$order->type_of_service}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Format or citation style: </td>
                                                                <td>{{$order->citation}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Deadline: </td>
                                                                <td>
                                                                    {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                         ->format(' jS \\ F, Y h:i A')}}
                                                                    <br>

                                                                    {!!
                                                                        \Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline))) > \Carbon\Carbon::now()
                                                                            ?  "<span style=\"color: #4D953C\">" : "<span style=\"color: red\">"


                                                                    !!}

                                                                    ({{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))->diffForHumans()}})
                                                                    </span>
                                                                </td>
                                                            </tr>


                                                       </tbody>
                                                   </table>

                                               </div>

                                               <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12"
                                                    style="border-right: 1px solid #eee; ">
                                                       <h4 style="text-align: center;">
                                                           <u>Paper Instructions</u>
                                                       </h4>
                                                   <br>

                                                   <div id="module" class="container">
                                                       <p class="collapse" id="collapseExample" aria-expanded="false">
                                                           {!!  $order->instructions !!}
                                                       </p>
                                                       <a role="button" class="collapsed" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                       </a>
                                                   </div>

                                               </div>

                                               <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12"
                                                        style="text-align: center;">
                                                   <h4>
                                                       <u>Additional Material (Files)</u>
                                                   </h4>

                                                   <br>

                                                   <ul style="text-align: left">
                                                       <li>
                                                           <a href="{{'/'.$order->file_path}}" target="_blank">
                                                               {{str_replace("uploads/".Auth::user()->name."-", "", $order->file_path)}}
                                                           </a>

                                                       </li>
                                                       @foreach(\App\AdditionalMaterial::where('order_id',$order->id)->orderBy('id', 'asc')->get() as $additional)
                                                           <li>
                                                               <a href="{{'/'.$additional->file_path}}" target="_blank">
                                                                   {{str_replace("uploads/".Auth::user()->name."-", "", $additional->file_path)}}
                                                               </a>

                                                           </li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                           </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8 col-lg-8 col-md-8 col-xs-12">
                                            @if(is_null(\App\Bid::where('bidder_id', Auth::user()->id)->where('order_id', $order->id)->first()))
                                                        <div class="card">
                                                            <div class="body">
                                                                <div style="text-align: center;">
                                                                    <h3>Place a Bid</h3>
                                                                </div>

                                                                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/bid/place') }}">
                                                                    {{ csrf_field() }}


                                                                    <input type="hidden" name="order_id" value="{{$order->id}}">

                                                                    <div class="row clearfix" style="margin-top: 10px">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <div class="form-line">

                                                                                <label >My bid per page ($)<span style="color: red">*</span></label>
                                                                                <input type="number" min="0" name="bid_amount" step=".01" class="form-control">
                                                                                <p style="color: red">{{$errors->first("bid_amount") }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="row clearfix">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <p class="font-bold col-teal">Recommended Bid: ${{$order->budget}}</p>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row clearfix" style="margin-top: 10px">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <label>Welcome Message</label><span style="color: red">*</span>
                                                                            <textarea name="welcome_message"  rows="5" class="form-control" placeholder="Please write a brief welcome message to the customer" aria-required="true"></textarea>
                                                                            <p style="color: red">{{$errors->first("welcome_message") }}</p>

                                                                        </div>
                                                                    </div>


                                                                    <div class="row clearfix" style="margin-top: 10px">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <b id="totalBid">Total Price: $0</b>
                                                                        </div>
                                                                    </div>


                                                                    <div class="row clearfix">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <div>
                                                                                <button type="submit" class="btn btn-success btn-block waves-effect">Submit Bid</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>

                                                            </div>
                                                        </div>

                                            @else
                                                        <div class="card">
                                                            <div class="body">
                                                                <div class="row">

                                                                    <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                                                        <div style="font-size: 13px; padding: 6px 12px;font-weight: 400; line-height: 1.42857143;background : #eee; color:  #3e434a;">
                                                                            My Bid Per Page: <strong>${{\App\Bid::where('bidder_id', Auth::user()->id)->where('order_id', $order->id)->first()->bid_amount}}</strong>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                                                        <div style="font-size: 13px; padding: 6px 12px;font-weight: 400; line-height: 1.42857143;background : #eee; color:  #3e434a;">
                                                                            My Total Price: <strong>${{\App\Bid::where('bidder_id', Auth::user()->id)->where('order_id', $order->id)->first()->bid_amount*$order->pages}}</strong>
                                                                        </div>
                                                                    </div>

                                                                    @if($order->status == 1)
                                                                        <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
                                                                            <button type="button" class="btn btn-lg btn-block btn-danger waves-effect m-r-20" onclick="remove_bid('{{\App\Bid::where('bidder_id', Auth::user()->id)->where('order_id', $order->id)->first()->id}}');" >REMOVE MY BID</button>
                                                                        </div>

                                                                        {{--<div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">--}}
                                                                            {{--<button type="button" class="btn btn-lg btn-block btn-warning waves-effect m-r-20" data-toggle="collapse" data-target="#collapseExample"--}}
                                                                                    {{--aria-expanded="false"--}}
                                                                                    {{--aria-controls="collapseExample">CHANGE MY BID</button>--}}
                                                                        {{--</div>--}}


                                                                        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 collapse" id="collapseExample">
                                                                            <form class="well" method="POST" action="{{ url('/bid/update') }}">
                                                                                {{csrf_field()}}
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" style="margin-bottom: 0px">
                                                                                        <div class="form-group" style="margin-bottom: 0px">
                                                                                            <div class="form-line">
                                                                                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                                                                                <input type="hidden" name="bid_id" value="{{\App\Bid::where('bidder_id', Auth::user()->id)->where('order_id', $order->id)->first()->id}}">
                                                                                                <input type="text" name="bid_amount" value="{{\App\Bid::where('bidder_id', Auth::user()->id)->where('order_id', $order->id)->first()->bid_amount}}" class="form-control" placeholder="Amount per page ($)">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin-bottom: 0px">
                                                                                        <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">UPDATE</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>

                                                        @switch($order->status)
                                                            @case(1)
                                                                {{--ongoing bidding--}}
                                                                <textarea readonly name="preview" id="ckeditor"></textarea>
                                                                @break
                                                            @case(2)
                                                                {{--in progress--}}
                                                                    <div class="col-sm-12 text-right" style="font-size: 13px; padding: 6px 12px;font-weight: 400; line-height: 1.42857143;background : #eee; color:  #3e434a;">
                                                                        Last Updated: <strong>{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->bid ? $order->bid->updated_at : "1973-00-00 00:00:00" )))
                                                                                                                         ->format(' jS \\ F, Y h:i A')}}</strong>
                                                                    </div>
                                                                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{url('/order/submission/update')}}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="bid_id" value="{{$order->bid ? $order->bid->id : 0 }}">
                                                            <textarea name="order_submission" id="ckeditor">
                                                                            {!! $order->bid ? $order->bid->submission : "" !!}
                                                                        </textarea>

                                                            <div class="row clearfix" style="margin-top: 2%">
                                                                @if(!($order->bid ? $order->bid->writer_finished : false))

                                                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                                                        <button type="submit" name="btn_action" value="save" class="btn btn-primary btn-block waves-effect">SAVE</button>
                                                                    </div>

                                                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pull-right">
                                                                        <button type="submit" name="btn_action" value="save_submit"  class="btn btn-success btn-lg btn-block">SAVE AND SUBMIT</button>
                                                                    </div>
                                                                @else
                                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                        <div class="btn-group" role="group">
                                                                            <span class="btn btn-info"> <span class="fa fa-small fa-check"></span> Order submitted</span>

                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </form>
                                                                @break
                                                            @case(3)
                                                                {{--completed--}}
                                                                <div class="row">
                                                                    <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">

                                                                        @switch($order->bid->writer_done_review)
                                                                            @case(1)
                                                                            {{--he already reviewed, show his rating and review--}}
                                                                            <div class="card">
                                                                                <div class="body">

                                                                                    <div class="row clearfix" style="margin-top: 10px">
                                                                                        <div class="col-md-12 col-md-offset-1">
                                                                                            <blockquote>
                                                                                                <p>{{\App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first() ?
                                                                                                \App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first()->review : ""}}</p>
                                                                                                <footer><div class="form-group form-float">

                                                                                                        <cite title="Source Title">
                                                                                                            <label>Your Rating</label>
                                                                                                        </cite>

                                                                                                        @php
                                                                                                            $stars = \App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first() ?
                                                                                                                        \App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first()->rating : 0;
                                                                                                        @endphp

                                                                                                        @switch($stars)
                                                                                                            @case(1)
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                            @break
                                                                                                            @case(2)
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                            @break
                                                                                                            @case(3)
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                            @break
                                                                                                            @case(4)
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star"></span>
                                                                                                            @break
                                                                                                            @case(5)
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                                <span class="fa fa-star" style="color: orange;"></span>
                                                                                                            @break
                                                                                                        @endswitch

                                                                                                    </div>
                                                                                                </footer>
                                                                                            </blockquote>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                        @break
                                                                        @default
                                                                         {{--he hasn't revied, show review form--}}
                                                                            <div class="card">
                                                                                <div class="body">
                                                                                    <h3>Rating and Review</h3>

                                                                                    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{url('/writer/customer/rate')}}">
                                                                                        {{ csrf_field() }}

                                                                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                                                                        <input type="hidden" name="rated_user" value="{{$order->customer->id}}">
                                                                                        <input type="hidden" name="bid_id" value="{{$order->bid->id}}">
                                                                                        <div class="row clearfix" style="margin-top: 10px">
                                                                                            <div class="col-md-6 col-md-offset-1">
                                                                                                <div class="form-group form-float">
                                                                                                    <label>Your Rating</label>

                                                                                                    <div class="star-rating">
                                                                                                        <input id="star-5" type="radio" name="rating" value="5">
                                                                                                        <label for="star-5" title="5 stars">
                                                                                                            <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                                                                                        </label>
                                                                                                        <input id="star-4" type="radio" name="rating" value="4">
                                                                                                        <label for="star-4" title="4 stars">
                                                                                                            <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                                                                                        </label>
                                                                                                        <input id="star-3" type="radio" name="rating" value="3">
                                                                                                        <label for="star-3" title="3 stars">
                                                                                                            <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                                                                                        </label>
                                                                                                        <input id="star-2" type="radio" name="rating" value="2">
                                                                                                        <label for="star-2" title="2 stars">
                                                                                                            <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                                                                                        </label>
                                                                                                        <input id="star-1" type="radio" name="rating" value="1">
                                                                                                        <label for="star-1" title="1 star">
                                                                                                            <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                                                                                        </label>
                                                                                                    </div>

                                                                                                    <p style="color: red">{{$errors->first("rating") }}</p>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row clearfix" style="margin-top: 10px">
                                                                                            <div class="col-md-6 col-md-offset-1">
                                                                                                <div style="margin-right: -15px; margin-left: -15px;">
                                                                                                    <label>Your Review</label>
                                                                                                    <textarea name="review"  rows="5" class="form-control" placeholder="Please write a brief review about your experience with the writer" aria-required="true"></textarea>
                                                                                                </div>
                                                                                                <p style="color: red">{{$errors->first("review") }}</p>

                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="row clearfix">
                                                                                            <div class="col-md-6 col-md-offset-1">
                                                                                                <div style="margin-right: -15px; margin-left: -15px;">
                                                                                                    <button type="submit" class="btn btn-success btn-block waves-effect">Submit Rating</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    </form>

                                                                                </div>
                                                                            </div>
                                                                        @endswitch

                                                                        <div class="card">
                                                                                <div class="body">

                                                                                    <h3 style="text-align: center">{{$order->topic}}</h3>

                                                                                    <textarea readonly name="preview" id="ckeditor">
                                                                                                {!! $order->bid->submission !!}
                                                                                    </textarea>

                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                                @break
                                                            @default
                                                                {{--cancelled--}}
                                                        @endswitch

                                            @endif
                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
                                            {{--chat here--}}
                                            @include('layouts.writer_chat')

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>







@endsection


    @section('css')
        <style>



            .star-rating {
                direction: rtl;
                display: inline-block;
                padding: 20px
            }

            .star-rating label:before,
            .star-rating label:after {
                display: none;
            }

            .star-rating label {
                color: #bbb;
                font-size: 18px;
                padding: 0;
                cursor: pointer;
                -webkit-transition: all .3s ease-in-out;
                transition: all .3s ease-in-out
            }

            .star-rating label:hover,
            .star-rating label:hover~label,
            .star-rating input[type=radio]:checked~label {
                color: #f2b600
            }








            #module {
                width: 100%;
            }

            #module p.collapse[aria-expanded="false"] {
                display: block;
                height: 40px !important;
                overflow: hidden;
            }

            #module p.collapsing[aria-expanded="false"] {
                height: 40px !important;
            }

            #module a.collapsed:after  {
                content: '+ Show More';
            }

            #module a:not(.collapsed):after {
                content: '- Show Less';
            }

        </style>
    @endsection

    @section('scripts')

        <script>


            $('input[name="rating"]').change(function(e) { // Select the radio input group

                // This returns the value of the checked radio button
                // which triggered the event.
                console.log( $(this).val() );

            });

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });

                $('input[name=bid_amount]').keyup(function(){
                    $('#totalBid').text("Total Price: $" +this.value*'{{$order->pages}}' );
                });



            });

            function makeFileList() {
                var input = document.getElementById("filesToUpload");
                var ul = document.getElementById("fileList");
                while (ul.hasChildNodes()) {
                    ul.removeChild(document.getElementById('fileList').firstChild);
                }
                for (var i = 0; i < input.files.length; i++) {
                    var li = document.createElement("li");
                    li.innerHTML = input.files[i].name;
                    ul.appendChild(li);
                }
                if(!ul.hasChildNodes()) {
                    var li = document.createElement("li");
                    li.innerHTML = 'No Files Selected';
                    ul.appendChild(li);
                }
            }

            function remove_bid(bidID){
                bootbox.confirm("Are you sure you want to remove your bid for this order? ", function(result) {
                    if(result) {
                        $.ajax({
                            url: '/bid/remove/' + bidID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

        </script>


        <!-- Ckeditor -->
        <script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>


    @endsection