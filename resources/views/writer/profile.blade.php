@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Profile</h2>
            </div>

            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif




            <div class="row clearfix">




                <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" style="padding: 20px" data-color="green" id="wizard">
                            <form action="{!! url('/writer/update_profile') !!}" method="post">
                            {!! csrf_field() !!}
                            <!--        You can switch " data-color="blue" "  with one of the next bright colors: "green", "orange", "red", "purple"             -->

                                <div class="wizard-header">
                                    <h3 class="wizard-title">
                                        {{Auth::user()->name}}
                                    </h3>
                                    <h5>Here is your Essay Energy Profile</h5>
                                </div>
                                <div class="wizard-navigation">
                                    <ul>
                                        <li><a href="#details" data-toggle="tab">Personal Details</a></li>
                                        <li><a href="#education" data-toggle="tab">Education</a></li>
                                        <li><a href="#experience" data-toggle="tab">Experience</a></li>
                                    </ul>
                                </div>

                                <div class="tab-content">
                                    <div class="tab-pane" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="info-text"> </h4>
                                            </div>

                                            <div class="col-sm-6" style="border-right: solid #eee">

                                                <div class="col-sm-12 ">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">First Name <span style="color: red">*</span></label>
                                                        <input type="text" required name="first_name" value="{{$profile->first_name}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("first_name") }}</div>

                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Last Name<span style="color: red">*</span></label>
                                                        <input type="text" required name="last_name" value="{{$profile->last_name}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("last_name") }}</div>

                                                    </div>
                                                </div>

                                                <div class="col-sm-12 ">
                                                    <label >Gender <span style="color: red">*</span></label>
                                                    <input name="gender" type="radio" value="Male" {{$profile->gender == 'Male' ? 'checked' : ''}} class="with-gap" id="male" />
                                                    <label for="male">Male</label>
                                                    <input name="gender" type="radio" value="Female" {{$profile->gender == 'Female' ? 'checked' : ''}} id="female" class="with-gap" />
                                                    <label for="female">Female</label>
                                                </div>


                                                <div class="col-sm-12 ">
                                                    <div class="form-group form2" style="margin-top: 0px">
                                                        <label class="control-label">Country<span style="color: red">*</span></label>
                                                        <select name="country" required class="select2 form-control">
                                                            <option value="{{$profile->country}}" title="{{$profile->country}}" selected>{{$profile->country}}</option>
                                                            <option value="Afghanistan" title="Afghanistan">Afghanistan</option>
                                                            <option value="Åland Islands" title="Åland Islands">Åland Islands</option>
                                                            <option value="Albania" title="Albania">Albania</option>
                                                            <option value="Algeria" title="Algeria">Algeria</option>
                                                            <option value="American Samoa" title="American Samoa">American Samoa</option>
                                                            <option value="Andorra" title="Andorra">Andorra</option>
                                                            <option value="Angola" title="Angola">Angola</option>
                                                            <option value="Anguilla" title="Anguilla">Anguilla</option>
                                                            <option value="Antarctica" title="Antarctica">Antarctica</option>
                                                            <option value="Antigua and Barbuda" title="Antigua and Barbuda">Antigua and Barbuda</option>
                                                            <option value="Argentina" title="Argentina">Argentina</option>
                                                            <option value="Armenia" title="Armenia">Armenia</option>
                                                            <option value="Aruba" title="Aruba">Aruba</option>
                                                            <option value="Australia" title="Australia">Australia</option>
                                                            <option value="Austria" title="Austria">Austria</option>
                                                            <option value="Azerbaijan" title="Azerbaijan">Azerbaijan</option>
                                                            <option value="Bahamas" title="Bahamas">Bahamas</option>
                                                            <option value="Bahrain" title="Bahrain">Bahrain</option>
                                                            <option value="Bangladesh" title="Bangladesh">Bangladesh</option>
                                                            <option value="Barbados" title="Barbados">Barbados</option>
                                                            <option value="Belarus" title="Belarus">Belarus</option>
                                                            <option value="Belgium" title="Belgium">Belgium</option>
                                                            <option value="Belize" title="Belize">Belize</option>
                                                            <option value="Benin" title="Benin">Benin</option>
                                                            <option value="Bermuda" title="Bermuda">Bermuda</option>
                                                            <option value="Bhutan" title="Bhutan">Bhutan</option>
                                                            <option value="Bolivia, Plurinational State of" title="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option>
                                                            <option value="Bonaire, Sint Eustatius and Saba" title="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                                            <option value="Bosnia and Herzegovina" title="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                            <option value="Botswana" title="Botswana">Botswana</option>
                                                            <option value="Bouvet Island" title="Bouvet Island">Bouvet Island</option>
                                                            <option value="Brazil" title="Brazil">Brazil</option>
                                                            <option value="British Indian Ocean Territory" title="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                            <option value="Brunei Darussalam" title="Brunei Darussalam">Brunei Darussalam</option>
                                                            <option value="Bulgaria" title="Bulgaria">Bulgaria</option>
                                                            <option value="Burkina Faso" title="Burkina Faso">Burkina Faso</option>
                                                            <option value="Burundi" title="Burundi">Burundi</option>
                                                            <option value="Cambodia" title="Cambodia">Cambodia</option>
                                                            <option value="Cameroon" title="Cameroon">Cameroon</option>
                                                            <option value="Canada" title="Canada">Canada</option>
                                                            <option value="Cape Verde" title="Cape Verde">Cape Verde</option>
                                                            <option value="Cayman Islands" title="Cayman Islands">Cayman Islands</option>
                                                            <option value="Central African Republic" title="Central African Republic">Central African Republic</option>
                                                            <option value="Chad" title="Chad">Chad</option>
                                                            <option value="Chile" title="Chile">Chile</option>
                                                            <option value="China" title="China">China</option>
                                                            <option value="Christmas Island" title="Christmas Island">Christmas Island</option>
                                                            <option value="Cocos (Keeling) Islands" title="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                            <option value="Colombia" title="Colombia">Colombia</option>
                                                            <option value="Comoros" title="Comoros">Comoros</option>
                                                            <option value="Congo" title="Congo">Congo</option>
                                                            <option value="Congo, the Democratic Republic of the" title="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                                                            <option value="Cook Islands" title="Cook Islands">Cook Islands</option>
                                                            <option value="Costa Rica" title="Costa Rica">Costa Rica</option>
                                                            <option value="Côte d'Ivoire" title="Côte d'Ivoire">Côte d'Ivoire</option>
                                                            <option value="Croatia" title="Croatia">Croatia</option>
                                                            <option value="Cuba" title="Cuba">Cuba</option>
                                                            <option value="Curaçao" title="Curaçao">Curaçao</option>
                                                            <option value="Cyprus" title="Cyprus">Cyprus</option>
                                                            <option value="Czech Republic" title="Czech Republic">Czech Republic</option>
                                                            <option value="Denmark" title="Denmark">Denmark</option>
                                                            <option value="Djibouti" title="Djibouti">Djibouti</option>
                                                            <option value="Dominica" title="Dominica">Dominica</option>
                                                            <option value="Dominican Republic" title="Dominican Republic">Dominican Republic</option>
                                                            <option value="Ecuador" title="Ecuador">Ecuador</option>
                                                            <option value="Egypt" title="Egypt">Egypt</option>
                                                            <option value="El Salvador" title="El Salvador">El Salvador</option>
                                                            <option value="Equatorial Guinea" title="Equatorial Guinea">Equatorial Guinea</option>
                                                            <option value="Eritrea" title="Eritrea">Eritrea</option>
                                                            <option value="Estonia" title="Estonia">Estonia</option>
                                                            <option value="Ethiopia" title="Ethiopia">Ethiopia</option>
                                                            <option value="Falkland Islands (Malvinas)" title="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                            <option value="Faroe Islands" title="Faroe Islands">Faroe Islands</option>
                                                            <option value="Fiji" title="Fiji">Fiji</option>
                                                            <option value="Finland" title="Finland">Finland</option>
                                                            <option value="France" title="France">France</option>
                                                            <option value="French Guiana" title="French Guiana">French Guiana</option>
                                                            <option value="French Polynesia" title="French Polynesia">French Polynesia</option>
                                                            <option value="French Southern Territories" title="French Southern Territories">French Southern Territories</option>
                                                            <option value="Gabon" title="Gabon">Gabon</option>
                                                            <option value="Gambia" title="Gambia">Gambia</option>
                                                            <option value="Georgia" title="Georgia">Georgia</option>
                                                            <option value="Germany" title="Germany">Germany</option>
                                                            <option value="Ghana" title="Ghana">Ghana</option>
                                                            <option value="Gibraltar" title="Gibraltar">Gibraltar</option>
                                                            <option value="Greece" title="Greece">Greece</option>
                                                            <option value="Greenland" title="Greenland">Greenland</option>
                                                            <option value="Grenada" title="Grenada">Grenada</option>
                                                            <option value="Guadeloupe" title="Guadeloupe">Guadeloupe</option>
                                                            <option value="Guam" title="Guam">Guam</option>
                                                            <option value="Guatemala" title="Guatemala">Guatemala</option>
                                                            <option value="Guernsey" title="Guernsey">Guernsey</option>
                                                            <option value="Guinea" title="Guinea">Guinea</option>
                                                            <option value="Guinea-Bissau" title="Guinea-Bissau">Guinea-Bissau</option>
                                                            <option value="Guyana" title="Guyana">Guyana</option>
                                                            <option value="Haiti" title="Haiti">Haiti</option>
                                                            <option value="Heard Island and McDonald Islands" title="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                                                            <option value="Holy See (Vatican City State)" title="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                            <option value="Honduras" title="Honduras">Honduras</option>
                                                            <option value="Hong Kong" title="Hong Kong">Hong Kong</option>
                                                            <option value="Hungary" title="Hungary">Hungary</option>
                                                            <option value="Iceland" title="Iceland">Iceland</option>
                                                            <option value="India" title="India">India</option>
                                                            <option value="Indonesia" title="Indonesia">Indonesia</option>
                                                            <option value="Iran, Islamic Republic of" title="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                            <option value="Iraq" title="Iraq">Iraq</option>
                                                            <option value="Ireland" title="Ireland">Ireland</option>
                                                            <option value="Isle of Man" title="Isle of Man">Isle of Man</option>
                                                            <option value="Israel" title="Israel">Israel</option>
                                                            <option value="Italy" title="Italy">Italy</option>
                                                            <option value="Jamaica" title="Jamaica">Jamaica</option>
                                                            <option value="Japan" title="Japan">Japan</option>
                                                            <option value="Jersey" title="Jersey">Jersey</option>
                                                            <option value="Jordan" title="Jordan">Jordan</option>
                                                            <option value="Kazakhstan" title="Kazakhstan">Kazakhstan</option>
                                                            <option value="Kenya" title="Kenya">Kenya</option>
                                                            <option value="Kiribati" title="Kiribati">Kiribati</option>
                                                            <option value="Korea, Democratic People's Republic of" title="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                                            <option value="Korea, Republic of" title="Korea, Republic of">Korea, Republic of</option>
                                                            <option value="Kuwait" title="Kuwait">Kuwait</option>
                                                            <option value="Kyrgyzstan" title="Kyrgyzstan">Kyrgyzstan</option>
                                                            <option value="Lao People's Democratic Republic" title="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                            <option value="Latvia" title="Latvia">Latvia</option>
                                                            <option value="Lebanon" title="Lebanon">Lebanon</option>
                                                            <option value="Lesotho" title="Lesotho">Lesotho</option>
                                                            <option value="Liberia" title="Liberia">Liberia</option>
                                                            <option value="Libya" title="Libya">Libya</option>
                                                            <option value="Liechtenstein" title="Liechtenstein">Liechtenstein</option>
                                                            <option value="Lithuania" title="Lithuania">Lithuania</option>
                                                            <option value="Luxembourg" title="Luxembourg">Luxembourg</option>
                                                            <option value="Macao" title="Macao">Macao</option>
                                                            <option value="Macedonia, the former Yugoslav Republic of" title="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option>
                                                            <option value="Madagascar" title="Madagascar">Madagascar</option>
                                                            <option value="Malawi" title="Malawi">Malawi</option>
                                                            <option value="Malaysia" title="Malaysia">Malaysia</option>
                                                            <option value="Maldives" title="Maldives">Maldives</option>
                                                            <option value="Mali" title="Mali">Mali</option>
                                                            <option value="Malta" title="Malta">Malta</option>
                                                            <option value="Marshall Islands" title="Marshall Islands">Marshall Islands</option>
                                                            <option value="Martinique" title="Martinique">Martinique</option>
                                                            <option value="Mauritania" title="Mauritania">Mauritania</option>
                                                            <option value="Mauritius" title="Mauritius">Mauritius</option>
                                                            <option value="Mayotte" title="Mayotte">Mayotte</option>
                                                            <option value="Mexico" title="Mexico">Mexico</option>
                                                            <option value="Micronesia, Federated States of" title="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                            <option value="Moldova, Republic of" title="Moldova, Republic of">Moldova, Republic of</option>
                                                            <option value="Monaco" title="Monaco">Monaco</option>
                                                            <option value="Mongolia" title="Mongolia">Mongolia</option>
                                                            <option value="Montenegro" title="Montenegro">Montenegro</option>
                                                            <option value="Montserrat" title="Montserrat">Montserrat</option>
                                                            <option value="Morocco" title="Morocco">Morocco</option>
                                                            <option value="Mozambique" title="Mozambique">Mozambique</option>
                                                            <option value="Myanmar" title="Myanmar">Myanmar</option>
                                                            <option value="Namibia" title="Namibia">Namibia</option>
                                                            <option value="Nauru" title="Nauru">Nauru</option>
                                                            <option value="Nepal" title="Nepal">Nepal</option>
                                                            <option value="Netherlands" title="Netherlands">Netherlands</option>
                                                            <option value="New Caledonia" title="New Caledonia">New Caledonia</option>
                                                            <option value="New Zealand" title="New Zealand">New Zealand</option>
                                                            <option value="Nicaragua" title="Nicaragua">Nicaragua</option>
                                                            <option value="Niger" title="Niger">Niger</option>
                                                            <option value="Nigeria" title="Nigeria">Nigeria</option>
                                                            <option value="Niue" title="Niue">Niue</option>
                                                            <option value="Norfolk Island" title="Norfolk Island">Norfolk Island</option>
                                                            <option value="Northern Mariana Islands" title="Northern Mariana Islands">Northern Mariana Islands</option>
                                                            <option value="Norway" title="Norway">Norway</option>
                                                            <option value="Oman" title="Oman">Oman</option>
                                                            <option value="Pakistan" title="Pakistan">Pakistan</option>
                                                            <option value="Palau" title="Palau">Palau</option>
                                                            <option value="Palestinian Territory, Occupied" title="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                            <option value="Panama" title="Panama">Panama</option>
                                                            <option value="Papua New Guinea" title="Papua New Guinea">Papua New Guinea</option>
                                                            <option value="Paraguay" title="Paraguay">Paraguay</option>
                                                            <option value="Peru" title="Peru">Peru</option>
                                                            <option value="Philippines" title="Philippines">Philippines</option>
                                                            <option value="Pitcairn" title="Pitcairn">Pitcairn</option>
                                                            <option value="Poland" title="Poland">Poland</option>
                                                            <option value="Portugal" title="Portugal">Portugal</option>
                                                            <option value="Puerto Rico" title="Puerto Rico">Puerto Rico</option>
                                                            <option value="Qatar" title="Qatar">Qatar</option>
                                                            <option value="Réunion" title="Réunion">Réunion</option>
                                                            <option value="Romania" title="Romania">Romania</option>
                                                            <option value="Russian Federation" title="Russian Federation">Russian Federation</option>
                                                            <option value="Rwanda" title="Rwanda">Rwanda</option>
                                                            <option value="Saint Barthélemy" title="Saint Barthélemy">Saint Barthélemy</option>
                                                            <option value="Saint Helena, Ascension and Tristan da Cunha" title="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
                                                            <option value="Saint Kitts and Nevis" title="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                            <option value="Saint Lucia" title="Saint Lucia">Saint Lucia</option>
                                                            <option value="Saint Martin (French part)" title="Saint Martin (French part)">Saint Martin (French part)</option>
                                                            <option value="Saint Pierre and Miquelon" title="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                            <option value="Saint Vincent and the Grenadines" title="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                                            <option value="Samoa" title="Samoa">Samoa</option>
                                                            <option value="San Marino" title="San Marino">San Marino</option>
                                                            <option value="Sao Tome and Principe" title="Sao Tome and Principe">Sao Tome and Principe</option>
                                                            <option value="Saudi Arabia" title="Saudi Arabia">Saudi Arabia</option>
                                                            <option value="Senegal" title="Senegal">Senegal</option>
                                                            <option value="Serbia" title="Serbia">Serbia</option>
                                                            <option value="Seychelles" title="Seychelles">Seychelles</option>
                                                            <option value="Sierra Leone" title="Sierra Leone">Sierra Leone</option>
                                                            <option value="Singapore" title="Singapore">Singapore</option>
                                                            <option value="Sint Maarten (Dutch part)" title="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
                                                            <option value="Slovakia" title="Slovakia">Slovakia</option>
                                                            <option value="Slovenia" title="Slovenia">Slovenia</option>
                                                            <option value="Solomon Islands" title="Solomon Islands">Solomon Islands</option>
                                                            <option value="Somalia" title="Somalia">Somalia</option>
                                                            <option value="South Africa" title="South Africa">South Africa</option>
                                                            <option value="South Georgia and the South Sandwich Islands" title="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                                                            <option value="South Sudan" title="South Sudan">South Sudan</option>
                                                            <option value="Spain" title="Spain">Spain</option>
                                                            <option value="Sri Lanka" title="Sri Lanka">Sri Lanka</option>
                                                            <option value="Sudan" title="Sudan">Sudan</option>
                                                            <option value="Suriname" title="Suriname">Suriname</option>
                                                            <option value="Svalbard and Jan Mayen" title="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                            <option value="Swaziland" title="Swaziland">Swaziland</option>
                                                            <option value="Sweden" title="Sweden">Sweden</option>
                                                            <option value="Switzerland" title="Switzerland">Switzerland</option>
                                                            <option value="Syrian Arab Republic" title="Syrian Arab Republic">Syrian Arab Republic</option>
                                                            <option value="Taiwan, Province of China" title="Taiwan, Province of China">Taiwan, Province of China</option>
                                                            <option value="Tajikistan" title="Tajikistan">Tajikistan</option>
                                                            <option value="Tanzania, United Republic of" title="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                            <option value="Thailand" title="Thailand">Thailand</option>
                                                            <option value="Timor-Leste" title="Timor-Leste">Timor-Leste</option>
                                                            <option value="Togo" title="Togo">Togo</option>
                                                            <option value="Tokelau" title="Tokelau">Tokelau</option>
                                                            <option value="Tonga" title="Tonga">Tonga</option>
                                                            <option value="Trinidad and Tobago" title="Trinidad and Tobago">Trinidad and Tobago</option>
                                                            <option value="Tunisia" title="Tunisia">Tunisia</option>
                                                            <option value="Turkey" title="Turkey">Turkey</option>
                                                            <option value="Turkmenistan" title="Turkmenistan">Turkmenistan</option>
                                                            <option value="Turks and Caicos Islands" title="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                            <option value="Tuvalu" title="Tuvalu">Tuvalu</option>
                                                            <option value="Uganda" title="Uganda">Uganda</option>
                                                            <option value="Ukraine" title="Ukraine">Ukraine</option>
                                                            <option value="United Arab Emirates" title="United Arab Emirates">United Arab Emirates</option>
                                                            <option value="United Kingdom" title="United Kingdom">United Kingdom</option>
                                                            <option value="United States" title="United States">United States</option>
                                                            <option value="United States Minor Outlying Islands" title="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                            <option value="Uruguay" title="Uruguay">Uruguay</option>
                                                            <option value="Uzbekistan" title="Uzbekistan">Uzbekistan</option>
                                                            <option value="Vanuatu" title="Vanuatu">Vanuatu</option>
                                                            <option value="Venezuela, Bolivarian Republic of" title="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option>
                                                            <option value="Viet Nam" title="Viet Nam">Viet Nam</option>
                                                            <option value="Virgin Islands, British" title="Virgin Islands, British">Virgin Islands, British</option>
                                                            <option value="Virgin Islands, U.S." title="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                            <option value="Wallis and Futuna" title="Wallis and Futuna">Wallis and Futuna</option>
                                                            <option value="Western Sahara" title="Western Sahara">Western Sahara</option>
                                                            <option value="Yemen" title="Yemen">Yemen</option>
                                                            <option value="Zambia" title="Zambia">Zambia</option>
                                                            <option value="Zimbabwe" title="Zimbabwe">Zimbabwe</option>
                                                        </select>
                                                        <div style="color: red">{{$errors->first("country") }}</div>
                                                    </div>
                                                </div>


                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>A short description of your professional skills and experience as a writer (Max. 300 characters)<span style="color: red">*</span></label>
                                                        <textarea maxlength="300" required name="description" class="form-control"
                                                                  rows="5">{{$profile->description}}</textarea>
                                                        <div style="color: red">{{$errors->first("description") }}</div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 ">
                                                <div class="col-sm-12">
                                                    <label class="control-label">Which citation styles are you familiar with? (select all that apply)<span style="color: red">*</span></label>

                                                    <div class="form-group" style="margin-top: 0px">
                                                        {!! Form::select('citation[]', \App\Citation::pluck('citation', 'id'), $profile->citation_ids(),
                                                        ['class' => 'select2 form-control', 'id'=>'citation','required', 'multiple']) !!}
                                                        <div style="color: red">{{$errors->first("citation") }}</div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <label class="control-label">Which academic disciplines are you familiar with? (select all that apply)<span style="color: red">*</span></label>

                                                    <div class="form-group" style="margin-top: 0px">
                                                        {!! Form::select('discipline[]', \App\Discipline::pluck('discipline', 'id'), $profile->discipline_ids(),
                                                        ['class' => 'select2 form-control', 'id'=>'discipline','required', 'multiple']) !!}
                                                        <div style="color: red">{{$errors->first("discipline") }}</div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Native language?<span style="color: red">*</span></label>
                                                        <input type="text" name="native_language" required value="{{$profile->native_language}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("native_language") }}</div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="education">
                                        <h4 class="info-text">Tell us about your highest education level</h4>
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3 ">
                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Name of University <span style="color: red">*</span></label>
                                                        <input type="text" required name="uni_name" value="{{$profile->uni_name}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("uni_name") }}</div>

                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Course Name <span style="color: red">*</span> </label>
                                                        <input type="text" required name="course_name"  value="{{$profile->course_name}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("course_name") }}</div>

                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <label class="control-label">Degree <span style="color: red">*</span></label>

                                                    <div class="form-group" style="margin-top: 0px">
                                                        <select name="degree" required class="'select2 form-control">
                                                            <option value="{{$profile->degree}}">{{$profile->degree}}</option>
                                                            <option value="Bachelors">Bachelors</option>
                                                            <option value="Masters">Masters</option>
                                                            <option value="Ph.D">Ph.D</option>
                                                            <option value="Associate">Associate</option>
                                                        </select>
                                                        <div style="color: red">{{$errors->first("degree") }}</div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Graduation Year<span style="color: red">*</span></label>
                                                        <input type="text" required name="grad_year"  value="{{$profile->grad_year}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("grad_year") }}</div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="experience">
                                        <div class="row">
                                            <h4 class="info-text"> What is your most relevant work experience?</h4>
                                            <div class="col-sm-6 col-sm-offset-3 ">
                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Name of Company <span style="color: red">*</span></label>
                                                        <input type="text" required name="company_name" value="{{$profile->company_name}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("company_name") }}</div>

                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Field of Work <span style="color: red">*</span> </label>
                                                        <input type="text" required name="field_of_work"  value="{{$profile->field_of_work}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("field_of_work") }}</div>

                                                    </div>
                                                </div>


                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Position <span style="color: red">*</span></label>
                                                        <input type="text" required name="position"  value="{{$profile->position}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("position") }}</div>

                                                    </div>
                                                </div>


                                                <div class="col-sm-12">
                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <label class="control-label">Duration<span style="color: red">*</span> </label>
                                                        <input type="text" required name="duration"  value="{{$profile->duration}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("duration") }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Update' />
                                    </div>
                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-fill btn-primary btn-wd' name='previous' value='Previous' />

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- wizard container -->



            </div>

        </div>
    </section>
@endsection


@section("scripts")
    <script src="{{ url('/inner/wizard/js/jquery.bootstrap.js')}}"></script>
    <script src="{{ url('/inner/wizard/js/material-bootstrap-wizard.js')}}"></script>
    <script src="{{ url('/inner/wizard/js/jquery.validate.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('/inner/wizard/css/material-bootstrap-wizard.css') }}">
    <script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>

@endsection



@section('css')
    <style>
        .bs-wizard {margin-top: 20px;}

        /*Form Wizard*/
        .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
        .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
        /*END Form Wizard*/

    </style>
@endsection

