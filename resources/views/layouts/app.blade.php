<?php
use App\UserGroup;
use App\User;
//$perm_role = UserGroup::find(Auth::user()->user_group);
?>
        <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Essay Energy') }}</title>

    <script>
        (function(t,a,l,k,j,s){
            s=a.createElement('script');s.async=1;s.src="https://cdn.talkjs.com/talk.js";a.head.appendChild(s)
            ;k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l
                .push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);
    </script>

    <!-- Styles -->
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{url('/inner/plugins/bootstrap/css/bootstrap.css')}}"  rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{url('/inner/plugins/node-waves/waves.css')}}"  rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{url('/inner/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{url('/inner/css/style.css')}}" rel="stylesheet">

    <link href="{{url('/inner/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />

    <link href="{{url('/inner/form-select2/select2.css')}}" type="text/css" rel="stylesheet"/>





    <!-- Bootstrap Select Css -->
    <link href="{{url('/inner/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{url('/inner/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />--}}


    <link href="{{url('/inner/plugins/nouislider/nouislider.min.css')}}" type="text/css" rel="stylesheet"/>

@yield('css')



<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{url('/inner/css/themes/theme-cyan.css')}}" rel="stylesheet" />

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</head>
<body class="theme-cyan">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>

<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->

<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{url('/')}}">Essay Energy</a>
            {{--<img src="{{url('images/logo.png')}}">--}}

        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call time -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <span style="padding: 7px 7px 2px 7px;" id="time"> </span>
                    </a>
                </li>
                <!-- #END# Call time -->


                <!-- Instant Notifications -->

                <li class="dropdown" id="markasread" onclick="markNotificationAsread('{{count(auth()->user()->unreadNotifications)}}')">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">notifications</i>
                        <span class="label-count" id="notif-count">{{count(auth()->user()->unreadNotifications)}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu" id="notifications-list">

                                @forelse(auth()->user()->unreadNotifications as $notification)

                                    <li>
                                        @include('layouts.notification.'.snake_case(class_basename($notification->type)))
                                        @empty

                                            <div style="text-align: center;">
                                                <div class="menu-info">
                                                    <p id="notif-message">
                                                        No Unread Notifications
                                                    </p>
                                                </div>
                                            </div>

                                    </li>
                                @endforelse

                            </ul>
                        </li>
                        {{--<li class="footer">--}}
                            {{--<a href="javascript:void(0);">View All Notifications</a>--}}
                        {{--</li>--}}
                    </ul>
                </li>
                <!-- #END# Instant Notifications -->

                <!-- Notifications -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">reorder</i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">ACCOUNT OVERVIEW</li>
                        <li class="body">
                            <ul class="menu">
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-blue-grey">
                                            <i class="material-icons">attach_money</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Available Balance</h4>
                                            <b>
                                                {{--$ {{number_format(\App\PayPalIPNMessage::where('custom', Auth::user()->email)->sum('mc_gross'),2)}}--}}
                                                $ {{auth()->user()->customer->balance}}
                                            </b>
                                        </div>
                                    </a>
                                </li>

                                @if(Auth::user()->user_group == 1)

                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-cyan">
                                            <i class="material-icons">money_off</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Reserved for Orders</h4>
                                            <b>
                                                $ {{auth()->user()->reserved()}}
                                            </b>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-red">
                                            <i class="material-icons">update</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Orders In Progress</h4>
                                            <b>
                                               {{\App\Order::where('customer_id', auth()->user()->id)->where('status_id', 6)->count()}} Order(s)
                                            </b>
                                        </div>
                                    </a>
                                </li>

                                @endif
                                @if(Auth::user()->user_group == 2)


                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">update</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Orders In Progress</h4>
                                                <b>
                                                    {{ \App\OrderAssignment::where('support_id',auth()->user()->id)->where('status_id',6)->count() }} Order(s)
                                                </b>
                                            </div>
                                        </a>
                                    </li>

                                @endif
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-orange">
                                            <i class="material-icons">rate_review</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Average Rating</h4>
                                            @switch(round(auth()->user()->avg_rating(),0))
                                            @case(1)
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            @break
                                            @case(2)
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            @break
                                            @case(3)
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            @break
                                            @case(4)
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star"></span>
                                            @break
                                            @case(5)
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            <span class="fa fa-star" style="color: orange;"></span>
                                            @break
                                            @endswitch
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="{{ Laricon::getImageDataUri(Auth::user()->email) }}" width="50" height="50"  alt="{{Auth::user()->name}}" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">#{{Auth::user()->name}}</div>
                <div class="email">{{Auth::user()->email}}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{url('logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header active">MAIN NAVIGATION</li>

                @if(Auth::user()->user_group != 1)
                    <li class="{{\Request::is('dashboard') ? 'active' : ''}}">
                        <a href="{{url('/dashboard')}}">
                            <i class="material-icons">dashboard</i>
                            <span>Available Orders</span>
                        </a>
                    </li>
                @endif

                @if(Auth::user()->user_group == 1)

                    <li class="{{\Request::is('orders/*') || \Request::is('orders/new') || \Request::is('orders/in_progress')|| \Request::is('orders/my_orders')
                                                || \Request::is('orders/completed')|| \Request::is('orders/cancelled') ? 'active' : ''}}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">receipt</i>
                            <span>Orders</span>
                        </a>
                            <ul class="ml-menu">
                                <li class="{{\Request::is('orders/new') ? 'active' : ''}}">
                                    <a href="{{url('orders/new')}}">Create Order</a>
                                </li>

                                <li class="{{\Request::is('orders/my_orders') ? 'active' : ''}}">
                                    <a href="{{url('orders/my_orders')}}">My Orders</a>
                                </li>


                                <li class="{{\Request::is('orders/in_progress') ? 'active' : ''}}">
                                    <a href="{{url('orders/in_progress')}}">In Progress</a>
                                </li>

                                <li class="{{\Request::is('orders/completed') ? 'active' : ''}}">
                                    <a href="{{url('orders/completed')}}">Completed</a>
                                </li>

                                <li class="{{\Request::is('orders/cancelled') ? 'active' : ''}}">
                                    <a href="{{url('orders/cancelled')}}">Cancelled</a>
                                </li>
                        </ul>
                    </li>

                @else
                    <li class="{{\Request::is('order/*') || \Request::is('order/support/in_progress') || \Request::is('order/support/completed')
                                                ? 'active' : ''}}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">receipt</i>
                            <span>My Orders</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{\Request::is('order/support/my_orders') ? 'active' : ''}}">
                                <a href="{{url('order/support/my_orders')}}">My Orders</a>
                            </li>

                            <li class="{{\Request::is('order/support/in_progress') ? 'active' : ''}}">
                                <a href="{{url('order/support/in_progress')}}">Orders in progress</a>
                            </li>

                            <li class="{{\Request::is('order/support/completed') ? 'active' : ''}}">
                                <a href="{{url('order/support/completed')}}">Completed Orders</a>
                            </li>

                        </ul>
                    </li>
                @endif

                <li class="{{\Request::is('payments') ? 'active' : ''}}">
                    <a href="{{url('payments')}}">
                        <i class="material-icons">payment</i>
                        <span>Payments</span>
                    </a>
                </li>


                <li class="{{\Request::is('reviews') ? 'active' : ''}}">
                        <a href="{{url('reviews')}}">
                            <i class="material-icons">rate_review</i>
                            <span>Reviews</span>
                        </a>
                    </li>

                    {{--<li class="{{\Request::is('reports') ? 'active' : ''}}">--}}
                        {{--<a onclick="comingSoon()">--}}
                            {{--<i class="material-icons">live_help</i>--}}
                            {{--<span>Support</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}


                @if(Auth::user()->user_group == 2)
                    <li class="{{\Request::is('writer/profile') ? 'active' : ''}}">
                        <a href="{{url('writer/profile')}}">
                            <i class="material-icons">person</i>
                            <span>My Profile</span>
                        </a>
                    </li>
                @endif





            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; {{\Carbon\Carbon::now()->year}} <a href="javascript:void(0);">Essay Energy</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.1
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->

</section>


@yield('content')

<link rel="stylesheet" href="{{url('/build/css/bootstrap-datetimepicker.min.css')}}">


<!-- Jquery Core Js -->
<script src="{{url('/inner/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{url('/inner/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Select Plugin Js -->
{{--<script src="{{url('/inner/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>--}}

<!-- Slimscroll Plugin Js -->
<script src="{{url('/inner/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{url('/inner/plugins/node-waves/waves.js')}}"></script>

{{--<script type="text/javascript" src="{{url('/inner/js/jquery.uploadPreview.min.js')}}"></script>--}}

<script src="{{url('/inner/plugins/autosize/autosize.js')}}"></script>


<!-- Moment Plugin Js -->
{{--<script src="{{url('/inner/plugins/momentjs/moment.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{url('/build/js/bootstrap-datetimepicker.min.js')}}"></script>



{{--<script src="{{url('/inner/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" ></script>--}}

<!-- Multi Select Plugin Js -->
<script src="{{url('/inner/plugins/multi-select/js/jquery.multi-select.js')}}"></script>



<script src="{{url('/inner/plugins/bootbox/bootbox.js')}}"></script>


<!-- Bootstrap Material Datetime Picker Plugin Js -->
{{--<script src="{{url('/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>--}}

<!-- Custom Js -->
<script src="{{url('/inner/js/admin.js')}}"></script>
<script src="{{url('/inner/js/pages/index.js')}}"></script>
{{--<script src="{{url('/inner/js/pages/forms/basic-form-elements.js')}}"></script>--}}
<!-- Custom Js -->

{{--<script src="{{url('js/pages/forms/advanced-form-elements.js')}}"></script>--}}
<script src="//js.pusher.com/3.1/pusher.min.js"></script>


<!-- Demo Js -->
<script src="{{url('/inner/js/demo.js')}}"></script>


<!-- Jquery CountTo Plugin Js -->
<script src="{{url('/inner/plugins/jquery-countto/jquery.countTo.js')}}"></script>

<script src="{{url('/inner/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{ url('/inner/form-select2/select2.min.js') }}"></script>
<script src="{{ asset('/js/main.js') }}"></script>






<script>

    $(document).ready(function () {
        $("select").removeClass('form-control');
        $("select").select2({width: '100%'});
    });




    (function () {
        function checkTime(i) {
            return (i < 10) ? "0" + i : i;
        }

        function startTime() {
            var today = new Date(),
                h = checkTime(today.getUTCHours()),
                m = checkTime(today.getUTCMinutes()),
                s = checkTime(today.getUTCSeconds());
            document.getElementById('time').innerHTML = h + ":" + m + ":" + s + " UTC";
            t = setTimeout(function () {
                startTime()
            }, 500);
        }
        startTime();
    })();


    var notificationsWrapper   = $('.dropdown-notifications');
    var notificationsCountElem = notificationsWrapper.find('i[notif-count]');
    var notificationsCount     = parseInt(notificationsCountElem.data('count'));
//    var notifications          = $('.notifications-list');
    var notifications          = notificationsWrapper.find('ul.dropdown-menu');


    if (notificationsCount <= 0) {
        notificationsWrapper.hide();
    }

    // Enable pusher logging - don't include this in production
     Pusher.logToConsole = true;


    var pusher = new Pusher('6d9c576a9a8660286c9e', {
        cluster: 'mt1',
        encrypted: true
    });

//    var pusher = new Pusher('6d9c576a9a8660286c9e', {
//        cluster: 'mt1',
//        forceTLS: true
//    });


    var channel = pusher.subscribe('bid-placed');



    // Bind a function to a Event (the full Laravel class)
    channel.bind('App\\Events\\BidPlaced', function(data) {
//    channel.bind('pusher:bid-placed', function(data) {

        //console.log("shit");
        alert(JSON.stringify(data));
        var existingNotifications = notifications.html();
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
        var newNotificationHtml =
            "<li> " +
            "<a href='javascript:void(0);'> " +
            "<div class='icon-circle bg-blue-grey'> " +
            "<i class='material-icons'>comment</i> " +
            "</div> " +
            "<div class='menu-info'> " +
            "<h4 id='notif-title'> New bid</h4> " +
            "<p id='notif-message'> " +
            "data.message " +
            "</p> " +
            "</div>" +
            "</a> " +
            "</li>";


        notifications.html(newNotificationHtml + existingNotifications);

        notificationsCount += 1;
        notificationsCountElem.html(notificationsCount);
//        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
        notificationsWrapper.show();
    });





    //
//    function comingSoon() {
//        swal({
//            title: "Coming Soon!",
//            text: "This module is still under contruction... ",
//            type: "warning",
//            showCancelButton: false,
//            confirmButtonColor: "#DD6B55",
//            confirmButtonText: "Okay",
//            closeOnConfirm: true,
//        });
//    }

</script>

@yield('scripts')

</body>
</html>
