
<!-- container element in which TalkJS will display a chat UI -->
<div id="talkjs-container" style="width: 90%;  height: 500px"><i>Loading chat...</i></div>

<!-- TalkJS initialization code, which we'll customize in the next steps -->
<script>
    Talk.ready.then(function() {
        var me = new Talk.User({
            id: "{{Auth::user()->id}}",
            name: "{{Auth::user()->name}}",
            email: "{{Auth::user()->email}}",
            configuration: "users"
            //photoUrl: "https://demo.talkjs.com/img/alice.jpg",
            //welcomeMessage: "Hey there! How are you? :-)"
        });
        window.talkSession = new Talk.Session({
            appId: "ARxdZvbY",
            // appId: "t426MmOI",
            me: me
        });
        var other = new Talk.User({
            id: "{{$bid->writer->id}}",
            name: "{{$bid->writer->name}}",
            email: "{{$bid->writer->email}}"
           // photoUrl: "https://demo.talkjs.com/img/9.jpg",
            //welcomeMessage: "Hey, how can I help?"
        });

        //var conversation = talkSession.getOrCreateConversation("order_"+'{{$order->id}}');
        var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other));
        conversation.setAttributes({
            subject: "{{$order->topic}}"
        });
        conversation.setParticipant(me);
        conversation.setParticipant(other);
//        var inbox = talkSession.createInbox({selected: conversation});
//        inbox.mount(document.getElementById("talkjs-container"));

        var chatbox = talkSession.createChatbox(conversation);
        chatbox.mount(document.getElementById("talkjs-container"));
    });
</script>



    {{--<div class="panel panel-primary">--}}
        {{--<div class="panel-heading" id="accordion">--}}
            {{--<span class="glyphicon glyphicon-comment"></span> Chat--}}
        {{--</div>--}}
        {{--<div class="panel-collapse " id="collapseOne">--}}
            {{--<div class="panel-body">--}}
                {{--<ul class="chat">--}}
                    {{--<li class="left clearfix"><span class="chat-img pull-left">--}}
                            {{--<img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />--}}
                        {{--</span>--}}
                        {{--<div class="chat-body clearfix">--}}
                            {{--<div class="header">--}}
                                {{--<strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">--}}
                                    {{--<span class="glyphicon glyphicon-time"></span>12 mins ago</small>--}}
                            {{--</div>--}}
                            {{--<p>--}}
                                {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare--}}
                                {{--dolor, quis ullamcorper ligula sodales.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="right clearfix"><span class="chat-img pull-right">--}}
                            {{--<img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />--}}
                        {{--</span>--}}
                        {{--<div class="chat-body clearfix">--}}
                            {{--<div class="header">--}}
                                {{--<small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>--}}
                                {{--<strong class="pull-right primary-font">Bhaumik Patel</strong>--}}
                            {{--</div>--}}
                            {{--<p>--}}
                                {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare--}}
                                {{--dolor, quis ullamcorper ligula sodales.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="left clearfix"><span class="chat-img pull-left">--}}
                            {{--<img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />--}}
                        {{--</span>--}}
                        {{--<div class="chat-body clearfix">--}}
                            {{--<div class="header">--}}
                                {{--<strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">--}}
                                    {{--<span class="glyphicon glyphicon-time"></span>14 mins ago</small>--}}
                            {{--</div>--}}
                            {{--<p>--}}
                                {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare--}}
                                {{--dolor, quis ullamcorper ligula sodales.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="right clearfix"><span class="chat-img pull-right">--}}
                            {{--<img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />--}}
                        {{--</span>--}}
                        {{--<div class="chat-body clearfix">--}}
                            {{--<div class="header">--}}
                                {{--<small class=" text-muted"><span class="glyphicon glyphicon-time"></span>15 mins ago</small>--}}
                                {{--<strong class="pull-right primary-font">Bhaumik Patel</strong>--}}
                            {{--</div>--}}
                            {{--<p>--}}
                                {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare--}}
                                {{--dolor, quis ullamcorper ligula sodales.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--</ul>--}}

                <!-- minified snippet to load TalkJS without delaying your page -->

            {{--</div>--}}
            {{--<div class="panel-footer">--}}
                {{--<div class="input-group">--}}
                    {{--<input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />--}}
                    {{--<span class="input-group-btn">--}}
                            {{--<button class="btn btn-warning btn-sm" id="btn-chat">--}}
                                {{--Send</button>--}}
                        {{--</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


