<a href="{{url('orders/details/'.$notification->data['order']['id'])}}">

    <div class="menu-info row">
        <div class="icon-circle bg-green col-md-3 center-block">
            <i class="material-icons">spellcheck</i>
        </div>
        <div class="col-md-9" style="padding-right: 0px">
            <h4 id="notif-title"> Order Complete</h4>
            <p id="notif-message">
                Your Order #{{$notification->data['order']['id']}} ({{$notification->data['order']['topic']}}) is complete
            </p>
        </div>
    </div>
</a>