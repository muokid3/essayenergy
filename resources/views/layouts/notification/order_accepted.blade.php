<a href="{{url('orders/details/'.$notification->data['order']['id'])}}">
    <div class="icon-circle bg-blue-grey">
        <i class="material-icons">comment</i>
    </div>
    <div class="menu-info">
        <h4 id="notif-title"> Order Accepted</h4>
        <p id="notif-message">
            Your Order #{{$notification->data['order']['id']}} is being processed by  #{{$notification->data['support']['name']}}
        </p>
    </div>
</a>