<a href="{{url('orders/bid/details/'.$notification->data['bid']['id'])}}">
    <div class="icon-circle bg-blue-grey">
        <i class="material-icons">comment</i>
    </div>
    <div class="menu-info">
        <h4 id="notif-title"> New Bid</h4>
        <p id="notif-message">
            {{$notification->data['bidder']['name']}} placed a bid on order #{{$notification->data['bid']['order_id']}}
        </p>
    </div>
</a>