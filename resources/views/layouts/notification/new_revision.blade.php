<a href="{{url('order/'.$notification->data['order']['id'])}}">

    <div class="menu-info row">
        <div class="icon-circle bg-green col-md-3 center-block">
            <i class="material-icons">mode_edit</i>
        </div>
        <div class="col-md-9" style="padding-right: 0px">
            <h4 id="notif-title"> New Revision</h4>
            <p id="notif-message">
                Your have a new revision for order #{{$notification->data['order']['id']}} ({{$notification->data['order']['topic']}})
            </p>
        </div>

    </div>
</a>