<!-- Header -->

<header class="header">

    <!-- Top Bar -->
    <div class="top_bar">
        <div class="top_bar_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
                            <ul class="top_bar_contact_list">
                                <li><div class="question">Have any questions?</div></li>
                                <li>
                                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                    <div><a href="https://wa.me/12132951892" target="_blank">+1 (213) 295-1892</a></div>
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <div><a href="mailto:support@essayenergy.com?Subject=Hello" target="_top">support@essayenergy.com</a></div>
                                </li>
                            </ul>
                            <div class="top_bar_login ml-auto">
                                <div class="login_button"><a href="{{url('dashboard')}}">My Account</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Header Content -->
    <div class="header_container">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="header_content d-flex flex-row align-items-center justify-content-start">
                        <div class="logo_container">
                            <a href="{{url('/')}}">
                                <div class="logo_text">Essay<span>Energy</span></div>
                            </a>
                        </div>
                        <nav class="main_nav_contaner ml-auto">
                            <ul class="main_nav">
                                <li style="margin-right: 20px;" class="{{\Request::is('/') ? 'active' : ''}}"><a href="{{url('/')}}">Home</a></li>
                                <li style="margin-right: 20px;" class="{{\Request::is('how_it_works') ? 'active' : ''}}"><a href="{{url('/how_it_works')}}">How it Works</a></li>
                                <li style="margin-right: 20px;" class="{{\Request::is('about') ? 'active' : ''}}"><a href="{{url('/about')}}">About Us</a></li>
                                {{--<li style="margin-right: 20px;" class="{{\Request::is('latest_orders') ? 'active' : ''}}"><a href="{{url('/latest_orders')}}">Latest Orders</a></li>--}}
                                <li style="margin-right: 20px;" class="{{\Request::is('latest_reviews') ? 'active' : ''}}"><a href="{{url('/latest_reviews')}}">Latest Reviews</a></li>
                                <li style="margin-right: 20px;" class="{{\Request::is('sample_papers') ? 'active' : ''}}"><a href="{{url('/sample_papers')}}">Sample Research Papers</a></li>
                                <li style="margin-right: 20px;" class="{{\Request::is('blog') ? 'active' : ''}}"><a href="{{url('/blog')}}">Blog</a></li>
                            </ul>

                            <!-- Hamburger -->
                            <div class="hamburger menu_mm">
                                <i class="fa fa-bars menu_mm" aria-hidden="true"></i>
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>

</header>

<!-- Menu -->

<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
    <div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
    <nav class="menu_nav">
        <ul class="menu_mm">
            <li class="menu_mm"><a href="{{url('/')}}">Home</a></li>
            <li class="menu_mm"><a href="{{url('/how_it_works')}}">How it Works</a></li>
            {{--<li class="menu_mm"><a href="{{url('/latest_orders')}}">Latest Orders</a></li>--}}
            <li class="menu_mm"><a href="{{url('/latest_reviews')}}">Latest Reviews</a></li>
            <li class="menu_mm"><a href="{{url('/sample_papers')}}">Sample Research Papers</a></li>
            <li class="menu_mm"><a href="{{url('/blog')}}">Blog</a></li>

        </ul>
    </nav>
</div>