<!-- Footer -->

<footer class="footer">
    <div class="footer_background" style="background-image:url(images/footer_background.png)"></div>
    <div class="container">
        <div class="row footer_row">
            <div class="col">
                <div class="footer_content">
                    <div class="row">

                        <div class="col-lg-3 footer_col">

                            <!-- Footer About -->
                            <div class="footer_section footer_about">
                                <div class="footer_logo_container">
                                    <a href="#">
                                        <div class="footer_logo_text">Essay<span>Energy</span></div>
                                    </a>
                                </div>
                                <div class="footer_about_text">
                                    <p>High quality and timely papers.</p>
                                </div>
                                {{--<div class="footer_social">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                                        {{--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                                        {{--<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>--}}
                                        {{--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col">

                            <!-- Footer Contact -->
                            <div class="footer_section footer_contact">
                                <div class="footer_title">Contact Us</div>
                                <div class="footer_contact_info">
                                    <ul>
                                        <li><a href="mailto:support@essayenergy.com?Subject=Hello" target="_top">support@essayenergy.com</a></li>
                                        <li>Phone:  <a href="tel:+1 (213) 295-1892">+1 (213) 295-1892</a> </li>
                                        {{--<li>40 Baria Sreet 133/2 New York City, United States</li>--}}
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col">

                            <!-- Footer links -->
                            <div class="footer_section footer_links">
                                <div class="footer_title">Quick Links</div>
                                <div class="footer_links_container">
                                    <ul>
                                        <li><a href="{{url('/')}}">Home</a></li>
                                        <li><a href="{{url('how_it_works')}}">How it Works</a></li>
                                        <li><a href="{{url('latest_reviews')}}">Reviews</a></li>
                                        <li><a href="{{url('blog')}}">Blog</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col clearfix">

                            <!-- Footer links -->
                            {{--<div class="footer_section footer_mobile">--}}
                                {{--<div class="footer_title">Mobile</div>--}}
                                {{--<div class="footer_mobile_content">--}}
                                    {{--<div class="footer_image"><a href="#"><img src="images/mobile_1.png" alt=""></a></div>--}}
                                    {{--<div class="footer_image"><a href="#"><img src="images/mobile_2.png" alt=""></a></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row copyright_row">
            <div class="col">
                <div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
                    <div class="cr_text">
                        Copyright &copy;<script data-cfasync="false" src="https://colorlib.com/cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | EssayEnergy
                    </div>
                    <div class="ml-lg-auto cr_links">
                        <ul class="cr_list">
                            <li><a href="{{url('terms')}}">Terms of Use</a></li>
                            <li><a href="{{url('privacy')}}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>