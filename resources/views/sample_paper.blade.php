<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <title>Sample Research Paper</title>
    <link rel="stylesheet" type="text/css" href="{{url('plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('plugins/OwlCarousel2-2.2.1/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('styles/courses.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('styles/courses_responsive.css')}}">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description" content="NB: This research paper is to be used for research and learning purposes only">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{url('styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{url('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('plugins/colorbox/colorbox.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{url('styles/main_styles.css')}}">

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


</head>
<body>

<div class="super_container">


@include('layouts.header')


<!-- Home -->

    <div class="home" style="height: 200px">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><a href="{{url('/sample_papers')}}">Sample Research Papers</a></li>
                                <li>{{$sample_paper->question}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="courses">
        <div class="container">
            <div class="row">

                <!-- Courses Main Content -->
                <div class="col-lg-8">
                    <h4><i>NB: This research paper is to be used for research and learning purposes only</i></h4>
                    <div class="courses_container">
                        <!-- Event -->
                        <div class="row courses_row">
                                <!-- Event -->
                                <div class="col-lg-12 event_col">
                                    <div class="event event_mid">
                                        <div class="event_body d-flex flex-row align-items-start justify-content-start">
                                            {{--<div class="event_date">--}}
                                                {{--<div class="d-flex flex-column align-items-center justify-content-center trans_200">--}}
                                                    {{--<div class="event_day trans_200">${{(float)$sample_paper->price}}</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="event_content">
                                                <div class="event_title"><h4>{{$sample_paper->question}}</h4></div>
                                                <div class="event_info_container">
                                                    <div class="event_info"><i class="fa fa-bookmark-o" aria-hidden="true"></i><span>{{$sample_paper->discipline->discipline}}</span></div>
                                                    <div class="event_text">
                                                        <p>{!! $sample_paper->essay !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        @if(is_null(\App\SampleEssayPayment::where('user_id',auth()->user()->id)->where('essay_id',$sample_paper->id)->first()))
                        <div class="row">
                            <div class="col-md-6">
                                <i class="text-info">To view and download this sample paper, please purchase it</i>
                                <h4 class="text-danger">Price: $ {{$sample_paper->price}}</h4>
                            </div>
                            <div class="col-md-3">

                                <div id="paypal-button-container"></div>

                                <script
                                        {{--src="https://www.paypal.com/sdk/js?client-id=AeqZYY8uRCC9cp_l6xvxI7em5eK7UhXUoDpHjyqnPrFuKvzGj8S2LMFgWu0E0mS__UDJt5z-IKBZbqdd">--}}
                                         src="https://www.paypal.com/sdk/js?client-id=AWE1ttaQ_STrnDaMNBfpyQuufSWdP5yOUadRkwJt4QdwoTxva0NA7Uc4b04tYG8Uhc_So-DvmiumkBxt">
                                </script>

                                <script>
                                    paypal.Buttons({
                                        style: {
                                            size: 'small',
                                            color: 'gold',
                                            shape: 'pill',
                                            label: 'pay',
                                            fundingicons: true,


                                        },
                                        createOrder: function(data, actions) {
                                            // Set up the transaction
                                            return actions.order.create({
                                                purchase_units: [{
                                                    amount: {
                                                        value: '{{$sample_paper->price}}',
                                                        currency: 'USD'
                                                    },
                                                    custom_id: 'paper#{{$sample_paper->id}}#userid#{{auth()->user()->id}}',
                                                    description: 'Purchase for sample paper ID '+'{{$sample_paper->id}}',
                                                }]
                                            });
                                        },
                                        onApprove: function(data, actions) {
                                            // Capture the funds from the transaction
                                            return actions.order.capture().then(function(details) {
                                                // Show a success message to your buyer
                                                //alert('Transaction completed by ' + details.payer.name.given_name);

                                                showVerifying();
                                                //console.log("please hold while we verify ur payment");

                                                $.ajax({
                                                    url: '/paypal/sample_papers/get_order/' + data.orderID,
                                                    type: 'get',
                                                    headers: {
                                                        'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                                                    },
                                                    success: function (html) {

                                                        swal.close();
                                                        location.reload();
                                                    },
                                                    error: function(xhr, type, exception) {
                                                        // if ajax fails display error alert
                                                        alert("A fatal error occurred ("+exception+"). Please contact support team");
                                                        swal.close();
                                                    }
                                                });

                                            });
                                        }
                                    }).render('#paypal-button-container');
                                </script>

                            </div>
                        </div>
                        @else
                            <span class="alert-success"><i>You have purchased this sample research paper. You can download it for reasearch purposes only</i></span>
                            <br>
                            <a href="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($sample_paper->file) }}" class="btn btn-success align-center" target="_blank" style="margin-top: 20px">Download Now</a>

                        @endif

                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="sidebar">

                        <!-- Categories -->
                        <div class="sidebar_section">
                            {{--<div class="sidebar_section_title">Place an order now</div>--}}
                            <div class="sidebar_categories">
                                @guest

                                    <form class="counter_form_content d-flex flex-column align-items-center justify-content-center" action="{{url('/register')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user_group"  value="1">
                                        <div class="counter_form_title">place order now</div>

                                        {{ $errors->first('email') }}

                                        <input type="email" name="email" class="counter_input" placeholder="E-Mail" required="required">
                                        @if ($errors->has('email'))
                                        @section('scripts')
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                    $('#newLoginModal').modal('show');
                                                });
                                            </script>
                                        @endsection
                                        @endif
                                        {{--{!! Form::select('first_order_type', \App\Type::pluck('name', 'id'), null,--}}
                                        {{--['class' => 'dropdown_item_select counter_input', 'id'=>'specialDude','required']) !!}--}}

                                        <select id="specialDude" required  class="dropdown_item_select counter_input" name="first_order_type">
                                            <option value="">Type of paper</option>
                                            @foreach(\App\Type::all() as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach

                                        </select>


                                        <input type="number" name="first_order_pages" min="1" class="counter_input" placeholder="Number of pages" required="required">

                                        <button type="submit" class="btn btn-success btn-block">Get Started Now</button>
                                    </form>

                                @else
                                    <div style="text-align: center;" class="counter_form_content d-flex flex-column align-items-center justify-content-center">
                                        <div class="counter_form_title">place order now</div>

                                        <a href="{{url('orders/new')}}" class="btn btn-success align-center" style="margin-top: 20px">Place your order now</a>

                                    </div>

                                @endauth
                            </div>
                        </div>

                        <!-- Latest posts -->
                        <div class="sidebar_section">
                            <div class="sidebar_section_title">Latest Posts</div>
                            <div class="sidebar_latest">

                            @foreach($latest as $blogItem)
                                <!-- Latest Post -->
                                    <div class="latest d-flex flex-row align-items-start justify-content-start">
                                        <div class="latest_image">
                                            <div>
                                                <img src="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($blogItem->image_link)}}" alt="">
                                            </div>
                                        </div>
                                        <div class="latest_content">
                                            <div class="latest_title"><a href="{{url('blog/'.$blogItem->slug)}}">{{$blogItem->title}}</a></div>
                                            <div class="latest_date">{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($blogItem->created_at)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}</div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <!-- Banner -->
                        <div class="sidebar_section">
                            <div class="sidebar_banner d-flex flex-column align-items-center justify-content-center text-center">
                                <div class="sidebar_banner_background" style="background-image:url(images/banner_1.jpg)"></div>
                                <div class="sidebar_banner_overlay"></div>
                                <div class="sidebar_banner_content">
                                    <div class="banner_title">High quality papers</div>
                                    <div class="banner_button"><a href="{{url('how_it_works')}}">learn more</a></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>


    @include('layouts.footer')

<!-- Modal -->
    <div class="modal fade" id="newLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sign in to continue to Essay Energy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 mx-auto">
                                <div class="account-wall">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">

                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Password" autofocus required>
                                        @if ($errors->has('password'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                        @if ($errors->has('email'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('email') }}</strong></span>
                                        @endif

                                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                                            Sign in</button>

                                        <a href="{{url('/password/reset')}}"  class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



</div>

    <style type="text/css">
        .checked {
            color: orange;
        }

    </style>

<script src="{{url('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{url('styles/bootstrap4/popper.js')}}"></script>
<script src="{{url('styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{url('plugins/easing/easing.js')}}"></script>
<script src="{{url('plugins/parallax-js-master/parallax.min.js')}}"></script>
<script src="{{url('plugins/colorbox/jquery.colorbox-min.js')}}"></script>
<script src="{{url('js/blog_single.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



@yield('scripts')


<script type="text/javascript">

    function showVerifying() {
        swal({
            title: 'Verifying ... ',
            text: "Please hold as we verify your payment",
            content: {
                element: "span",
                attributes: {
                    class: "fa fa-spin",
                },
            },

            buttons: false,
            icon: '{{url('/img/shield.png')}}',
            closeOnEsc: false,
            closeOnClickOutside: false

        });
    }
</script>




</body>

</html>