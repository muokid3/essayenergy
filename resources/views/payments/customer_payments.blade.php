@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Current Balance: $ {{auth()->user()->customer->balance}}

                                <br>
                                <br>
                                @if(Auth::user()->user_group == 1)
                                    <span><i><b>Reserved for Orders:  $ {{\App\Escrow::where('customer_id',Auth::user()->customer_id)->sum('total_charge')}}</b></i></span>
                                @endif

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                            @endif


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#payments" data-toggle="tab">Payment History</a></li>
                                @if(Auth::user()->user_group == 1)
                                    <li role="presentation"><a href="#addfunds" data-toggle="tab">Add Funds</a></li>
                                @endif

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="payments">
                                    <div class="card">
                                        <div class="body">

                                            <table class="table table-striped table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Amount ($)</th>
                                                    <th>Previous Balance</th>
                                                    <th>Type</th>
                                                    <th>Source</th>
                                                    <th>TXR ID</th>
                                                    <th>Narration</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($transactions as $transaction)

                                                    <tr>
                                                        <td>
                                                            {{$transaction->amount}}
                                                        </td>
                                                        <td>
                                                            {{$transaction->previous_balance}}
                                                        </td>
                                                        <td>
                                                            @switch($transaction->transaction_type)
                                                                @case("DR")
                                                                <span class="label bg-red">DEBIT</span>
                                                                @break

                                                                @case("CR")
                                                                <span class="label bg-green">CREDIT</span>
                                                                @break

                                                                @default
                                                                <span class="label bg-orange">CREDIT</span>
                                                            @endswitch


                                                        </td>
                                                        <td>
                                                            {{$transaction->source}}
                                                        </td>
                                                        <td>
                                                            {{$transaction->external_trx_id}}
                                                        </td>
                                                        <td>
                                                            {{$transaction->narration}}
                                                        </td>
                                                        <td>
                                                            {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($transaction->created_at)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            {{$transactions->links()}}



                                        </div>
                                    </div>
                                </div>


                                <div role="tabpanel" class="tab-pane fade in" id="addfunds">
                                    <div class="card">
                                        <div class="body">

                                            <div class="row">

                                                <form name="payform" class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="form-label">Payment Methods</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <img src="{{url('/inner/images/paypal.png')}}" width="80" height="20" alt="Paypal" />
                                                            <img style="margin-left: 10px" src="{{url('/inner/images/bitcoin.png')}}" width="80" height="20" alt="Bitcoin" />
                                                        </div>
                                                    </div>

                                                    <div class="row clearfix center">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group form-float">
                                                                <div class="form-line">
                                                                    <input type="number" id="topAmount" min="1" name="amount" class="form-control">
                                                                    <label class="form-label">Amount(USD)</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div id="paypal-button"></div>
                                                            <script src="https://www.paypalobjects.com/api/checkout.js"></script>
                                                            <script>

                                                                paypal.Button.render({
                                                                    // Configure environment
                                                                    env: 'production',
                                                                    client: {
                                                                        //sandbox: 'AeqZYY8uRCC9cp_l6xvxI7em5eK7UhXUoDpHjyqnPrFuKvzGj8S2LMFgWu0E0mS__UDJt5z-IKBZbqdd',
                                                                        production: 'AWE1ttaQ_STrnDaMNBfpyQuufSWdP5yOUadRkwJt4QdwoTxva0NA7Uc4b04tYG8Uhc_So-DvmiumkBxt'
                                                                    },
                                                                    // Customize button (optional)
                                                                    locale: 'en_US',
                                                                    style: {
                                                                        size: 'small',
                                                                        color: 'gold',
                                                                        shape: 'pill',
                                                                        label: 'pay',
                                                                        fundingicons: true,

                                                                    },
                                                                    // Set up a payment
                                                                    payment: function (data, actions) {
                                                                        return actions.payment.create({
                                                                            transactions: [{
                                                                                amount: {
                                                                                    total: payform.amount.value,
                                                                                    currency: 'USD'
                                                                                },
                                                                                description: 'EssayEnergy Account Top UP.',
                                                                                custom: '{{Auth::user()->email}}'
                                                                            }]
                                                                        });
                                                                    },
                                                                    commit: true,

                                                                    // Execute the payment
                                                                    onAuthorize: function (data, actions) {
                                                                        return actions.payment.execute()
                                                                            .then(function () {
                                                                                // Show a confirmation message to the buyer
                                                                                //window.alert('Thank you for your purchase!');
                                                                                showReceived();
                                                                            });
                                                                    }
                                                                }, '#paypal-button');
                                                            </script>
                                                        </div>
                                                    </div>

                                                </form>

                                                <form class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12" role="form"
                                                          method="POST" enctype="multipart/form-data" action="{{ url('/pay/btc') }}">
                                                        {{ csrf_field() }}
                                                    <div class="row clearfix center">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group form-float">
                                                                <div class="form-line">
                                                                    <input type="number" min="1" required name="amountbtc" class="form-control">
                                                                    <label class="form-label">Amount(USD)</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <button type="submit" id="bitcoin-button" class="btn bg-orange waves-effect">PAY WITH BITCOINS</button>
                                                        </div>
                                                    </div>
                                                </form>


                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>



@endsection


    @section('scripts')

        <script>

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });

            });


            function showReceived() {
                swal({
                    title: 'Payment received!',
                    text: "We have received your payment and will credit your account in a moment",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Great!'
                },function() {
                    location.reload();

                });

            }

        </script>


        <!-- Ckeditor -->
        {{--<script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>--}}
        {{--<script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>--}}


    @endsection