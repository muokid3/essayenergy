@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{--Current Balance: $ {{auth()->user()->customer->balance}}--}}

                                {{--<br>--}}
                                {{--<br>--}}
                                {{--@if(Auth::user()->user_group == 1)--}}
                                    {{--<span><i><b>Reserved for Orders:  $ {{\App\Escrow::where('customer_id',Auth::user()->customer_id)->sum('total_charge')}}</b></i></span>--}}
                                {{--@endif--}}

                                {{--<br>--}}


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                            @endif


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="payments">
                                    <div class="card">
                                        <div class="body">
                                            <br>

                                            <iframe width="100%" height="600" frameborder="0" src="https://payments.essayenergy.com/crypto/bitcoin.php?amount={{$amount}}&orderID=10&userID={{auth()->user()->customer_id}}"></iframe>



                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>



@endsection


    @section('scripts')

        <script>

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });

            });


            function showReceived() {
                swal({
                    title: 'Payment received!',
                    text: "We have received your payment and will credit your account in a moment",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Great!'
                },function() {
                    location.reload();

                });

            }

        </script>


        <!-- Ckeditor -->
        {{--<script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>--}}
        {{--<script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>--}}


    @endsection