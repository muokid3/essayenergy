@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Order Details
                                <small>You can monitor the progress of your prder from this page</small>

                                <br>

                                <div class="alert alert-danger">Please reserve funds in order to publish your order</div>

                            @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->

                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <button class="btn btn-default btn-lg btn-block" type="button">ORDER ID <span class="badge">#{{$order->id}}</span></button>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-6 col-lg-6">
                                    Your order will cost <strong>$ {{$order->cost}}</strong> Kindly reserve funds to publish the order. Your account balance is <strong>${!! auth()->user()->customer->balance !!}</strong>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <span style="margin: 2px" class="btn btn-success btn-block" onclick="reserve_funds('{{$order->id}}','{{$order->cost}}');">
                                        RESERVE FUNDS
                                    </span>

                                </div>


                                {{--@if($order->status == 1)--}}
                                    {{--<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">--}}
                                        {{--<span class="btn btn-info btn-lg btn-block " type="button">Total Bids <span class="badge">{{\App\Bid::where('order_id',$order->id)->count()}}</span></span>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            </ul>



                                <div class="row bs-wizard" style="border-bottom:0;">

                                    <div class="col-xs-3 bs-wizard-step complete">
                                        <div class="text-center bs-wizard-stepnum">Step 1</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Create Order</div>
                                    </div>

                                    {{--<div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->--}}
                                    {{--<div class="text-center bs-wizard-stepnum">Step 2</div>--}}
                                    {{--<div class="progress"><div class="progress-bar"></div></div>--}}
                                    {{--<a href="#" class="bs-wizard-dot"></a>--}}
                                    {{--<div class="bs-wizard-info text-center"> Select a writer</div>--}}
                                    {{--</div>--}}

                                    <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 2</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Reserve funds</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 3</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Track Progress</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 4</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Receive completed work</div>
                                    </div>
                                </div>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                    <div class="card">
                                        <div class="body">

                                           <div class="row">
                                               <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12"
                                               style="border-right: 1px solid #eee;">
                                                   <img src="{{ Laricon::getImageDataUri(Auth::user()->email) }}" width="50%" class="img-rounded"  alt="{{Auth::user()->name}}" />
                                                   <br>
                                                   <h5>
                                                       #{{Auth::user()->name}}
                                                   </h5>

                                                   @switch(round(Auth::user()->avg_rating(),0))
                                                   @case(1)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(2)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(3)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(4)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(5)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   @break
                                                   @endswitch



                                                   <h5>Completed Orders: {{\App\Order::where('customer_id', Auth::user()->id)->where('status_id',5)->count()}}</h5>
                                                   <h5>Status: <span class="col-yellow">{{$order->order_status->code}}</span></h5>


                                               </div>

                                               <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12"
                                                    style="border-right: 1px solid #eee;">

                                                   <table class='table no-border' border="0" cellpadding="0" cellspacing="0">
                                                       <tbody>
                                                            <tr>
                                                                <td>Type of paper: </td>
                                                                <td>{{$order->type->name}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Topic: </td>
                                                                <td>{{$order->topic}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Pages: </td>
                                                                <td>{{$order->pages}} ({{$order->pages*\App\OrderCostSettings::find(1)->words_per_page}} words)</td>

                                                            </tr>

                                                            <tr>
                                                                <td>Discipline: </td>
                                                                <td>{{$order->discipline->discipline}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Type of service: </td>
                                                                <td>{{$order->type_of_service}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Format or citation style: </td>
                                                                <td>{{$order->citation}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Deadline: </td>
                                                                <td>
                                                                    {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                         ->format(' jS \\ F, Y h:i A')}}
                                                                    <br>

                                                                    {!!
                                                                        \Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline))) > \Carbon\Carbon::now()
                                                                            ?  "<span style=\"color: #4D953C\">" : "<span style=\"color: red\">"


                                                                    !!}

                                                                    ({{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))->diffForHumans()}})
                                                                    </span>
                                                                </td>
                                                            </tr>


                                                       </tbody>
                                                   </table>

                                               </div>

                                               <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12"
                                                    style="border-right: 1px solid #eee; ">
                                                       <h4 style="text-align: center;">
                                                           <u>Order Instructions</u>
                                                       </h4>
                                                   <br>

                                                   <div id="module" class="container">
                                                       <p class="collapse" id="collapseExample" aria-expanded="false">
                                                           {!!  $order->instructions !!}
                                                       </p>
                                                       <a role="button" class="collapsed" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                       </a>
                                                   </div>

                                               </div>

                                               <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12"
                                                        style="text-align: center;">
                                                   <h4>
                                                       <u>Additional Material (Files)</u>
                                                   </h4>

                                                   <br>

                                                   <ul style="text-align: left">
                                                       @if(!is_null($order->file_path))
                                                           <li>
                                                            <a href="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($order->file_path)}}" target="_blank">
                                                                   {{$order->file_path}}
                                                                   {{str_replace("order_files/", "", $order->file_path)}}
                                                               </a>
                                                            </li>
                                                       @endif

                                                   @foreach(\App\AdditionalMaterial::where('order_id',$order->id)->orderBy('id', 'asc')->get() as $additional)
                                                           <li>
                                                               <a href="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($additional->file_path)}}" target="_blank">
                                                                   {{str_replace("order_files/", "", $additional->file_path)}}
                                                               </a>
                                                           </li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                           </div>

                                            <div class="row">

                                                {{--@if($order->status == 1)--}}
                                                     {{--ongoing bidding --}}
                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                                        <button type="button" class="btn btn-lg btn-block btn-info waves-effect m-r-20" data-toggle="modal" data-target="#uploadAdditional">UPLOAD ADDITIONAL MATERIAL</button>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>





    <div class="modal fade" id="extendDeadline" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Extend Deadline</h4>
                </div>
                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/orders/extend_deadline') }}">
                    {{ csrf_field() }}

                    <div class="modal-body">

                        <p>Current Deadline:{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                             ->format(' jS \\ F, Y h:i A')}}

                            {!!   \Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline))) > \Carbon\Carbon::now()
                                                                                ?  "<span style=\"color: #4D953C\">" : "<span style=\"color: red\">"!!}

                            ({{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))->diffForHumans()}})
                            </span>

                        </p>

                        <input type="hidden" name="order_id" value="{{$order->id}}">


                        <div class="row clearfix margin-top-10">
                                <div style="margin: 15px;" class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="new_deadline" type="text"
                                                   class="datetimepicker form-control"  required placeholder="Please choose the new date & time..." >
                                        </div>
                                        <p style="color: red">{{$errors->first("new_deadline") }}</p>
                                    </div>
                                </div>
                            </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="uploadAdditional" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Upload Additional Material</h4>
                </div>

                <form enctype='multipart/form-data' method='POST' action='{{url('/orders/upload_additional')}}'>
                    {{ csrf_field() }}

                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <div class="modal-body">

                            <input onchange="makeFileList()" name="filesToUpload[]" id="filesToUpload" type="file" required multiple="" />

                        <p>
                        <ul>
                            <li>Accepted formats: jpg, jpeg, png, bmp, pdf, xls, xlsx, doc and docx</li>
                            <li>Maximum size per file: 2MB</li>
                            <li>Maximum files: 5</li>
                        </ul>

                        <strong>Files You Selected:</strong>
                        </p>
                        <ul id="fileList"><li>No Files Selected</li></ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">UPLOAD</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cancelOrder" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/orders/cancel') }}">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">Cancel Order</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="order_id" value="{{$order->id}}">

                            <div class="row clearfix" style="margin-top: 10px">
                                <div class="col-md-12 ">
                                    <div>
                                        <label>Cancellation Reason<span style="color: red">*</span></label>
                                        <textarea name="reason"  rows="5" class="form-control" placeholder="Please provide a valid reason for cancelling this order" aria-required="true"></textarea>
                                    </div>
                                    <p style="color: red">{{$errors->first("reason") }}</p>

                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">CANCEL ORDER</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="modal fade" id="addFunds" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">Insufficient Funds</h4>
                    </div>
                    <h5 style="text-align: center; color: red"><i>Please Add Funds to your account to continue</i></h5>
                    <div class="modal-body">

                            <div class="row clearfix" style="margin-top: 10px">
                                <form name="payform" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <label class="form-label">Payment Methods</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <img src="{{url('/inner/images/paypal.png')}}" width="80" height="20" alt="Paypal" />
                                            <img style="margin-left: 10px" src="{{url('/inner/images/bitcoin.png')}}" width="80" height="20" alt="Bitcoin" />
                                        </div>
                                    </div>

                                    <div class="row clearfix center">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" id="topAmount" min="0" name="amount" class="form-control">
                                                    <label class="form-label">Amount(USD)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div id="paypal-button"></div>
                                            <script src="https://www.paypalobjects.com/api/checkout.js"></script>
                                            <script>

                                                paypal.Button.render({
                                                    // Configure environment
                                                    env: 'production',
                                                    client: {
                                                        //sandbox: 'AeqZYY8uRCC9cp_l6xvxI7em5eK7UhXUoDpHjyqnPrFuKvzGj8S2LMFgWu0E0mS__UDJt5z-IKBZbqdd',
                                                        production: 'AWE1ttaQ_STrnDaMNBfpyQuufSWdP5yOUadRkwJt4QdwoTxva0NA7Uc4b04tYG8Uhc_So-DvmiumkBxt'
                                                    },
                                                    // Customize button (optional)
                                                    locale: 'en_US',
                                                    style: {
                                                        size: 'small',
                                                        color: 'gold',
                                                        shape: 'pill',
                                                        label: 'pay',
                                                        fundingicons: true,

                                                    },
                                                    // Set up a payment
                                                    payment: function (data, actions) {
                                                        return actions.payment.create({
                                                            transactions: [{
                                                                amount: {
                                                                    total: payform.amount.value,
                                                                    currency: 'USD'
                                                                },
                                                                description: 'EssayEnergy Account Top UP.',
                                                                custom: '{{Auth::user()->email}}'
                                                            }]
                                                        });
                                                    },
                                                    commit: true,

                                                    // Execute the payment
                                                    onAuthorize: function (data, actions) {
                                                        return actions.payment.execute()
                                                            .then(function () {
                                                                // Show a confirmation message to the buyer
                                                                //window.alert('Thank you for your purchase!');
                                                                showReceived();
                                                            });
                                                    }
                                                }, '#paypal-button');
                                            </script>
                                        </div>
                                    </div>
                                </form>

                                <form class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12" role="form"
                                      method="POST" enctype="multipart/form-data" action="{{ url('/pay/btc') }}">
                                    {{ csrf_field() }}
                                    <div class="row clearfix center">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" min="1" required name="amountbtc" class="form-control">
                                                    <label class="form-label">Amount(USD)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <button type="submit" id="bitcoin-button" class="btn bg-orange waves-effect">PAY WITH BITCOINS</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
        </div>
    </div>


@endsection


    @section('css')
        <style>

            .star-rating {
                direction: rtl;
                display: inline-block;
                padding: 20px
            }

            .star-rating label:before,
            .star-rating label:after {
                display: none;
            }

            .star-rating label {
                color: #bbb;
                font-size: 18px;
                padding: 0;
                cursor: pointer;
                -webkit-transition: all .3s ease-in-out;
                transition: all .3s ease-in-out
            }

            .star-rating label:hover,
            .star-rating label:hover~label,
            .star-rating input[type=radio]:checked~label {
                color: #f2b600
            }

            .bs-wizard {margin-top: 40px;}

            /*Form Wizard*/
            .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
            .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
            .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
            .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
            .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
            .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
            .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
            .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
            .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
            .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
            .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
            .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
            .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
            .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }










            #module {
                width: 100%;
            }

            #module p.collapse[aria-expanded="false"] {
                display: block;
                height: 40px !important;
                overflow: hidden;
            }

            #module p.collapsing[aria-expanded="false"] {
                height: 40px !important;
            }

            #module a.collapsed:after  {
                content: '+ Show More';
            }

            #module a:not(.collapsed):after {
                content: '- Show Less';
            }

        </style>
    @endsection

    @section('scripts')

        <script>

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });

            });

            function makeFileList() {
                var input = document.getElementById("filesToUpload");
                var ul = document.getElementById("fileList");
                while (ul.hasChildNodes()) {
                    ul.removeChild(document.getElementById('fileList').firstChild);
                }
                for (var i = 0; i < input.files.length; i++) {
                    var li = document.createElement("li");
                    li.innerHTML = input.files[i].name;
                    ul.appendChild(li);
                }
                if(!ul.hasChildNodes()) {
                    var li = document.createElement("li");
                    li.innerHTML = 'No Files Selected';
                    ul.appendChild(li);
                }
            }

            function confirm_order(orderID){
                bootbox.confirm("Mark this order as received? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/orders/mark_complete/' + orderID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

            function republish_order(orderID){
                bootbox.confirm("Are you sure you want to republish this order? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/orders/republish/' + orderID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

            function showReceived() {
                swal({
                    title: 'Payment received!',
                    text: "We have received your payment and will credit your account in a moment",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Great!'
                },function() {
                    location.reload();

                });

            }

            function reserve_funds(orderID, orderCost){
                bootbox.confirm("Reserve $"+orderCost+" for this order?", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/orders/reserve/' + orderID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (response) {
                                response = JSON.parse(response);

                                if (response.error) {
                                    //bootbox.alert(response.message);
                                    $('#addFunds').modal('show');
                                } else {
                                    bootbox.alert("Funds have been reserved successfully");
                                    var millisecondsToWait = 2000;
                                    setTimeout(function() {
                                        location.href = '/orders/details/'+orderID;
                                    }, millisecondsToWait);
                                }

                            },
                            error: function (request, status, error) {
                                bootbox.alert("Bid or order not found");
                        }
                        });
                    }
                });
            }


        </script>


        <!-- Ckeditor -->
        <script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>


    @endsection