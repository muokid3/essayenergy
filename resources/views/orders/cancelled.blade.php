@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cancelled Orders
                                <small>Orders That have been cancelled by you or the writer</small>

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#addorder" data-toggle="tab">ORDERS IN CANCELLATION</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                    <div class="card">
                                        <div class="body">

                                            <table class="table table-striped table-responsive">
                                                <thead>
                                                <tr>
                                                    <th class="hidden-xs">Order No.</th>
                                                    <th class="hidden-xs">Topic</th>
                                                    <th class="hidden-xs">Type Of paper</th>
                                                    <th>Pages (Words)</th>
                                                    <th class="hidden-xs">Deadline</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($orders as $order)

                                                    <tr>
                                                        <td>
                                                            #{{$order->id}}
                                                        </td>

                                                        <td class="hidden-xs">
                                                            {{$order->topic}}
                                                        </td>

                                                        <td class="hidden-xs">
                                                            {{$order->type->name}}
                                                        </td>

                                                        <td>
                                                            {{$order->pages}} <span style="color: red">({{$order->pages*\App\OrderCostSettings::find(1)->words_per_page}} words)</span>
                                                        </td>

                                                        <td class="hidden-xs">
                                                            {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                            ->format(' jS \\ F, Y h:i A')}}

                                                        </td>

                                                        <td>
                                                            <a class="btn btn-success" href="{{url('/orders/details/'.$order->id)}}">
                                                                View Order
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            {{$orders->links()}}



                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>

@endsection


    @section('css')
        <style>
            .bs-wizard {margin-top: 40px;}

            /*Form Wizard*/
            .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
            .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
            .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
            .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
            .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
            .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
            .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
            .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
            .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
            .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
            .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
            .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
            .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
            .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
            /*END Form Wizard*/
        </style>
    @endsection
    @section('scripts')

        <script>

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });



                if (document.getElementById('other').checked) {
                    document.getElementById('other_input').style.display = 'block';
                }
                    else document.getElementById('other_input').style.display = 'none';

            });


            function buttonChanged() {
                if (document.getElementById('other').checked) {
                    document.getElementById('other_input').style.display = 'block';
                }
                else document.getElementById('other_input').style.display = 'none';


            }

        </script>


        <!-- Ckeditor -->
        {{--<script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>--}}
        {{--<script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>--}}


    @endsection