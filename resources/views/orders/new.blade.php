@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Place new Order
                                <small>Fill in Order details -> Order Bidding -> Choose Writer and reserve money ->Track progress </small>

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#addorder" data-toggle="tab">ORDER DETAILS</a></li>

                            </ul>

                                <div class="row bs-wizard" style="border-bottom:0;">

                                    <div class="col-xs-3 bs-wizard-step active">
                                        <div class="text-center bs-wizard-stepnum">Step 1</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Create Order</div>
                                    </div>

                                    {{--<div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->--}}
                                        {{--<div class="text-center bs-wizard-stepnum">Step 2</div>--}}
                                        {{--<div class="progress"><div class="progress-bar"></div></div>--}}
                                        {{--<a href="#" class="bs-wizard-dot"></a>--}}
                                        {{--<div class="bs-wizard-info text-center"> Select a writer</div>--}}
                                    {{--</div>--}}

                                    <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 2</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Reserve funds</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 3</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Track Progress</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 4</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Receive completed work</div>
                                    </div>
                                </div>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/orders/new/create') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-body">

                                            <div class="card">
                                                <div class="body">
                                                    <h2 class="card-inside-title">Basic Details</h2>

                                                    <div class="row">
                                                        <div class="col-xs-12 col-lg-8 col-md-8">

                                                            <div class="row clearfix">
                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="form-group form-float">
                                                                        <div class="form-line">
                                                                            <label>Type of Paper <span style="color: red">*</span></label>
                                                                            {!! Form::select('type', \App\Type::pluck('name', 'id'), Auth::user()->first_order_complete ? 1 : Auth::user()->first_order_type,
                                                                                    ['class' => ' form-control show-tick', 'id'=>'type','required', 'data-live-search'=>'true']) !!}
                                                                        </div>
                                                                        <p style="color: red">{{$errors->first("type") }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row clearfix margin-top-10">
                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="form-group form-float">
                                                                        <div class="form-line">
                                                                            <input min="1" name="pages" type="number"  value="{{ Auth::user()->first_order_complete ? '' : Auth::user()->first_order_pages}}" class="form-control">
                                                                            <label class="form-label">Pages<span style="color: red">*</span></label>
                                                                        </div>
                                                                        <p style="color: red">{{$errors->first("pages") }}</p>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row clearfix margin-top-10">

                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input name="deadline" type="text"
                                                                                   class="datetimepicker form-control"  required placeholder="Select deadline (date & time)" >
                                                                        </div>
                                                                        <p style="color: red">{{$errors->first("deadline") }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row clearfix margin-top-10">
                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="form-group form-float">
                                                                        <div class="form-line">
                                                                            <input type="text" name="topic" class="form-control">
                                                                            <label class="form-label">Topic<span style="color: red">*</span></label>
                                                                        </div>
                                                                        <p style="color: red">{{$errors->first("topic") }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="body">
                                                    <h2 class="card-inside-title">Additional Information</h2>

                                                    <div class="row clearfix">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <div class="form-group form-float">
                                                                <div class="form-line">
                                                                    <label>Discipline<span style="color: red">*</span></label>
                                                                    {!! Form::select('discipline', \App\Discipline::pluck('discipline', 'id'), null,
                                                                            ['class' => ' form-control show-tick', 'id'=>'type','required', 'data-live-search'=>'true']) !!}
                                                                </div>
                                                                <p style="color: red">{{$errors->first("discipline") }}</p>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row clearfix" style="margin-top: 10px">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <div class="form-group form-float">
                                                                <div class="form-line">
                                                                    <label>Type of Service<span style="color: red">*</span></label>
                                                                    <div class="demo-radio-button">

                                                                        <input name="service_type" type="radio" class="with-gap" value="Writing from scratch" checked id="radio_3" />
                                                                        <label for="radio_3">Writing from scratch</label>

                                                                        <input name="service_type" type="radio" id="radio_4" value="Editing or rewriting" class="with-gap" />
                                                                        <label for="radio_4">Editing or rewriting</label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p style="color: red">{{$errors->first("service_type") }}</p>

                                                        </div>
                                                    </div>

                                                     <div class="row clearfix" style="margin-top: 10px">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <div class="form-group form-float">
                                                                <div class="form-line">
                                                                    <label>Format/Citation Style<span style="color: red">*</span></label>
                                                                    <div id="citations" class="demo-radio-button">
                                                                        <input name="citation" type="radio" onclick="buttonChanged();" class="with-gap" value="MLA" checked id="mla" />
                                                                        <label for="mla">MLA</label>

                                                                        <input name="citation" type="radio" onclick="buttonChanged();" id="apa" value="APA" class="with-gap" />
                                                                        <label for="apa">APA</label>

                                                                        <input name="citation" type="radio" onclick="buttonChanged();" id="chicago" value="Chicago" class="with-gap" />
                                                                        <label for="chicago">Chicago</label>

                                                                        <input name="citation" type="radio" onclick="buttonChanged();" id="na" value="Not Applicable" class="with-gap" />
                                                                        <label for="na">Not Applicable</label>

                                                                        <input name="citation" type="radio" onclick="buttonChanged();" id="other" value="Other" class="with-gap" />
                                                                        <label for="other">Other</label>

                                                                        <div id="other_input" class="form-line">
                                                                            <input type="text" name="citation_other" class="form-control">
                                                                            <label class="form-label">Enter format or citation style</label>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p style="color: red">{{$errors->first("citation") }}</p>

                                                        </div>
                                                    </div>

                                                    <!-- CKEditor -->
                                                    <div class="row clearfix" style="margin-top: 10px">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <div style="margin-right: -15px; margin-left: -15px;">
                                                                <label>Paper Instructions<span style="color: red">*</span></label>
                                                                <textarea name="instructions"  rows="5" class="form-control" placeholder="Any other relevant information that may include: structure and/or outline, grading scale, types and number of references to be used, specific number of words not be exceeded, academic level, or any other requirements." aria-required="true"></textarea>
                                                            </div>
                                                            <p style="color: red">{{$errors->first("instructions") }}</p>

                                                        </div>
                                                    </div>
                                                    <!-- #END# CKEditor -->


                                                    <div class="row clearfix">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <div style="margin-right: -15px; margin-left: -15px;">
                                                                <label>Upload additional Information</label>
                                                                <input type="file" name="additional_info_file">

                                                                <i style="color: red;">Max upload size is 5MB</i>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row clearfix">
                                                        <div class="col-md-6 col-md-offset-1">
                                                            <div style="margin-right: -15px; margin-left: -15px;">
                                                                <button type="submit" class="btn btn-success btn-block waves-effect">Create order</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>

@endsection


    @section('css')
        <style>
            .bs-wizard {margin-top: 40px;}

            /*Form Wizard*/
            .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
            .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
            .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
            .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
            .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
            .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
            .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
            .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
            .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
            .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
            .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
            .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
            .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
            .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
            /*END Form Wizard*/
        </style>
    @endsection
    @section('scripts')

        <script>

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });


                if (document.getElementById('other').checked) {
                    document.getElementById('other_input').style.display = 'block';
                }
                    else document.getElementById('other_input').style.display = 'none';

            });


            function buttonChanged() {
                if (document.getElementById('other').checked) {
                    document.getElementById('other_input').style.display = 'block';
                }
                else document.getElementById('other_input').style.display = 'none';


            }



        </script>


    @endsection