<!DOCTYPE html>
<html lang="en">
<head>
    <title>Essay Energy</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description" content="Essay Energy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    <link href="{{url('/form-select2/select2.css')}}" type="text/css" rel="stylesheet"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />


    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


</head>
<body>

<div class="super_container">

    @include('layouts.header')



    <div class="courses">
        <div class="container">
            <div class="row">
                <div class="counter">
                    <div class="counter_background" style="background-image:url(images/counter_background.jpg)"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="counter_content">
                                    <h2 class="counter_title">Just place an order!</h2>
                                    <div class="counter_text">
                                        <p>
                                          To become a customer on Essay Energy, just fill out the order form with the order instructions using the subtitles given and place an order


                                        </p>
                                    </div>

                                    <!-- Milestones -->

                                    <div class="milestones d-flex flex-md-row flex-column align-items-center justify-content-between">

                                        <!-- Milestone -->
                                        <div class="milestone">
                                            {{--<div class="milestone_counter" data-end-value="{{\App\Order::where('status_id', 5)->count()}}">0</div>--}}
                                            <div class="milestone_counter" data-end-value="{{config('constants.orders')}}">0</div>
                                            <div class="milestone_text">Completed Orders</div>
                                        </div>

                                        <!-- Milestone -->
                                        <div class="milestone">
                                            {{--<div class="milestone_counter" data-end-value="{{\App\User::where('user_group',1)->count()}}">0</div>--}}
                                            <div class="milestone_counter" data-end-value="{{config('constants.customers')}}">0</div>
                                            <div class="milestone_text">Happy Customers</div>
                                        </div>

                                        {{--<!-- Milestone -->--}}
                                        {{--<div class="milestone">--}}
                                        {{--<div class="milestone_counter" data-end-value="4">0</div>--}}
                                        {{--<div class="milestone_text">Online Writers</div>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="counter_form">
                            <div class="row fill_height">
                                <div class="col fill_height">
                                    @guest

                                        {{--<form class="counter_form_content d-flex flex-column align-items-center justify-content-center" action="{{url('/register')}}" method="post">--}}
                                            {{--{{csrf_field()}}--}}
                                            {{--<input type="hidden" name="user_group"  value="1">--}}
                                            {{--<div class="counter_form_title">place order now</div>--}}

                                            {{--<a href="{{url('/')}}" class="btn btn-success btn-block">Get Started Now</a>--}}
                                        {{--</form>--}}
                                        <form class="counter_form_content d-flex flex-column align-items-center justify-content-center" action="{{url('/register/express')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="user_group"  value="1">

                                            @if (Session::has('message'))
                                                <div class="alert alert-info">{{ Session::get('message') }}</div>
                                            @endif
                                            @if (Session::has('error'))
                                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                            @endif
                                            @if (Session::has('success'))
                                                <div class="alert alert-success">{{ Session::get('success') }}</div>
                                            @endif

                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <div class="counter_form_title">sign up now</div>

                                            {{ $errors->first('email') }}

                                            <input type="email" name="email" class="counter_input" placeholder="E-Mail" required="required">
                                            @if ($errors->has('email'))
                                                @section('scripts')
                                                    <script type="text/javascript">
                                                        $(document).ready(function(){
                                                            $('#newLoginModal').modal('show');
                                                        });
                                                    </script>
                                                @endsection
                                            @endif

                                            <button type="submit" class="btn btn-success btn-block">Get Started Now</button>
                                        </form>
                                    @else
                                        <div style="text-align: center;">
                                            <div class="counter_form_title">place order now</div>

                                            <a href="{{url('orders/new')}}" class="btn btn-success align-center" style="margin-top: 20px">Place your order now</a>

                                        </div>

                                    @endauth
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <!-- Footer -->

    @include('layouts.footer')

    <!-- Modal -->
    <div class="modal fade" id="newLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sign in to continue to Essay Energy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 mx-auto">
                                <div class="account-wall">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">

                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Password" autofocus required>
                                        @if ($errors->has('password'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                        @if ($errors->has('email'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('email') }}</strong></span>
                                        @endif

                                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                                            Sign in</button>

                                        <a href="{{url('/password/reset')}}"  class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<link rel="stylesheet" href="{{url('/build/css/bootstrap-datetimepicker.min.css')}}">

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>




<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="{{ url('/form-select2/select2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{url('build/js/bootstrap-datetimepicker.min.js')}}"></script>

@yield('scripts')



<script>
    $(document).ready(function () {
        $('select:not(#specialDude)').removeClass('form-control');
        $('select:not(#specialDude)').select2({width: '100%'});

//        $(function() {
//            $( ".datetimepicker" ).datetimepicker({
//
//            });
//        });

        $('.datetimepicker').datetimepicker({
            minDate: moment()
        });


//        $(function () {
//            $("#deadlineDate").datepicker({
//                //viewMode: 'years',
//                format: 'DD-MM-YYYY'
//                //maxDate: moment().subtract(18, 'years')
//            });
//        });
    });
</script>

</body>

</html>
