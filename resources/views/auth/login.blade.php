<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description" content="Essay Energy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    <link href="{{url('/form-select2/select2.css')}}" type="text/css" rel="stylesheet"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    {{--<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.css" rel="stylesheet"/>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.js"></script>--}}
<style>
    .form-signin
    {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox
    {
        margin-bottom: 10px;
    }
    .form-signin .checkbox
    {
        font-weight: normal;
    }
    .form-signin .form-control
    {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .form-signin .form-control:focus
    {
        z-index: 2;
    }
    .form-signin input[type="text"]
    {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }
    .form-signin input[type="password"]
    {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    .account-wall
    {
        margin-top: 20px;
        padding: 40px 0px 20px 0px;
        background-color: #f7f7f7;
        -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    }
    .login-title
    {
        color: #555;
        font-size: 18px;
        font-weight: 400;
        display: block;
    }
    .profile-img
    {
        width: 96px;
        height: 96px;
        margin: 0 auto 10px;
        display: block;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
    }
    .need-help
    {
        margin-top: 10px;
    }
    .new-account
    {
        display: block;
        margin-top: 10px;
    }
</style>

</head>
<body>

<div class="super_container">

    @include('layouts.header')




    <div class="courses" style="padding-top: 140px;">
        <div class="section_background parallax-window" data-parallax="scroll" data-image-src="images/courses_background.jpg" data-speed="0.8"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class=" text-center">
                        <h2 class="section_title">Log in to your account</h2>
                    </div>
                </div>
            </div>
            <div class="row courses_row">

                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <h1 class="text-center login-title">Sign in to continue to Essay Energy</h1>
                            <div class="account-wall">
                                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                                     alt="">
                                    <div class="row fill_height">
                                        <div class="col fill_height">

                                            <div class="row">
                                                @if (Session::has('message'))
                                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                                @endif
                                                @if (Session::has('error'))
                                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                                @endif
                                                @if (Session::has('success'))
                                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                                @endif

                                            </div>


                                            <form style="padding: 5%" class="counter_form_content d-flex flex-column align-items-center justify-content-center"
                                                  method="POST" action="{{ url('/login') }}">
                                                {{ csrf_field() }}
                                                <div class="counter_form_title">log in to continue</div>
                                                <input type="email" name="email" class="counter_input" placeholder="E-Mail" required="required">
                                                @if ($errors->has('email'))
                                                    <span class="help-block"> <strong>{{ $errors->first('email') }}</strong></span>
                                                @endif
                                                <input type="password" name="password" class="counter_input form-control"   placeholder="Password" required="required">
                                                @if ($errors->has('password'))
                                                    <span class="help-block"> <strong>{{ $errors->first('password') }}</strong></span>
                                                @endif

                                                <button type="submit" class="btn btn-success btn-block">Log In</button>
                                            </form>

                                            <a href="{{url('/password/reset')}}" class="text-center new-account">Forgot your password?</a>

                                        </div>
                                    </div>

                            </div>
                            <a href="{{url('register')}}" class="text-center new-account">Create an account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Footer -->
    @include('layouts.footer')
</div>

<link rel="stylesheet" href="{{url('/build/css/bootstrap-datetimepicker.min.css')}}">

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>




<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="{{ url('/form-select2/select2.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ url('/js/moment.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<script src="{{url('build/js/bootstrap-datetimepicker.min.js')}}"></script>


{{--<script src="{{ url('/js/datepicker.min.js') }}"></script> <!-- All Scripts -->--}}
{{--<script src="{{ url('/js/datetimepicker.js') }}"></script> <!-- All Scripts -->--}}



<script>
    $(document).ready(function () {
        $('select:not(#specialDude)').removeClass('form-control');
        $('select:not(#specialDude)').select2({width: '100%'});

//        $(function() {
//            $( ".datetimepicker" ).datetimepicker({
//
//            });
//        });

        $('.datetimepicker').datetimepicker({
            minDate: moment()
        });


//        $(function () {
//            $("#deadlineDate").datepicker({
//                //viewMode: 'years',
//                format: 'DD-MM-YYYY'
//                //maxDate: moment().subtract(18, 'years')
//            });
//        });
    });
</script>

</body>

</html>