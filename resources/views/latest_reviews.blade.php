<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <title>Latest Reviews</title>
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/courses.css">
    <link rel="stylesheet" type="text/css" href="styles/courses_responsive.css">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description" content="Great Assistance. The support team constantly updated me on the paper’s progress. I would recommend the site">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{url('styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{url('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('plugins/colorbox/colorbox.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{url('styles/main_styles.css')}}">

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


</head>
<body>

<div class="super_container">


@include('layouts.header')


<!-- Home -->

    <div class="home" style="height: 200px">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>Latest Reviews</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="courses">
        <div class="container">
            <div class="row">

                <!-- Courses Main Content -->
                <div class="col-lg-8">
                    <div class="courses_container">
                        <div class="row courses_row">

                            {{--@foreach($reviews as $review)--}}
                            {{--<!-- Course -->--}}
                            {{--<div class="col-lg-12 course_col">--}}
                                {{--<div class="course">--}}
                                    {{--<div class="course_body">--}}
                                        {{--<h3 class="course_title">{{$review->topic}}</h3>--}}
                                        {{--<hr>--}}
                                        {{--<div class="course_text">--}}
                                            {{--<table style="width:100%">--}}
                                                {{--<tr>--}}
                                                    {{--<td>{{\App\Type::find($review->type_id)->name}}, {{\App\Discipline::find($review->discipline_id)->discipline}}. {{$review->pages}} Pages</td>--}}
                                                    {{--<td>#{{$review->order_id}}, <b class="text-success">Completed</b></td>--}}
                                                {{--</tr>--}}
                                                {{--<tr><td> &nbsp;</td></tr>--}}

                                                {{--<tr>--}}
                                                    {{--<td>--}}
                                                        {{--Customer's feedback:--}}

                                                        {{--@switch($review->rating)--}}
                                                            {{--@case(1)--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--@break--}}
                                                            {{--@case(2)--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--@break--}}
                                                            {{--@case(3)--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--@break--}}
                                                            {{--@case(4)--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star"></span>--}}
                                                            {{--@break--}}
                                                            {{--@case(5)--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--<span class="fa fa-star" style="color: orange;"></span>--}}
                                                            {{--@break--}}
                                                        {{--@endswitch--}}


                                                    {{--</td>--}}
                                                {{--</tr>--}}

                                                {{--<tr>--}}
                                                    {{--<td>--}}
                                                        {{--<div class="blog_quote d-flex flex-row align-items-center justify-content-start">--}}
                                                            {{--<i class="fa fa-quote-left" aria-hidden="true" style="color: #14bdee"></i><i>{{$review->review}}</i>--}}
                                                        {{--</div>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}

                                            {{--</table>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="course_footer">--}}
                                        {{--<div class="course_footer_content d-flex flex-row align-items-center justify-content-start">--}}
                                            {{--<div class="course_info">--}}
                                                {{--<i class="fa fa-clock-o" aria-hidden="true"></i>--}}
                                                {{--<span>{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($review->created_at)))->diffForHumans()}}</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--@endforeach--}}

                            @foreach($reviews as $review)
                            <!-- Course -->
                            <div class="col-lg-12 course_col">
                                <div class="course">
                                    <div class="course_body">
                                        <h4 class="course_title">{{\App\Discipline::find($review->paper_discipline)->discipline}}</h4>
                                        <hr>
                                        <div class="course_text">
                                            <table style="width:100%">
                                                <tr>
                                                    <td>{{\App\Type::find($review->paper_type)->name}}. {{$review->pages}} Pages</td>
                                                    <td>#{{$review->order_id}}, <b class="text-success">Completed</b></td>
                                                </tr>
                                                <tr><td> &nbsp;</td></tr>

                                                <tr>
                                                    <td>
                                                        Customer's feedback:

                                                        @switch($review->rating)
                                                            @case(1)
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            @break
                                                            @case(2)
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            @break
                                                            @case(3)
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            @break
                                                            @case(4)
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star"></span>
                                                            @break
                                                            @case(5)
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            <span class="fa fa-star" style="color: orange;"></span>
                                                            @break
                                                        @endswitch


                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="blog_quote d-flex flex-row align-items-center justify-content-start">
                                                            <i class="fa fa-quote-left" aria-hidden="true" style="color: #14bdee"></i><i>{{$review->review}}</i>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                                    <div class="course_footer">
                                        <div class="course_footer_content d-flex flex-row align-items-center justify-content-start">
                                            <div class="course_info">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                <span>{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($review->created_at)))->diffForHumans()}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="sidebar">

                        <!-- Categories -->
                        <div class="sidebar_section">
                            {{--<div class="sidebar_section_title">Place an order now</div>--}}
                            <div class="sidebar_categories">
                                @guest

                                    <form class="counter_form_content d-flex flex-column align-items-center justify-content-center" action="{{url('/register')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user_group"  value="1">
                                        <div class="counter_form_title">place order now</div>

                                        {{ $errors->first('email') }}

                                        <input type="email" name="email" class="counter_input" placeholder="E-Mail" required="required">
                                        @if ($errors->has('email'))
                                        @section('scripts')
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                    $('#newLoginModal').modal('show');
                                                });
                                            </script>
                                        @endsection
                                        @endif
                                        {{--{!! Form::select('first_order_type', \App\Type::pluck('name', 'id'), null,--}}
                                        {{--['class' => 'dropdown_item_select counter_input', 'id'=>'specialDude','required']) !!}--}}

                                        <select id="specialDude" required  class="dropdown_item_select counter_input" name="first_order_type">
                                            <option value="">Type of paper</option>
                                            @foreach(\App\Type::all() as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach

                                        </select>


                                        <input type="number" name="first_order_pages" min="1" class="counter_input" placeholder="Number of pages" required="required">

                                        <button type="submit" class="btn btn-success btn-block">Get Started Now</button>
                                    </form>

                                @else
                                    <div style="text-align: center;" class="counter_form_content d-flex flex-column align-items-center justify-content-center">
                                        <div class="counter_form_title">place order now</div>

                                        <a href="{{url('orders/new')}}" class="btn btn-success align-center" style="margin-top: 20px">Place your order now</a>

                                    </div>

                                @endauth
                            </div>
                        </div>

                        <!-- Latest posts -->
                        <div class="sidebar_section">
                            <div class="sidebar_section_title">Latest Posts</div>
                            <div class="sidebar_latest">

                            @foreach($latest as $blogItem)
                                <!-- Latest Post -->
                                    <div class="latest d-flex flex-row align-items-start justify-content-start">
                                        <div class="latest_image">
                                            <div>
                                                <img src="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($blogItem->image_link)}}" alt="">
                                            </div>
                                        </div>
                                        <div class="latest_content">
                                            <div class="latest_title"><a href="{{url('blog/'.$blogItem->slug)}}">{{$blogItem->title}}</a></div>
                                            <div class="latest_date">{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($blogItem->created_at)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}</div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <!-- Banner -->
                        <div class="sidebar_section">
                            <div class="sidebar_banner d-flex flex-column align-items-center justify-content-center text-center">
                                <div class="sidebar_banner_background" style="background-image:url(images/banner_1.jpg)"></div>
                                <div class="sidebar_banner_overlay"></div>
                                <div class="sidebar_banner_content">
                                    <div class="banner_title">High quality papers</div>
                                    <div class="banner_button"><a href="{{url('how_it_works')}}">learn more</a></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>


    @include('layouts.footer')

<!-- Modal -->
    <div class="modal fade" id="newLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sign in to continue to Essay Energy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 mx-auto">
                                <div class="account-wall">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">

                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Password" autofocus required>
                                        @if ($errors->has('password'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                        @if ($errors->has('email'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('email') }}</strong></span>
                                        @endif

                                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                                            Sign in</button>

                                        <a href="{{url('/password/reset')}}"  class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



</div>

    <style type="text/css">
        .checked {
            color: orange;
        }

    </style>

<script src="{{url('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{url('styles/bootstrap4/popper.js')}}"></script>
<script src="{{url('styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{url('plugins/easing/easing.js')}}"></script>
<script src="{{url('plugins/parallax-js-master/parallax.min.js')}}"></script>
<script src="{{url('plugins/colorbox/jquery.colorbox-min.js')}}"></script>
<script src="{{url('js/blog_single.js')}}"></script>


@yield('scripts')




</body>

</html>