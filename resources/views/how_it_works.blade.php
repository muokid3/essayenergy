<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <title>About</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Place your order, confirm your email and password sent via e-mail, upload the order instructions and reserve the money, chat with your writer and confirm the complete task, release the payment and finally leave a review! Simple!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/about.css">
    <link rel="stylesheet" type="text/css" href="styles/about_responsive.css">

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    
</head>
<body>

<div class="super_container">


    @include('layouts.header')


    <div class="home">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>How it works</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Feature -->

    <div class="feature">
        <div class="feature_background" style="background-image:url(images/courses_background.jpg)"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <h2 class="section_title">How it Works</h2>
                        <div class="section_subtitle"><p>
                                Place your order, confirm your email and password sent via e-mail, upload the order
                                instructions and reserve the money, chat with your writer and confirm the complete task,
                                release the payment and finally leave a review! Simple!
                            </p></div>
                    </div>
                </div>
            </div>
            <div class="row feature_row">

                <!-- Feature Content -->
                <div class="col-lg-6 feature_col">
                    <div class="feature_content">
                        <!-- Accordions -->
                        <div class="accordions">

                            <div class="elements_accordions">

                                <div class="accordion_container">
                                    <div class="accordion d-flex flex-row align-items-center active"><div>Place your order</div></div>
                                    <div class="accordion_panel">
                                        <p>Fill out the order form with the order instructions using the subtitles given</p>
                                    </div>
                                </div>

                                <div class="accordion_container">
                                    <div class="accordion d-flex flex-row align-items-center"><div>Reserve money</div></div>
                                    <div class="accordion_panel">
                                        <p>Reserve the funds for your order with our secure escrow system</p>
                                    </div>
                                </div>

                                <div class="accordion_container">
                                    <div class="accordion d-flex flex-row align-items-center"><div>Communicate with writers</div></div>
                                    <div class="accordion_panel">
                                        <p>Engage your writer via chat and choose a writer of your preference</p>
                                    </div>
                                </div>



                                <div class="accordion_container">
                                    <div class="accordion d-flex flex-row align-items-center"><div>Track your order progress</div></div>
                                    <div class="accordion_panel">
                                        <p>Feel free to request for a draft of your work and review the paper progress</p>
                                    </div>
                                </div>

                                <div class="accordion_container">
                                    <div class="accordion d-flex flex-row align-items-center"><div>Your paper is ready!</div></div>
                                    <div class="accordion_panel">
                                        <p>Pay for the paper and download it. Request for a free revision if necessary!</p>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- Accordions End -->
                    </div>
                </div>

                <!-- Feature Video -->
                <div class="col-lg-6 feature_col">
                    <div class="feature_video d-flex flex-column align-items-center justify-content-center">
                        <iframe width="100%" height="480" src="https://www.youtube.com/embed/qORISyMhurU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @include('layouts.footer')


</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/about.js"></script>

</body>

</html>