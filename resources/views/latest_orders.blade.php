<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <title>Latest Orders</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Essay Energy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/courses.css">
    <link rel="stylesheet" type="text/css" href="styles/courses_responsive.css">

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


</head>
<body>

<div class="super_container">


@include('layouts.header')


<!-- Home -->

    <div class="home">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>Latest Orders</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="courses">
        <div class="container">
            <div class="row">

                <!-- Courses Main Content -->
                <div class="col-lg-12">
                    <div class="courses_container">
                        <div class="row courses_row">

                            @foreach($orders as $order)
                            <!-- Course -->
                            <div class="col-lg-4 course_col">
                                <div class="course">
                                    <div class="course_body">
                                        <h3 class="course_title">{{$order->topic}}</h3>
                                        <div class="course_teacher"><i class="fa fa-user" style="color: #00A6C7" aria-hidden="true"></i> {{$order->customer->name}}</div>
                                        <div class="course_text">
                                            <table style="width:100%">
                                                <tr>
                                                    <td>Type</td>
                                                    <td>: {{$order->type->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Deadline</td>
                                                    <td>:
                                                        {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                                  ->format(' jS \\ F, Y h:i A')}}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Pages</td>
                                                    <td>: {{$order->pages}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="course_footer">
                                        <div class="course_footer_content d-flex flex-row align-items-center justify-content-start">
                                            <div class="course_info">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                <span>{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))->diffForHumans()}}</span>
                                            </div>
                                            <div class="course_price ml-auto"><span style="text-decoration: none">Cost:</span>${{$order->cost}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach





                        </div>
                        <div class="row pagination_row">
                            <div class="col">
                                <div class="pagination_container d-flex flex-row align-items-center justify-content-center">
                                    <ul class="pagination_list">
                                        <button onclick="location.href='{{url('/dashboard')}}'" class="courses_search_button ml-auto">see more </button>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    @include('layouts.footer')


</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>



</body>

</html>