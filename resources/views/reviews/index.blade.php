@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Reviews
                                <small>This is what other people you have worked with have rated you.</small>

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->


                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">

                                    <div class="card">
                                        <div class="body">

                                            <h4>Average Rating: {{round(Auth::user()->avg_rating(),1)}}</h4>

                                            @foreach($reviews as $review)

                                                <blockquote>
                                                    <p>{{$review->review}}</p>
                                                    @switch(round($review->rating,0))
                                                    @case(1)
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    @break
                                                    @case(2)
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    @break
                                                    @case(3)
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    @break
                                                    @case(4)
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star"></span>
                                                    @break
                                                    @case(5)
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    <span class="fa fa-star" style="color: orange;"></span>
                                                    @break
                                                    @endswitch

                                                    <footer>{{\App\User::find($review->rated_by)->name}}</footer>
                                                </blockquote>

                                            @endforeach

                                            {{$reviews->links()}}

                                        </div>

                                    </div>


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>






@endsection


    @section('css')
        <style>
            .bs-wizard {margin-top: 40px;}

            /*Form Wizard*/
            .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
            .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
            .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
            .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
            .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
            .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
            .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
            .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
            .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
            .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
            .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
            .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
            .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
            .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
            .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
            .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
            /*END Form Wizard*/



            .checked {
                color: orange;
            }

            .table.no-border tr td, .table.no-border tr th {
                border-width: 0;
            }

            .chat
            {
                list-style: none;
                margin: 0;
                padding: 0;
            }

            .chat li
            {
                margin-bottom: 10px;
                padding-bottom: 5px;
                border-bottom: 1px dotted #B3A9A9;
            }

            .chat li.left .chat-body
            {
                margin-left: 60px;
            }

            .chat li.right .chat-body
            {
                margin-right: 60px;
            }


            .chat li .chat-body p
            {
                margin: 0;
                color: #777777;
            }

            .panel .slidedown .glyphicon, .chat .glyphicon
            {
                margin-right: 5px;
            }

            .panel-body
            {
                overflow-y: scroll;
                height: 500px;
            }

            ::-webkit-scrollbar-track
            {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #F5F5F5;
            }

            ::-webkit-scrollbar
            {
                width: 12px;
                background-color: #F5F5F5;
            }

            ::-webkit-scrollbar-thumb
            {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                background-color: #555;
            }

        </style>
    @endsection

    @section('scripts')

        <script>

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });



            });

            function confirm_order(orderID){
                bootbox.confirm("Are you sure you want to mark this order as finished? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/orders/mark_complete/' + orderID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }


            function republish_order(orderID){
                bootbox.confirm("Are you sure you want to republish this order? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/orders/republish/' + orderID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

        </script>


        <!-- Ckeditor -->
        {{--<script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>--}}
        {{--<script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>--}}


    @endsection