<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5d77947dab6f1000123c86a6&product=inline-share-buttons' async='async'></script>


    <title>Essay Energy</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="description" content="These terms & conditions (the “T&C” or “Terms”) govern your access and use of our Website and services at EssayEnergy.com (the “Website”). Please read them carefully.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{url('styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{url('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('plugins/colorbox/colorbox.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{url('styles/blog_single.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('styles/blog_single_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('styles/main_styles.css')}}">





    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


</head>
<body>

<div class="super_container">


    @include('layouts.header')


    <!-- Home -->

        <div class="home" style="height: 200px">
            <div class="breadcrumbs_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="breadcrumbs">
                                <ul>
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <li><a href="{{url('terms')}}">Terms of use</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Blog -->

        <div class="blog">
            <div class="container">
                <div class="row">

                    <!-- Blog Content -->
                    <div class="col-lg-8">
                        @if(!is_null($terms->first()))
                            {!! $terms->first()->terms !!}
                        @endif
                    </div>

                    <!-- Blog Sidebar -->
                    <div class="col-lg-4">
                        <div class="sidebar">

                            <!-- Categories -->
                            <div class="sidebar_section">
                                {{--<div class="sidebar_section_title">Place an order now</div>--}}
                                <div class="sidebar_categories">
                                    @guest

                                        <form class="counter_form_content d-flex flex-column align-items-center justify-content-center" action="{{url('/register')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="user_group"  value="1">
                                            <div class="counter_form_title">place order now</div>

                                            <input type="email" name="email" class="counter_input" placeholder="E-Mail" required="required">
                                            @if ($errors->has('email'))
                                                @section('scripts')
                                                    <script type="text/javascript">
                                                        $(document).ready(function(){
                                                            $('#newLoginModal').modal('show');
                                                        });
                                                    </script>
                                                @endsection
                                            @endif
                                            {{--{!! Form::select('first_order_type', \App\Type::pluck('name', 'id'), null,--}}
                                            {{--['class' => 'dropdown_item_select counter_input', 'id'=>'specialDude','required']) !!}--}}

                                            <select id="specialDude" required  class="dropdown_item_select counter_input" name="first_order_type">
                                                <option value="">Type of paper</option>
                                                @foreach(\App\Type::all() as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach

                                            </select>


                                            <input type="number" name="first_order_pages" min="1" class="counter_input" placeholder="Number of pages" required="required">

                                            <button type="submit" class="btn btn-success btn-block">Get Started Now</button>
                                        </form>

                                    @else
                                        <div style="text-align: center;" class="counter_form_content d-flex flex-column align-items-center justify-content-center">
                                            <div class="counter_form_title">place order now</div>

                                            <a href="{{url('orders/new')}}" class="btn btn-success align-center" style="margin-top: 20px">Place your order now</a>

                                        </div>

                                    @endauth
                                </div>
                            </div>

                            <!-- Latest posts -->
                            <div class="sidebar_section">
                                <div class="sidebar_section_title">Latest Posts</div>
                                <div class="sidebar_latest">

                                    @foreach($latest as $blogItem)
                                        <!-- Latest Post -->
                                            <div class="latest d-flex flex-row align-items-start justify-content-start">
                                                <div class="latest_image">
                                                    <div>
                                                        <img src="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($blogItem->image_link)}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_title"><a href="{{url('blog/'.$blogItem->slug)}}">{{$blogItem->title}}</a></div>
                                                    <div class="latest_date">{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($blogItem->created_at)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}</div>
                                                </div>
                                            </div>
                                    @endforeach

                                </div>
                            </div>

                            <!-- Banner -->
                            <div class="sidebar_section">
                                <div class="sidebar_banner d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="sidebar_banner_background" style="background-image:url(images/banner_1.jpg)"></div>
                                    <div class="sidebar_banner_overlay"></div>
                                    <div class="sidebar_banner_content">
                                        <div class="banner_title">High quality papers</div>
                                        <div class="banner_button"><a href="{{url('how_it_works')}}">learn more</a></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="sharethis-inline-share-buttons"></div>
                </div>
            </div>
        </div>



    @include('layouts.footer')


    <!-- Modal -->
    <div class="modal fade" id="newLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sign in to continue to Essay Energy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 mx-auto">
                                <div class="account-wall">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">

                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Password" autofocus required>
                                        @if ($errors->has('password'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                        @if ($errors->has('email'))
                                            <span class="help-block has-error"> <strong>{{ $errors->first('email') }}</strong></span>
                                        @endif

                                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                                            Sign in</button>

                                        <a href="{{url('/password/reset')}}"  class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>




</div>

<script src="{{url('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{url('styles/bootstrap4/popper.js')}}"></script>
<script src="{{url('styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{url('plugins/easing/easing.js')}}"></script>
<script src="{{url('plugins/parallax-js-master/parallax.min.js')}}"></script>
<script src="{{url('plugins/colorbox/jquery.colorbox-min.js')}}"></script>
<script src="{{url('js/blog_single.js')}}"></script>

@yield('scripts')

</body>

</html>