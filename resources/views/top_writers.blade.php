<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146338409-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146338409-1');
    </script>

    <title>Top Writers</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Essay Energy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="styles/about.css">
    <link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
    <style>
        .stars i {
            color: #ffc80a;
        }
    </style>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ce2580ad07d7e0c639467de/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</head>
<body>

<div class="super_container">

@include('layouts.header')


<!-- Home -->

    <div class="home">
        <div class="breadcrumbs_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>Top Writers</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="team">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <h2 class="section_title">The Best Writers in Town</h2>
                        <div class="section_subtitle"><p>Here are some of the top writers with the highest ratings at Essay Energy</p></div>
                    </div>
                </div>
            </div>
            <div class="row team_row">

                @foreach($topWriters as $topWriter)
                <!-- Team Item -->
                <div class="col-lg-3 col-md-6 team_col">
                    <div class="team_item">
                        <div class="team_image">
                            <img class="team_image" src="{{ Laricon::getImageDataUri(\App\User::find($topWriter->id)->name) }}" alt="{{\App\User::find($topWriter->id)->name}}" />
                        </div>
                        {{--<div class="team_image"><img src="images/team_1.jpg" alt=""></div>--}}
                        <div class="team_body">
                            <div class="team_title"><a href="#">{{\App\User::find($topWriter->id)->name}}</a></div>
                            <div class="team_subtitle">{{\App\WriterProfile::where('user_id', $topWriter->id)->first()->description}}</div>
                            <div class="social_list">
                                @switch(round($topWriter->rating,0))
                                @case(1)
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                @break
                                @case(2)
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                @break
                                @case(3)
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                @break
                                @case(4)
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star"></span>
                                @break
                                @case(5)
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                <span class="fa fa-star" style="color: orange;"></span>
                                @break
                                @endswitch
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>


    @include('layouts.footer')


</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/about.js"></script>

</body>

</html>