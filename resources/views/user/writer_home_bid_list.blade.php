@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">done_all</i>
                        </div>
                        <div class="content">
                            <div class="text">COMPLETE ORDERS</div>
                            <div class="number count-to" data-from="0" data-to="{{\App\Order::join('bids','orders.bid_id','bids.id')
                                                                                    ->select('orders.*')
                                                                                    ->where('bids.bidder_id', Auth::user()->id)
                                                                                    ->where('bids.status', 1)
                                                                                    ->where('orders.status', 3)
                                                                                    ->orderBy('bids.updated_at', 'desc')
                                                                                    ->count()}}"
                                 data-speed="1000" data-fresh-interval="5">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">query_builder</i>
                        </div>
                        <div class="content">
                            <div class="text">IN PROGRESS</div>
                            <div class="number count-to" data-from="0" data-to="{{\App\Order::join('bids','orders.bid_id','bids.id')
                                                                                        ->select('orders.*')
                                                                                        ->where('bids.bidder_id', Auth::user()->id)
                                                                                        ->where('bids.status', 1)
                                                                                        ->where('orders.status', 2)
                                                                                        ->orderBy('bids.updated_at', 'desc')
                                                                                        ->count()}}"
                                 data-speed="1000" data-fresh-interval="5">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">receipt</i>
                        </div>
                        <div class="content">
                            <div class="text">MY BIDS</div>
                            <div class="number count-to" data-from="0" data-to="{{\App\Bid::join('orders','bids.order_id','orders.id')
                                                                                    ->select('bids.*','orders.id as order_id', 'orders.topic','orders.deadline','orders.pages')
                                                                                    ->where('bids.bidder_id', Auth::user()->id)
                                                                                    ->where('bids.status', 0)
                                                                                    ->where('orders.status', 1)
                                                                                    ->orderBy('bids.updated_at', 'desc')
                                                                                    ->count()}}"
                                 data-speed="1000" data-fresh-interval="5">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">rate_review</i>
                        </div>
                        <div class="content">
                            <div class="text">AVERAGE RATING</div>
                            <div class="number">{{round(Auth::user()->avg_rating(),2)}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->


            <div class="row clearfix">



                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        Available Orders
                                        <small>Here is a list of the latest orders that you can bid on</small>

                                        <br>



                                    </h2>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="material-icons">more_vert</i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="javascript:void(0);">Action</a></li>
                                                <li><a href="javascript:void(0);">Another action</a></li>
                                                <li><a href="javascript:void(0);">Something else here</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                                <div class="body">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#addorder" data-toggle="tab">BID ON AN ORDER</a></li>

                                    </ul>


                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                            <div class="card">
                                                <div class="body">

                                                    <table class="table table-hover  table-striped table-responsive">
                                                        <thead>
                                                        <tr>
                                                            <th>Order No.</th>
                                                            <th>Topic</th>
                                                            <th>Discipline</th>
                                                            <th>Pages</th>
                                                            <th>Deadline</th>
                                                            <th>Budget</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($orders as $order)

                                                            @if(is_null(\App\Bid::where('order_id',$order->id)->where('bidder_id', Auth::user()->id)->first()))
                                                                <tr>
                                                                <td>
                                                                    <a href="{{url('/bid/'.$order->id)}}">
                                                                        #{{$order->id}}

                                                                    </a>


                                                                </td>

                                                                <td data-toggle="tooltip" data-placement="top" title="{{$order->type->name}}">
                                                                    <a href="{{url('/bid/'.$order->id)}}">
                                                                        {{$order->topic}}
                                                                    </a>
                                                                </td>

                                                                <td>
                                                                    {{$order->discipline->discipline}}
                                                                </td>

                                                                <td>
                                                                    {{$order->pages}}
                                                                </td>

                                                                <td data-toggle="tooltip" data-placement="top" title="{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                                  ->format(' jS \\ F, Y h:i A')}}">

                                                                    <br>

                                                                    {!!
                                                                        \Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline))) > \Carbon\Carbon::now()
                                                                            ?  "<span style=\"color: #4D953C\">" : "<span style=\"color: red\">"
                                                                    !!}

                                                                    {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))->diffForHumans()}}
                                                                    </span>
                                                                </td>

                                                                <td>
                                                                    $ {{$order->budget*$order->pages}}
                                                                    {{--$ {{$order->budget/config('constants.multiplier')}}--}}

                                                                    {{--@if(\App\Bid::where('order_id',$order->id)->where('bidder_id', Auth::user()->id)->first())--}}
                                                                    {{--<i data-toggle="tooltip" data-placement="top" title="You already placed a bid on this order" class="material-icons">turned_in</i>--}}
                                                                    {{--@endif--}}
                                                                </td>
                                                            </tr>

                                                            @endif

                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                    {{$orders->links()}}


                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


            </div>

        </div>
    </section>
@endsection


@section("scripts")
    <script src="{{url('/inner/js/pages/ui/tooltips-popovers.js')}}"></script>

@endsection


