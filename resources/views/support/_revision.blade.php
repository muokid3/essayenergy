<div class="col-sm-12 text-right" style="font-size: 13px; padding: 6px 12px;font-weight: 400; line-height: 1.42857143;background : #eee; color:  #3e434a;">
    Last Updated: <strong>{{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->assigned ? $order->assigned->updated_at : "1973-00-00 00:00:00" )))
                                                                                                                      ->format(' jS \\ F, Y h:i A')}}</strong>
</div>
<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{url('/order/submission/update')}}">
    {{ csrf_field() }}
    <input type="hidden" name="assigned_id" value="{{$order->assigned ? $order->assigned->id : 0 }}">
    <textarea name="order_submission" id="ckeditor">
                                                                            {!! $order->assigned ? $order->assigned->submission : "" !!}
                                                                        </textarea>

    <div class="row clearfix" style="margin-top: 2%">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
            <button type="submit" name="btn_action" value="save" class="btn btn-primary btn-block waves-effect">SAVE</button>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label>Upload final file</label>
            <input type="file" name="file">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label>Current file: </label>
            <a href="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($order->assigned->submitted_file)}}">View/Download current submission</a>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
            <button type="submit" name="btn_action" value="save_submit"  class="btn btn-success btn-block">SAVE AND SUBMIT</button>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-12 col-md-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="row clearfix" style="margin-top: 10px">
                        <div class="col-md-12">
                            <blockquote>
                                <p>{!! optional($order->revision())->instructions !!}</p>
                                <footer><div class="form-group form-float">

                                        <cite title="Source Title">
                                            <label>Revision instructions</label>
                                        </cite>

                                    </div>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</form>
