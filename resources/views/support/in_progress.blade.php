@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Ongoing Orders
                                <small>These orders are currently ongoing. Please ensure you complete them within time</small>

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#addorder" data-toggle="tab">ORDERS IN PROGRESS</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                    <div class="card">
                                        <div class="body">

                                            <table class="table table-striped table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Order No.</th>
                                                    <th>Topic</th>
                                                    <th>Type Of paper</th>
                                                    <th>Pages (Words)</th>
                                                    <th>Deadline</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($ordersAssignments as $orderAssignment)

                                                    <tr>
                                                        <td>
                                                            <a href="{{url('/order/'.$orderAssignment->order->id)}}">
                                                                #{{$orderAssignment->order->id}}
                                                            </a>
                                                        </td>

                                                        <td>
                                                            <a href="{{url('/order/'.$orderAssignment->order->id)}}">
                                                                {{$orderAssignment->order->topic}}
                                                            </a>
                                                        </td>

                                                        <td>
                                                            {{$orderAssignment->order->type->name}}
                                                        </td>

                                                        <td>
                                                            {{$orderAssignment->order->pages}} <span style="color: red">({{$orderAssignment->order->pages*\App\OrderCostSettings::find(1)->words_per_page}} words)</span>
                                                        </td>

                                                        <td>
                                                            {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($orderAssignment->order->deadline)))
                                                                                                          ->format(' jS \\ F, Y h:i A')}}
                                                            <br>

                                                            {!!
                                                                \Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($orderAssignment->order->deadline))) > \Carbon\Carbon::now()
                                                                    ?  "<span style=\"color: #4D953C\">" : "<span style=\"color: red\">"


                                                            !!}

                                                            ({{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($orderAssignment->order->deadline)))->diffForHumans()}})
                                                            </span>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            {{$ordersAssignments->links()}}



                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>

@endsection