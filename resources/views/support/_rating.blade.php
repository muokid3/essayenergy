@switch($order->assigned->support_done_review)
    @case(1)
    {{--he already reviewed, show his rating and review--}}
    <div class="card">
        <div class="body">

            <div class="row clearfix" style="margin-top: 10px">
                <div class="col-md-12 ">
                    <blockquote>
                        <p>{{\App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first() ?
                                                                                                \App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first()->review : ""}}</p>
                        <footer><div class="form-group form-float">

                                <cite title="Source Title">
                                    <label>Your Rating</label>
                                </cite>

                                @php
                                    $stars = \App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first() ?
                                                \App\Rating::where('order_id',$order->id)->where('rated_by',Auth::user()->id)->first()->rating : 0;
                                @endphp

                                @switch($stars)
                                    @case(1)
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    @break
                                    @case(2)
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    @break
                                    @case(3)
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    @break
                                    @case(4)
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star"></span>
                                    @break
                                    @case(5)
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    <span class="fa fa-star" style="color: orange;"></span>
                                    @break
                                @endswitch

                            </div>
                        </footer>
                    </blockquote>
                </div>
            </div>

        </div>
    </div>

    @break
    @default
    {{--he hasn't revied, show review form--}}
    <div class="card">
        <div class="body">
            <h3>Rating and Review</h3>

            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{url('/support/customer/rate')}}">
                {{ csrf_field() }}

                <input type="hidden" name="order_id" value="{{$order->id}}">
                <input type="hidden" name="rated_user" value="{{$order->customer->user->id}}">
                <input type="hidden" name="assigned_id" value="{{$order->assigned->id}}">
                <div class="row clearfix" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group form-float">
                            <label>Your Rating</label>

                            <div class="star-rating">
                                <input id="star-5" type="radio" name="rating" value="5">
                                <label for="star-5" title="5 stars">
                                    <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                </label>
                                <input id="star-4" type="radio" name="rating" value="4">
                                <label for="star-4" title="4 stars">
                                    <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                </label>
                                <input id="star-3" type="radio" name="rating" value="3">
                                <label for="star-3" title="3 stars">
                                    <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                </label>
                                <input id="star-2" type="radio" name="rating" value="2">
                                <label for="star-2" title="2 stars">
                                    <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                </label>
                                <input id="star-1" type="radio" name="rating" value="1">
                                <label for="star-1" title="1 star">
                                    <i class="active fa fa-2x fa-star" aria-hidden="true"></i>
                                </label>
                            </div>

                            <p style="color: red">{{$errors->first("rating") }}</p>

                        </div>
                    </div>
                </div>

                <div class="row clearfix" style="margin-top: 10px">
                    <div class="col-md-6 col-md-offset-1">
                        <div style="margin-right: -15px; margin-left: -15px;">
                            <label>Your Review</label>
                            <textarea name="review"  rows="5" class="form-control" placeholder="Please write a brief review about your experience with the writer" aria-required="true"></textarea>
                        </div>
                        <p style="color: red">{{$errors->first("review") }}</p>

                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-md-6 col-md-offset-1">
                        <div style="margin-right: -15px; margin-left: -15px;">
                            <button type="submit" class="btn btn-success btn-block waves-effect">Submit Rating</button>
                        </div>
                    </div>
                </div>


            </form>

        </div>
    </div>
@endswitch