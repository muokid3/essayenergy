@extends('layouts.app')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Essay Energy</h2>
            </div>

            <!-- Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Order Details
                                <small>View order details on this page</small>

                                <br>


                                @if (Session::has('message'))
                                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="body">
                            <!-- Nav tabs -->

                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <button class="btn btn-default btn-lg btn-block" type="button">ORDER ID <span class="badge">#{{$order->id}}</span></button>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <span class="btn btn-primary btn-lg btn-block " type="button">Status <span class="badge">{{$order->order_status->code}}</span></span>
                                </div>

                            </ul>


                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="addorder">
                                    <div class="card">
                                        <div class="body">

                                           <div class="row">
                                               <div class="col-sm-2 col-lg-2 col-md-2 col-xs-12"
                                               style="border-right: 1px solid #eee;">
                                                   <img src="{{url('/inner/images/user.png')}}" class="img-rounded" alt="Customer" width="75%" >
                                                   <br>
                                                   <h5>
                                                       #{{$order->customer->user->name}}
                                                   </h5>

                                                   @switch(round($order->customer->user->avg_rating(),0))
                                                   @case(1)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(2)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(3)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(4)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star"></span>
                                                   @break
                                                   @case(5)
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   <span class="fa fa-star" style="color: orange;"></span>
                                                   @break
                                                   @endswitch


                                                   <h5>Completed Orders: {{\App\Order::where('customer_id', $order->customer->id)->where('status_id',5)->count()}}</h5>

                                               </div>

                                               <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12"
                                                    style="border-right: 1px solid #eee;">

                                                   <table class='table no-border' border="0" cellpadding="0" cellspacing="0">
                                                       <tbody>
                                                            <tr>
                                                                <td>Type of paper: </td>
                                                                <td>{{$order->type->name}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Topic: </td>
                                                                <td>{{$order->topic}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Pages: </td>
                                                                <td>{{$order->pages}} ({{$order->pages*\App\OrderCostSettings::find(1)->words_per_page}} words)</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Discipline: </td>
                                                                <td>{{$order->discipline->discipline}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Type of service: </td>
                                                                <td>{{$order->type_of_service}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Format or citation style: </td>
                                                                <td>{{$order->citation}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Deadline: </td>
                                                                <td>
                                                                    {{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))
                                                                                                         ->format(' jS \\ F, Y h:i A')}}
                                                                    <br>

                                                                    {!!
                                                                        \Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline))) > \Carbon\Carbon::now()
                                                                            ?  "<span style=\"color: #4D953C\">" : "<span style=\"color: red\">"


                                                                    !!}

                                                                    ({{\Carbon\Carbon::parse(date("d-m-Y H:i:s", strtotime($order->deadline)))->diffForHumans()}})
                                                                    </span>
                                                                </td>
                                                            </tr>


                                                       </tbody>
                                                   </table>

                                               </div>

                                               <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12"
                                                    style="border-right: 1px solid #eee; ">
                                                       <h4 style="text-align: center;">
                                                           <u>Paper Instructions</u>
                                                       </h4>
                                                   <br>

                                                   <div id="module" class="container">
                                                       <p class="collapse" id="collapseExample" aria-expanded="false">
                                                           {!!  $order->instructions !!}
                                                       </p>
                                                       <a role="button" class="collapsed" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                       </a>
                                                   </div>

                                               </div>

                                               <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12"
                                                        style="text-align: center;">
                                                   <h4>
                                                       <u>Additional Material (Files)</u>
                                                   </h4>

                                                   <br>

                                                   <ul style="text-align: left">
                                                       @if(!is_null($order->file_path))
                                                           <li>
                                                               <a href="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($order->file_path)}}" target="_blank">
                                                                   {{$order->file_path}}
                                                                   {{str_replace("order_files/", "", $order->file_path)}}
                                                               </a>
                                                           </li>
                                                       @endif
                                                       @foreach(\App\AdditionalMaterial::where('order_id',$order->id)->orderBy('id', 'asc')->get() as $additional)
                                                           <li>
                                                               <a href="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($additional->file_path)}}" target="_blank">
                                                                   {{str_replace("order_files/", "", $additional->file_path)}}
                                                               </a>
                                                           </li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                           </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8 col-lg-8 col-md-8 col-xs-12">

                                            @if($order->status_id == 11)
                                                {{--published. accept the order--}}
                                                        <div class="card">
                                                            <div class="body">
                                                                <div style="text-align: center;">
                                                                    <h3>Accept Order</h3>
                                                                </div>

                                                                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/order/accept') }}">
                                                                    {{ csrf_field() }}


                                                                    <input type="hidden" name="order_id" value="{{$order->id}}">


                                                                    <div class="row clearfix" style="margin-top: 10px">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <b id="totalBid">Price Per page: ${{$order->cost/$order->pages}}</b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row clearfix">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <p class="font-bold col-teal">Total Price: ${{$order->cost}}</p>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row clearfix" style="margin-top: 10px">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <label>Welcome Message</label><span style="color: red">*</span>
                                                                            <textarea name="welcome_message"  rows="5" class="form-control" placeholder="Please write a brief welcome message to the customer" aria-required="true"></textarea>
                                                                            <p style="color: red">{{$errors->first("welcome_message") }}</p>

                                                                        </div>
                                                                    </div>





                                                                    <div class="row clearfix">
                                                                        <div class="col-md-6 col-md-offset-3">
                                                                            <div>
                                                                                <button type="submit" class="btn btn-success btn-block waves-effect">ACCEPT ORDER</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>

                                                            </div>
                                                        </div>
                                                @elseif($order->status_id == 5)
                                                {{--completed. ratings stuff--}}
                                                    @include('support._rating')
                                                @elseif($order->status_id == 6)
                                                {{--in progress. keep working--}}
                                                    @include('support._in_progress')
                                                @elseif($order->status_id == 7)
                                                {{--submitted waiting for acceptance or revision--}}
                                                    @include('support._in_progress')
                                                @elseif($order->status_id == 12)
                                                {{--revision. edit and correct mistakes--}}
                                                    @include('support._revision')

                                            @endif


                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
                                            {{--chat here--}}
                                            @if($order->status_id == 6 || $order->status_id == 7 || $order->status_id == 12)
                                            @include('layouts.writer_chat')
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->
        </div>
    </section>







@endsection


    @section('css')
        <style>



            .star-rating {
                direction: rtl;
                display: inline-block;
                padding: 20px
            }

            .star-rating label:before,
            .star-rating label:after {
                display: none;
            }

            .star-rating label {
                color: #bbb;
                font-size: 18px;
                padding: 0;
                cursor: pointer;
                -webkit-transition: all .3s ease-in-out;
                transition: all .3s ease-in-out
            }

            .star-rating label:hover,
            .star-rating label:hover~label,
            .star-rating input[type=radio]:checked~label {
                color: #f2b600
            }








            #module {
                width: 100%;
            }

            #module p.collapse[aria-expanded="false"] {
                display: block;
                height: 40px !important;
                overflow: hidden;
            }

            #module p.collapsing[aria-expanded="false"] {
                height: 40px !important;
            }

            #module a.collapsed:after  {
                content: '+ Show More';
            }

            #module a:not(.collapsed):after {
                content: '- Show Less';
            }

        </style>
    @endsection

    @section('scripts')

        <script>


            $('input[name="rating"]').change(function(e) { // Select the radio input group

                // This returns the value of the checked radio button
                // which triggered the event.
                console.log( $(this).val() );

            });

            $(document).ready(function () {

                $('.datetimepicker').datetimepicker({
                    minDate: moment()
                });

                $('input[name=bid_amount]').keyup(function(){
                    $('#totalBid').text("Total Price: $" +this.value*'{{$order->pages}}' );
                });



            });

            function makeFileList() {
                var input = document.getElementById("filesToUpload");
                var ul = document.getElementById("fileList");
                while (ul.hasChildNodes()) {
                    ul.removeChild(document.getElementById('fileList').firstChild);
                }
                for (var i = 0; i < input.files.length; i++) {
                    var li = document.createElement("li");
                    li.innerHTML = input.files[i].name;
                    ul.appendChild(li);
                }
                if(!ul.hasChildNodes()) {
                    var li = document.createElement("li");
                    li.innerHTML = 'No Files Selected';
                    ul.appendChild(li);
                }
            }

            function remove_bid(bidID){
                bootbox.confirm("Are you sure you want to remove your bid for this order? ", function(result) {
                    if(result) {
                        $.ajax({
                            url: '/bid/remove/' + bidID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

        </script>


        <!-- Ckeditor -->
        <script src="{{url('/inner/plugins/ckeditor/ckeditor.js')}}"></script>
        <script src="{{url('/inner/js/pages/forms/editors.js')}}"></script>


    @endsection