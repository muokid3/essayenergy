<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/how_it_works', 'HomeController@how_it_works');
//Route::get('/latest_orders', 'HomeController@latest_orders');
Route::get('/latest_reviews', 'HomeController@latest_reviews');
Route::get('/sample_papers', 'HomeController@sample_papers');
Route::get('/about', 'HomeController@about');
Route::get('/terms', 'HomeController@terms');
Route::get('/privacy', 'HomeController@privacy');
Route::get('/blog/{slug}', 'HomeController@get_blog');
Route::get('/blog', 'HomeController@blog');
Route::post('/paypal/ipn/receive', 'HomeController@receive_ipn');
Route::get('/paypal/sample_papers/get_order/{orderId}', 'HomeController@getSamplePaperOrder');

//Route::get('test', function () {
//    event(new App\Events\BidPlaced('Someone', '100'));
//    return "Event has been sent!";
//});

Auth::routes();
Route::post('/register/express', 'HomeController@register_user');




Route::group(['middleware' => ['auth']], function () {
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/dashboard', 'UserController@index');

    Route::get('/sample_papers/{slug}', 'HomeController@sample_paper');
    Route::get('/sample_papers/download/{id}', 'HomeController@download_sample_paper');



    Route::group(['middleware' => ['customer']], function () {
        //customer
        Route::get('/orders/new', 'OrderController@new_order');
        Route::post('/orders/new/create', 'OrderController@create_order');
        Route::get('/orders/details/{order_id}', 'OrderController@order');
        Route::get('/orders/reserve/{order_id}', 'OrderController@reserve_funds');
        Route::post('/orders/cancel', 'OrderController@cancel_order');
        Route::post('/orders/request_revision', 'OrderController@request_revision');
        Route::get('/orders/my_orders', 'OrderController@my_orders');
        Route::post('/orders/extend_deadline', 'OrderController@extend_deadline');
        Route::post('/orders/upload_additional', 'OrderController@upload_additional');
        Route::get('/orders/in_progress', 'OrderController@in_progress');
        Route::get('/orders/mark_complete/{order_id}', 'OrderController@mark_complete');
        Route::get('/orders/completed', 'OrderController@completed');
        Route::post('/customer/support/rate/', 'OrderController@rate_support');
        Route::get('/orders/cancelled', 'OrderController@cancelled');
        Route::get('/orders/republish/{order_id}', 'OrderController@republish');


    });






//    Route::get('/orders/bid/details/{bid_id}', 'OrderController@bid_details');
//    Route::get('/orders/bid/accept/{bid_id}', 'OrderController@accept_bid');



    Route::get('/payments', 'PaymentController@customer_payments');
    Route::post('/pay/btc', 'PaymentController@pay_btc');
    Route::get('/reviews', 'ReviewController@index');


    Route::group(['middleware' => ['support']], function () {
        //support
        Route::get('/order/{order_id}', 'SupportController@order');
        Route::post('/order/accept', 'SupportController@accept_order');
        Route::post('/order/submission/update', 'SupportController@update_submission');
        Route::get('/order/support/in_progress', 'SupportController@in_progress');
        Route::get('/order/support/completed', 'SupportController@completed');
        Route::get('/order/support/my_orders', 'SupportController@my_orders');
        Route::post('/support/customer/rate/', 'SupportController@rate_customer');

    });










    //writer
//    Route::post('/writer/create_profile', 'WriterController@create_profile');
//    Route::post('/writer/update_profile', 'WriterController@update_profile');
//    Route::post('/writer/create_sample_essay', 'WriterController@create_sample_essay');
//    Route::post('/writer/update_sample_essay', 'WriterController@update_sample_essay');
//    Route::post('/bid/place', 'WriterController@place_bid');
//    Route::post('/bid/update', 'WriterController@update_bid');
//    Route::get('/bid/remove/{bid_id}', 'WriterController@remove_bid');
//    Route::post('/order/submission/update', 'WriterController@update_submission');
//    Route::get('/writer/in_progress', 'WriterController@in_progress');
//    Route::get('/writer/bids', 'WriterController@bids');
//    Route::get('/writer/completed', 'WriterController@completed');
//    Route::get('/writer/cancelled', 'WriterController@cancelled');
//    Route::post('/writer/customer/rate/', 'WriterController@rate_customer');
//    Route::get('/writer/profile/', 'WriterController@profile');






    Route::get('/markAsRead', function (){
        auth()->user()->unreadNotifications->markAsRead();
    });







});
