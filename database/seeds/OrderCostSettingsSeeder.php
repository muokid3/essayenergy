<?php

use Illuminate\Database\Seeder;

class OrderCostSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('order_cost_settings')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            \App\OrderCostSettings::firstOrCreate(array(
                'id' => 1,
                'words_per_page' => 275
            ));
    }
}
