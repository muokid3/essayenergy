<?php

use Illuminate\Database\Seeder;

class OrganizationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Organization::firstOrCreate(array(
            'id' => 1,
            'org_code' => 'ESNGY',
            'org_name' => 'Essay Energy'
        ));
    }
}
