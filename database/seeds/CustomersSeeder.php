<?php

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Customer::firstOrCreate(array(
            'id' => 1,
            'status' => 'active',
            'balance' => '0.00'
        ));

        \App\Customer::firstOrCreate(array(
            'id' => 2,
            'status' => 'active',
            'balance' => '0.00'
        ));
    }
}
