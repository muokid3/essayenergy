<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\User::firstOrCreate(array(
            'id' => 1,
            'name' => "System Admin",
            'email' => "essayenergy18@gmail.com",
            'customer_id' => 2,
            'password' => bcrypt("essayenergy18"),
            'user_group' => 3,
        ));
    }
}
