<?php

use Illuminate\Database\Seeder;

class DummyReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('dummy_reviews')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $faker = \Faker\Factory::create();


        $json = File::get("database/data/dummy_reviews.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            $arrX = array(1,2,4,5,6,7,8,9,10,11);
            $randIndex = array_rand($arrX);

            \App\DummyReview::firstOrCreate(array(
                'id' => $obj->id,
                'topic' => $obj->topic,
                'paper_type' => $arrX[$randIndex], //rand(1,11),
                'paper_discipline' => rand(1,15),
                'pages' => rand(5,20),
                'order_id' => rand(100,1000),
                'rating' => rand(4,5),
                'review' => $obj->review,
                'created_at' => $faker->dateTimeBetween('-1 year','now')
            ));
        }
    }
}
