<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(TypeSeeder::class);
         $this->call(DisciplineSeeder::class);
         $this->call(CitationSeeder::class);
         $this->call(PermissionSeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(StatusSeeder::class);
         $this->call(DeadlinePricingSeeder::class);
         $this->call(OrderCostSettingsSeeder::class);
         $this->call(CustomersSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(OrganizationsSeeder::class);
    }
}
