<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('status_codes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $json = File::get("database/data/default_statuses.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\StatusCode::firstOrCreate(array(
                'id' => $obj->id,
                'code' => $obj->code,
                'description' => $obj->description
            ));
        }
    }
}
