<?php

use Illuminate\Database\Seeder;

class CitationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('user_groups')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $json = File::get("database/data/citations.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Citation::firstOrCreate(array(
                'id' => $obj->id,
                'citation' => $obj->citation
            ));
        }
    }
}
