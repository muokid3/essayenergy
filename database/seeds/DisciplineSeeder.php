<?php

use Illuminate\Database\Seeder;

class DisciplineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('disciplines')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $json = File::get("database/data/disciplines.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Discipline::firstOrCreate(array(
                'id' => $obj->id,
                'discipline' => $obj->name
            ));
        }
    }
}
