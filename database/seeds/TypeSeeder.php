<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $json = File::get("database/data/types.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Type::firstOrCreate(array(
                'id' => $obj->id,
                'name' => $obj->name
            ));
        }
    }
}
