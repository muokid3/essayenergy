<?php

use Illuminate\Database\Seeder;

class DeadlinePricingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('deadline_pricing_rules')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $json = File::get("database/data/deadline_pricing.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\DeadlinePricingRule::firstOrCreate(array(
                'id' => $obj->id,
                'min_hrs' => $obj->min_hrs,
                'max_hrs' => $obj->max_hrs,
                'price_per_page' => $obj->price_per_page
            ));
        }
    }
}
