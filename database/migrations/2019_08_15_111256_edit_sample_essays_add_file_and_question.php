<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSampleEssaysAddFileAndQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sample_essays', function(Blueprint $table) {
            $table->unsignedInteger('discipline_id')->after('user_id');
            $table->text('question')->after('discipline_id');
            $table->text('slug')->after('question');
            $table->decimal('price',8,2)->after('slug');
            $table->text('file')->after('essay');

            $table->foreign('discipline_id')->references('id')->on('disciplines');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sample_essays', function(Blueprint $table) {
            $table->dropColumn('discipline_id');
            $table->dropColumn('question');
            $table->dropColumn('file');
            $table->dropColumn('slug');
            $table->dropColumn('price');
        });
    }
}
