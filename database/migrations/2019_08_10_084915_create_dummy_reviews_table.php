<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDummyReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dummy_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('topic');
            $table->unsignedInteger('paper_type');
            $table->unsignedInteger('paper_discipline');
            $table->integer('pages');
            $table->integer('order_id');
            $table->integer('rating');
            $table->text('review');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dummy_reviews');
    }
}
