<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditEmailLogMakeFieldsLongText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_log', function(Blueprint $table) {

            DB::statement('ALTER TABLE email_log MODIFY body  LONGTEXT;');
            DB::statement('ALTER TABLE email_log MODIFY attachments  LONGTEXT;');

//            $table->longText('body')->change();
//            $table->longText('attachments')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
