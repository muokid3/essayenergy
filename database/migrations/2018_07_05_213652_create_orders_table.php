<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('deadline_pricing_rule_id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('type_id');
            $table->integer('pages');
            $table->dateTime('deadline');
            $table->string('topic');
            $table->decimal('cost',8,2);
            $table->integer('discipline_id');
            $table->string('type_of_service');
            $table->string('citation');
            $table->text('instructions');
            $table->string('file_path')->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('status_codes');
            $table->foreign('deadline_pricing_rule_id')->references('id')->on('deadline_pricing_rules');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('type_id')->references('id')->on('types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
