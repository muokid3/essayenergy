<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('support_id');
            $table->unsignedInteger('status_id')->default(6);
            $table->string('welcome_message');
            $table->text('submission')->nullable();
            $table->tinyInteger('support_finished')->default(0);
            $table->tinyInteger('customer_approved')->default(0);
            $table->tinyInteger('support_done_review')->default(0);
            $table->tinyInteger('customer_done_review')->default(0);
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('support_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('status_codes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_assignments');
    }
}
