<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWriterProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writer_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender');
            $table->string('country');
            $table->text('description');
            $table->string('citation_ids');
            $table->string('discipline_ids');
            $table->string('native_language');
            $table->string('uni_name');
            $table->string('course_name');
            $table->string('degree');
            $table->string('grad_year');
            $table->string('company_name');
            $table->string('field_of_work');
            $table->string('position');
            $table->string('duration');
            $table->boolean('approved')->default(0);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writer_profiles');
    }
}
