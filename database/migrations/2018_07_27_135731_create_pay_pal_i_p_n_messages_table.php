<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayPalIPNMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_pal_i_p_n_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business');
            $table->string('receiver_email');

            $table->string('txn_id');
            $table->string('payment_type');
            $table->string('txn_type')->nullable();
            $table->string('parent_txn_id')->nullable();
            $table->string('item_name')->nullable();
            $table->decimal('mc_gross',8,2);
            $table->string('mc_fee');
            $table->string('mc_currency');
            $table->string('payment_date');
            $table->string('payment_status');
            $table->string('pending_reason')->nullable();
            $table->string('reason_code')->nullable();

            $table->string('protection_eligibility');
            $table->string('address_status')->nullable();
            $table->string('address_name')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_zip')->nullable();
            $table->string('address_country_code')->nullable();

            $table->string('payer_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('payer_status')->nullable();
            $table->string('custom');

            $table->string('ipn_track_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_pal_i_p_n_messages');
    }
}
