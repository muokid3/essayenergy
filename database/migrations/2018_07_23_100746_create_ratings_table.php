<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rated_user');
            $table->unsignedInteger('rated_by');
            $table->unsignedInteger('order_id');
            $table->integer('rating');
            $table->text('review');
            $table->timestamps();

            $table->foreign('rated_user')->references('id')->on('users');
            $table->foreign('rated_by')->references('id')->on('users');
            $table->foreign('order_id')->references('id')->on('orders');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
